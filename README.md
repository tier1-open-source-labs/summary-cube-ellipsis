# Summary Cube Ellipsis

## Pre-Requisite:
Client Center 4.3+

## Manual steps

For version 2.1.4, this line needs to be added to the layout:
```
<apex-import name="T1C_Base__Tier1FeatureLoader" />
```
The layout path is: ACE.ALM.ApplicationSettings.ACECubes.SummaryCube.Layout:CORE:Layout
<br/>
<br/>
<br/>
<br/>
CustomLocale.js needs to be loaded into the ExternalResources of CC, by adding the following line to the External Resources Scripts:
```
{Resource.CustomLocale};
```
The AFR path is: ACE.ALM.Application.ExternalResources:CORE:Scripts
