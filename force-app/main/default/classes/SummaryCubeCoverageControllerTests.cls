/*
 * Name:    SummaryCubeCoverageControllerTests
 *
 * Confidential & Proprietary, 2021 Tier1 Financial Solutions Inc.
 * Property of Tier1 Financial Solutions Inc.
 * This document shall not be duplicated, transmitted or used in whole or in part
 * without written permission from Tier1 Financial Solutions.
 */
@isTest
public with sharing class SummaryCubeCoverageControllerTests 
{
    static Account acct;
    static Contact cont;

    @IsTest
    static void testCoverageMethods()
    {
        setupData();
        
        test.startTest();

        SummaryCubeCoverageController.CoverageInfo covInfo  = SummaryCubeCoverageController.getCoverages(cont.Id, UserInfo.getUserId(), 100,'ACE.CubeConfig.SummaryCube.CoverageConfig');

        SummaryCubeCoverageController.saveEmailInfo(UserInfo.getUserId(), '', '', '', 'Test Subject');
    }

    static void setupData()
    {
        T1C_FR__Feature__c ACE = new T1C_FR__Feature__c(Name = 'ACE', T1C_FR__Name__c = 'ACE');
        T1C_FR__Feature__c ACESecurity = new T1C_FR__Feature__c(Name = 'Security', T1C_FR__Name__c = 'ACE.Security');
        T1C_FR__Feature__c ACESecurityDisabled = new T1C_FR__Feature__c(Name = 'DisabledTriggers', T1C_FR__Name__c = 'ACE.Security.DisabledTriggers');
        T1C_FR__Feature__c ACECubeConfig = new T1C_FR__Feature__c(Name = 'CubeConfig', T1C_FR__Name__c = 'ACE.CubeConfig');
        T1C_FR__Feature__c ACECubeConfigSummaryCube = new T1C_FR__Feature__c(Name = 'SummaryCube', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccountButtons = new T1C_FR__Feature__c(Name = 'AccountButtons', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.AccountButtons');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccountButtonsClickToDial = new T1C_FR__Feature__c(Name = 'ClickToDial', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.AccountButtons.ClickToDial');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccountSection = new T1C_FR__Feature__c(Name = 'AccountSection', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.AccountSection');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccountSectionAccountDetailsLink = new T1C_FR__Feature__c(Name = 'AccountDetailsLink', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.AccountSection.AccountDetailsLink');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccountSectionAccountName = new T1C_FR__Feature__c(Name = 'AccountName', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.AccountSection.AccountName');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccountSectionBillingAddress = new T1C_FR__Feature__c(Name = 'BillingAddress', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.AccountSection.BillingAddress');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccountSectionIndustry = new T1C_FR__Feature__c(Name = 'Industry', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.AccountSection.Industry');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccountSectionInVotingPeriod = new T1C_FR__Feature__c(Name = 'InVotingPeriod', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.AccountSection.InVotingPeriod');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccountSectionLogo = new T1C_FR__Feature__c(Name = 'Logo', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.AccountSection.Logo');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccountSectionPhone = new T1C_FR__Feature__c(Name = 'Phone', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.AccountSection.Phone');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccountSectionRecordType = new T1C_FR__Feature__c(Name = 'RecordType', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.AccountSection.RecordType');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccountSectionTier = new T1C_FR__Feature__c(Name = 'Tier', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.AccountSection.Tier');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccountSectionWebsite = new T1C_FR__Feature__c(Name = 'Website', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.AccountSection.Website');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeContactButtons = new T1C_FR__Feature__c(Name = 'ContactButtons', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.ContactButtons');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeContactButtonsEmail = new T1C_FR__Feature__c(Name = 'Email', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.ContactButtons.Email');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeContactSection = new T1C_FR__Feature__c(Name = 'ContactSection', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.ContactSection');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeContactSectionContactName = new T1C_FR__Feature__c(Name = 'ContactName', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.ContactSection.ContactName');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeContactSectionCreatedDate = new T1C_FR__Feature__c(Name = 'CreatedDate', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.ContactSection.CreatedDate');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeContactSectionEmail = new T1C_FR__Feature__c(Name = 'Email', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.ContactSection.Email');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeContactSectionJobFunction = new T1C_FR__Feature__c(Name = 'JobFunction', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.ContactSection.JobFunction');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeContactSectionLastInteractionDate = new T1C_FR__Feature__c(Name = 'LastInteractionDate', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.ContactSection.LastInteractionDate');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeContactSectionMailingAddress = new T1C_FR__Feature__c(Name = 'MailingAddress', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.ContactSection.MailingAddress');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeContactSectionPhone = new T1C_FR__Feature__c(Name = 'Phone', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.ContactSection.Phone');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeContactSectionPhoto = new T1C_FR__Feature__c(Name = 'Photo', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.ContactSection.Photo');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeCoverageConfig = new T1C_FR__Feature__c(Name = 'CoverageConfig', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.CoverageConfig');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumns = new T1C_FR__Feature__c(Name = 'AccountCoverageColumns', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.CoverageConfig.AccountCoverageColumns');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsDept = new T1C_FR__Feature__c(Name = 'Dept', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.CoverageConfig.AccountCoverageColumns.Dept');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsDivision = new T1C_FR__Feature__c(Name = 'Division', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.CoverageConfig.AccountCoverageColumns.Division');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsEmployee = new T1C_FR__Feature__c(Name = 'Employee', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.CoverageConfig.AccountCoverageColumns.Employee');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsLOB = new T1C_FR__Feature__c(Name = 'LOB', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.CoverageConfig.AccountCoverageColumns.LOB');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsRole = new T1C_FR__Feature__c(Name = 'Role', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.CoverageConfig.AccountCoverageColumns.Role');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeCoverageConfigButtons = new T1C_FR__Feature__c(Name = 'Buttons', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.CoverageConfig.Buttons');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeCoverageConfigButtonsEmailCoverageTeams = new T1C_FR__Feature__c(Name = 'EmailCoverageTeams', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.CoverageConfig.Buttons.EmailCoverageTeams');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeCoverageConfigButtonsRequestCoverage = new T1C_FR__Feature__c(Name = 'RequestCoverage', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.CoverageConfig.Buttons.RequestCoverage');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeCoverageConfigContactCoverageColumns = new T1C_FR__Feature__c(Name = 'ContactCoverageColumns', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.CoverageConfig.ContactCoverageColumns');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeCoverageConfigContactCoverageColumnsEmployee = new T1C_FR__Feature__c(Name = 'Employee', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.CoverageConfig.ContactCoverageColumns.Employee');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeCoverageConfigContactCoverageColumnsLOB = new T1C_FR__Feature__c(Name = 'LOB', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.CoverageConfig.ContactCoverageColumns.LOB');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeCoverageConfigContactCoverageColumnsRole = new T1C_FR__Feature__c(Name = 'Role', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.CoverageConfig.ContactCoverageColumns.Role');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeCoverageConfigFilters = new T1C_FR__Feature__c(Name = 'Filters', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.CoverageConfig.Filters');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeCoverageConfigFiltersContactCoverageRole = new T1C_FR__Feature__c(Name = 'ContactCoverageRole', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.CoverageConfig.Filters.ContactCoverageRole');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeCoverageConfigFiltersAccountCoverageRole = new T1C_FR__Feature__c(Name = 'AccountCoverageRole', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.CoverageConfig.Filters.AccountCoverageRole');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeCoverageConfigFiltersLOB = new T1C_FR__Feature__c(Name = 'LOB', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.CoverageConfig.Filters.LOB');

        insert new list<T1C_FR__Feature__c>{ACE,ACESecurity,ACESecurityDisabled,ACECubeConfig,ACECubeConfigSummaryCube,ACECubeConfigSummaryCubeAccountButtons,ACECubeConfigSummaryCubeAccountButtonsClickToDial,
        ACECubeConfigSummaryCubeAccountSection,ACECubeConfigSummaryCubeAccountSectionAccountDetailsLink,ACECubeConfigSummaryCubeAccountSectionAccountName,
        ACECubeConfigSummaryCubeAccountSectionBillingAddress,ACECubeConfigSummaryCubeAccountSectionIndustry,
        ACECubeConfigSummaryCubeAccountSectionInVotingPeriod,ACECubeConfigSummaryCubeAccountSectionLogo,ACECubeConfigSummaryCubeAccountSectionPhone,
        ACECubeConfigSummaryCubeAccountSectionRecordType,ACECubeConfigSummaryCubeAccountSectionTier,ACECubeConfigSummaryCubeAccountSectionWebsite,
        ACECubeConfigSummaryCubeContactButtons,ACECubeConfigSummaryCubeContactButtonsEmail,ACECubeConfigSummaryCubeContactSection,
        ACECubeConfigSummaryCubeContactSectionContactName,ACECubeConfigSummaryCubeContactSectionCreatedDate,ACECubeConfigSummaryCubeContactSectionEmail,
        ACECubeConfigSummaryCubeContactSectionJobFunction,ACECubeConfigSummaryCubeContactSectionLastInteractionDate,ACECubeConfigSummaryCubeContactSectionMailingAddress,
        ACECubeConfigSummaryCubeContactSectionPhone,ACECubeConfigSummaryCubeContactSectionPhoto,ACECubeConfigSummaryCubeCoverageConfig,
        ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumns,ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsDept,
        ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsDivision,ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsEmployee,
        ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsLOB,ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsRole,
        ACECubeConfigSummaryCubeCoverageConfigButtons,ACECubeConfigSummaryCubeCoverageConfigButtonsEmailCoverageTeams,
        ACECubeConfigSummaryCubeCoverageConfigButtonsRequestCoverage,ACECubeConfigSummaryCubeCoverageConfigContactCoverageColumns,
        ACECubeConfigSummaryCubeCoverageConfigContactCoverageColumnsEmployee,ACECubeConfigSummaryCubeCoverageConfigContactCoverageColumnsLOB,
        ACECubeConfigSummaryCubeCoverageConfigContactCoverageColumnsRole,ACECubeConfigSummaryCubeCoverageConfigFilters,ACECubeConfigSummaryCubeCoverageConfigFiltersContactCoverageRole,
        ACECubeConfigSummaryCubeCoverageConfigFiltersAccountCoverageRole,ACECubeConfigSummaryCubeCoverageConfigFiltersLOB};
        
         insert new list<T1C_FR__Attribute__c>{
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACESecurityDisabled.Id, Name = 'MIGRATION', T1C_FR__Value__c = 'true'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigFiltersContactCoverageRole.Id, Name = 'ApplyTo', T1C_FR__Value__c = 'contact'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigFiltersContactCoverageRole.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Role__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigFiltersContactCoverageRole.Id, Name = 'Enabled', T1C_FR__Value__c = 'true'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigFiltersContactCoverageRole.Id, Name = 'Label', T1C_FR__Value__c = 'CC Role'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigFiltersContactCoverageRole.Id, Name = 'LabelField', T1C_FR__Value__c = 'T1C_Base__Role__c'),          
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigFiltersAccountCoverageRole.Id, Name = 'ApplyTo', T1C_FR__Value__c = 'account'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigFiltersAccountCoverageRole.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Role__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigFiltersAccountCoverageRole.Id, Name = 'Enabled', T1C_FR__Value__c = 'true'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigFiltersAccountCoverageRole.Id, Name = 'Label', T1C_FR__Value__c = 'AC Role'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigFiltersAccountCoverageRole.Id, Name = 'LabelField', T1C_FR__Value__c = 'T1C_Base__Role__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigFiltersLOB.Id, Name = 'ApplyTo', T1C_FR__Value__c = 'All'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigFiltersLOB.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Employee__r.T1C_Base__LOB__r.Name'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigFiltersLOB.Id, Name = 'Enabled', T1C_FR__Value__c = 'true'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigFiltersLOB.Id, Name = 'Label', T1C_FR__Value__c = 'LOB'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigFiltersLOB.Id, Name = 'LabelField', T1C_FR__Value__c = 'T1C_Base__Employee__r.T1C_Base__LOB__r.Name'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfig.Id, Name = 'AdditionalBCC', T1C_FR__Value__c = ''),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfig.Id, Name = 'AdditionalCC', T1C_FR__Value__c = ''),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfig.Id, Name = 'AdditionalTo', T1C_FR__Value__c = ''),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfig.Id, Name = 'EmailSubject', T1C_FR__Value__c = 'Test Subject'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfig.Id, Name = 'ShowEmailSettings', T1C_FR__Value__c = 'TRUE'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsDept.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Employee__r.T1C_Base__Department__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsDept.Id, Name = 'Label', T1C_FR__Value__c = 'Department'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsDept.Id, Name = 'Order', T1C_FR__Value__c = '4'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsDivision.Id, Name = 'DataField', T1C_FR__Value__c = 'CreatedDate'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsDivision.Id, Name = 'Formatter', T1C_FR__Value__c = 'dateTimeFormatter'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsDivision.Id, Name = 'Label', T1C_FR__Value__c = 'Created Date'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsDivision.Id, Name = 'Order', T1C_FR__Value__c = '5'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsDivision.Id, Name = 'Type', T1C_FR__Value__c = 'DateTime'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsEmployee.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Employee__r.Name'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsEmployee.Id, Name = 'Formatter', T1C_FR__Value__c = 'coverageGridEmailFormatter'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsEmployee.Id, Name = 'Label', T1C_FR__Value__c = 'Employee'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsEmployee.Id, Name = 'LinkField', T1C_FR__Value__c = 'T1C_Base__Employee__r.T1C_Base__Email__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsEmployee.Id, Name = 'Order', T1C_FR__Value__c = '1'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsLOB.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Employee__r.T1C_Base__LOB__r.Name'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsLOB.Id, Name = 'Label', T1C_FR__Value__c = 'LOB'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsLOB.Id, Name = 'Order', T1C_FR__Value__c = '2'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsRole.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Role__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsRole.Id, Name = 'Formatter', T1C_FR__Value__c = 'gridLinkFormatter'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsRole.Id, Name = 'Label', T1C_FR__Value__c = 'Role'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsRole.Id, Name = 'LinkField', T1C_FR__Value__c = 'Id'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigAccountCoverageColumnsRole.Id, Name = 'Order', T1C_FR__Value__c = '3'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigButtonsEmailCoverageTeams.Id, Name = 'Action', T1C_FR__Value__c = 'sendCoverageEmailButton'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigButtonsEmailCoverageTeams.Id, Name = 'ClassNames', T1C_FR__Value__c = 'emailButton'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigButtonsEmailCoverageTeams.Id, Name = 'DataField', T1C_FR__Value__c = 'coverageTeamEmails'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigButtonsEmailCoverageTeams.Id, Name = 'Label', T1C_FR__Value__c = 'Email Coverage Team'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigButtonsEmailCoverageTeams.Id, Name = 'Order', T1C_FR__Value__c = '2'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigButtonsEmailCoverageTeams.Id, Name = 'Type', T1C_FR__Value__c = 'Event'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigButtonsRequestCoverage.Id, Name = 'ButtonId', T1C_FR__Value__c = 'ClickToDialFromCoverage'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigButtonsRequestCoverage.Id, Name = 'DataField', T1C_FR__Value__c = 'account.Phone'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigButtonsRequestCoverage.Id, Name = 'Label', T1C_FR__Value__c = 'Click to Dial'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigButtonsRequestCoverage.Id, Name = 'Order', T1C_FR__Value__c = '1'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigButtonsRequestCoverage.Id, Name = 'SVGIcon', T1C_FR__Value__c = '<svg style="width:20px;height:20px" /></svg>'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigButtonsRequestCoverage.Id, Name = 'URLPrefix', T1C_FR__Value__c = 'tel:'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigButtonsRequestCoverage.Id, Name = 'URLSuffix', T1C_FR__Value__c = ''),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigButtonsRequestCoverage.Id, Name = 'URLTarget', T1C_FR__Value__c = ''),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigContactCoverageColumnsEmployee.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Employee__r.Name'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigContactCoverageColumnsEmployee.Id, Name = 'Formatter', T1C_FR__Value__c = 'coverageGridEmailFormatter'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigContactCoverageColumnsEmployee.Id, Name = 'Label', T1C_FR__Value__c = 'Employee'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigContactCoverageColumnsEmployee.Id, Name = 'LinkField', T1C_FR__Value__c = 'T1C_Base__Employee__r.T1C_Base__Email__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigContactCoverageColumnsEmployee.Id, Name = 'Order', T1C_FR__Value__c = '1'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigContactCoverageColumnsLOB.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Employee__r.T1C_Base__LOB__r.Name'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigContactCoverageColumnsLOB.Id, Name = 'Label', T1C_FR__Value__c = 'LOB'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigContactCoverageColumnsLOB.Id, Name = 'Order', T1C_FR__Value__c = '2'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigContactCoverageColumnsRole.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Role__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigContactCoverageColumnsRole.Id, Name = 'Label', T1C_FR__Value__c = 'Role'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeCoverageConfigContactCoverageColumnsRole.Id, Name = 'Order', T1C_FR__Value__c = '3')
        };
        
        Id clientRTId;
        for(RecordType rt : [Select Id from RecordType where Name = 'Client' and SObjectType = 'Account' limit 1])
        {
            clientRTId = rt.Id;
        }
        
        Tier1CRMTestDataFactory dataFactory = Tier1CRMTestDataFactorySetup.getDataFactory();

        acct = dataFactory.makeAccount('Paper Street Soap Company');
        acct.RecordTypeId = clientRTId;
        acct.T1C_Base__Logo__c = '<br><img src="https://google.com/image.jpg"/>';
		insert acct;
		
        cont = new Contact(AccountId = acct.Id, FirstName  = 'Marla', LastName = 'Singer', Email = 'msinger@paperstreet.com');
        insert cont;
        
        T1C_Base__Employee__c emp = dataFactory.makeEmployee(UserInfo.getFirstName(), UserInfo.getLastName(), UserInfo.getUserId());
        insert emp;
        
        T1C_Base__Account_Coverage__c ac = dataFactory.makeAccountCoverage(acct.Id, emp.Id);
        insert ac;

        T1C_Base__Contact_Coverage__c cc = new T1C_Base__Contact_Coverage__c(T1C_Base__Contact__c = cont.Id, T1C_Base__Employee__c = emp.Id);
        insert cc;

        T1C_Base__Call_Report__c cr = new T1C_Base__Call_Report__c(T1C_Base__Client__c = acct.Id, T1C_Base__Interaction_Type__c = 'Call', T1C_Base__Subject_Meeting_Objectives__c = 'Morning Call', 
                                                                    T1C_Base__Meeting_Date__c = Date.today(), T1C_Base__Status__c = 'Completed', T1C_Base__Notes__c = 'Had a good call');
        insert cr;
       
        T1C_Base__Call_Report_Attendee_Client__c crca = new T1C_Base__Call_Report_Attendee_Client__c(T1C_Base__Call_Report__c = cr.Id, T1C_Base__Contact__c = cont.Id);
        insert crca;
        
        T1C_Base__Call_Report_Attendee_Internal__c crai = new T1C_Base__Call_Report_Attendee_Internal__c(T1C_Base__Call_Report__c = cr.Id, T1C_Base__Employee__c = emp.Id);
        insert crai;
          
        T1C_Base__Interest_Subject_Type__c ist = new T1C_Base__Interest_Subject_Type__c(Name = 'Tickers');
        insert ist;

        T1C_Base__Interest_Subject__c intSub = new T1C_Base__Interest_Subject__c(Name = 'Mayhem Corp', T1C_Base__Interest_Subject_Type__c = ist.Id);
        insert intSub;      

        T1C_Base__Interest__c interest = new T1C_Base__Interest__c(T1C_Base__Account__c = acct.Id,T1C_Base__Contact__c  = cont.Id,T1C_Base__Interest_Subject_Type__c  = ist.Id,T1C_Base__Interest_Subject__c = intSub.Id);
        insert interest;
        
        Task tsk = new Task(T1C_Base__Interaction_Type__c = 'Call', Subject = 'Morning Call', ActivityDate = Date.today().addDays(-1), Status = 'Completed', WhoId = cont.Id, WhatId = acct.Id, Description = 'Had a good call');
        insert tsk;
        
        Event evt = new Event(T1C_Base__Interaction_Type__c = 'Call', Subject = 'Morning Call', ActivityDate = Date.today().addDays(-2), StartDateTime = DateTime.now().addDays(-2), DurationInMinutes = 30, WhoId = cont.Id, WhatId = acct.Id, Description = 'Had a good call'); 
        insert evt;
    }
}

