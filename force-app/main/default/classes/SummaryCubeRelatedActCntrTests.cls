/*
 * Name:    SummaryCubeRelatedActCntrTests
 *
 * Confidential & Proprietary, 2021 Tier1 Financial Solutions Inc.
 * Property of Tier1 Financial Solutions Inc.
 * This document shall not be duplicated, transmitted or used in whole or in part
 * without written permission from Tier1 Financial Solutions.
 */
@isTest
public with sharing class SummaryCubeRelatedActCntrTests 
{
    private static Account acct;
    private static Contact cont;

    @IsTest
    static void activitiesTest()
    {
        setupData();
        
        Test.startTest();

            SummaryCubeRelatedActivityController.RecentActivityInfo activities = SummaryCubeRelatedActivityController.getRecentActivities(cont.Id, UserInfo.getUserId(), 100, 'ACE.CubeConfig.SummaryCube.RecentActivities');
            activities = SummaryCubeRelatedActivityController.getRecentActivities(acct.Id, UserInfo.getUserId(), 100, 'ACE.CubeConfig.SummaryCube.RecentActivities');

        Test.stopTest();        
    }

    static void setupData()
    {
        T1C_FR__Feature__c ACE = new T1C_FR__Feature__c(Name = 'ACE', T1C_FR__Name__c = 'ACE');
        T1C_FR__Feature__c ACESecurity = new T1C_FR__Feature__c(Name = 'Security', T1C_FR__Name__c = 'ACE.Security');
        T1C_FR__Feature__c ACESecurityDisabled = new T1C_FR__Feature__c(Name = 'DisabledTriggers', T1C_FR__Name__c = 'ACE.Security.DisabledTriggers');
        T1C_FR__Feature__c ACECubeConfig = new T1C_FR__Feature__c(Name = 'CubeConfig', T1C_FR__Name__c = 'ACE.CubeConfig');
        T1C_FR__Feature__c ACECubeConfigSummaryCube = new T1C_FR__Feature__c(Name = 'SummaryCube', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeRecentActivities = new T1C_FR__Feature__c(Name = 'RecentActivities', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.RecentActivities');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeRecentActivitiesButtons = new T1C_FR__Feature__c(Name = 'Buttons', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.RecentActivities.Buttons');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeRecentActivitiesButtonsLogInteraction = new T1C_FR__Feature__c(Name = 'LogInteraction', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.RecentActivities.Buttons.LogInteraction');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeRecentActivitiesColumns = new T1C_FR__Feature__c(Name = 'Columns', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.RecentActivities.Columns');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeRecentActivitiesColumnsDate = new T1C_FR__Feature__c(Name = 'Date', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.RecentActivities.Columns.Date');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeRecentActivitiesColumnsEmployee = new T1C_FR__Feature__c(Name = 'Employee', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.RecentActivities.Columns.Employee');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeRecentActivitiesColumnsObjectType = new T1C_FR__Feature__c(Name = 'ObjectType', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.RecentActivities.Columns.ObjectType');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeRecentActivitiesColumnsSubject = new T1C_FR__Feature__c(Name = 'Subject', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.RecentActivities.Columns.Subject');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeRecentActivitiesColumnsType = new T1C_FR__Feature__c(Name = 'Type', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.RecentActivities.Columns.Type');
        
        insert new list<T1C_FR__Feature__c>{ACE,ACESecurity,ACESecurityDisabled,ACECubeConfig,ACECubeConfigSummaryCube,ACECubeConfigSummaryCubeRecentActivities,ACECubeConfigSummaryCubeRecentActivitiesButtons,
        ACECubeConfigSummaryCubeRecentActivitiesButtonsLogInteraction,ACECubeConfigSummaryCubeRecentActivitiesColumns,ACECubeConfigSummaryCubeRecentActivitiesColumnsDate,
        ACECubeConfigSummaryCubeRecentActivitiesColumnsEmployee,ACECubeConfigSummaryCubeRecentActivitiesColumnsObjectType,
        ACECubeConfigSummaryCubeRecentActivitiesColumnsSubject,ACECubeConfigSummaryCubeRecentActivitiesColumnsType};
        
         insert new list<T1C_FR__Attribute__c>{
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACESecurityDisabled.Id, Name = 'MIGRATION', T1C_FR__Value__c = 'true'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivities.Id, Name = 'CallReportExcludedTypes', T1C_FR__Value__c = ''),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivities.Id, Name = 'CallReportLaunchEvent', T1C_FR__Value__c = 'showLogAnInteraction'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivities.Id, Name = 'CallReportTypeField', T1C_FR__Value__c = 'T1C_Base__Interaction_Type__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivities.Id, Name = 'EventExcludedTypes', T1C_FR__Value__c = ''),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivities.Id, Name = 'EventTypeField', T1C_FR__Value__c = 'T1C_Base__Interaction_Type__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivities.Id, Name = 'MeetingLaunchEvent', T1C_FR__Value__c = ''),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivities.Id, Name = 'TaskExcludedTypes', T1C_FR__Value__c = ''),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivities.Id, Name = 'TaskLaunchEvent', T1C_FR__Value__c = ''),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivities.Id, Name = 'TaskTypeField', T1C_FR__Value__c = 'T1C_Base__Interaction_Type__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivities.Id, Name = 'ViewRecords', T1C_FR__Value__c = 'TRUE'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivitiesButtonsLogInteraction.Id, Name = 'Action', T1C_FR__Value__c = 'showLogAnInteraction'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivitiesButtonsLogInteraction.Id, Name = 'ClassNames', T1C_FR__Value__c = 'logInteraction'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivitiesButtonsLogInteraction.Id, Name = 'DataField', T1C_FR__Value__c = 'contact.Id'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivitiesButtonsLogInteraction.Id, Name = 'Label', T1C_FR__Value__c = 'Log Interaction'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivitiesButtonsLogInteraction.Id, Name = 'Order', T1C_FR__Value__c = '1'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivitiesButtonsLogInteraction.Id, Name = 'Type', T1C_FR__Value__c = 'Event'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivitiesColumnsDate.Id, Name = 'DataField', T1C_FR__Value__c = 'activityDate'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivitiesColumnsDate.Id, Name = 'Formatter', T1C_FR__Value__c = 'dateFormatter'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivitiesColumnsDate.Id, Name = 'Label', T1C_FR__Value__c = 'Date'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivitiesColumnsDate.Id, Name = 'Order', T1C_FR__Value__c = '2'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivitiesColumnsDate.Id, Name = 'Type', T1C_FR__Value__c = 'Date'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivitiesColumnsEmployee.Id, Name = 'DataField', T1C_FR__Value__c = 'owner'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivitiesColumnsEmployee.Id, Name = 'Label', T1C_FR__Value__c = 'Employee'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivitiesColumnsEmployee.Id, Name = 'Order', T1C_FR__Value__c = '1'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivitiesColumnsObjectType.Id, Name = 'DataField', T1C_FR__Value__c = 'type'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivitiesColumnsObjectType.Id, Name = 'Formatter', T1C_FR__Value__c = 'sobjectIconFormatter'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivitiesColumnsObjectType.Id, Name = 'Label', T1C_FR__Value__c = ''),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivitiesColumnsObjectType.Id, Name = 'Resizable', T1C_FR__Value__c = 'FALSE'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivitiesColumnsObjectType.Id, Name = 'Width', T1C_FR__Value__c = '20'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivitiesColumnsSubject.Id, Name = 'DataField', T1C_FR__Value__c = 'subject'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivitiesColumnsSubject.Id, Name = 'Label', T1C_FR__Value__c = 'Subject'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivitiesColumnsSubject.Id, Name = 'Order', T1C_FR__Value__c = '3'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivitiesColumnsType.Id, Name = 'DataField', T1C_FR__Value__c = 'activityType'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivitiesColumnsType.Id, Name = 'Label', T1C_FR__Value__c = 'Type'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeRecentActivitiesColumnsType.Id, Name = 'Order', T1C_FR__Value__c = '1')
        };
        
        Id clientRTId;
        for(RecordType rt : [Select Id from RecordType where Name = 'Client' and SObjectType = 'Account' limit 1])
        {
            clientRTId = rt.Id;
        }
        
        Tier1CRMTestDataFactory dataFactory = Tier1CRMTestDataFactorySetup.getDataFactory();

        acct = dataFactory.makeAccount('Paper Street Soap Company');
        acct.RecordTypeId = clientRTId;
        acct.T1C_Base__Logo__c = '<br><img src="https://google.com/image.jpg"/>';
		insert acct;
		
        cont = new Contact(AccountId = acct.Id, FirstName  = 'Marla', LastName = 'Singer', Email = 'msinger@paperstreet.com');
        insert cont;
        
        T1C_Base__Employee__c emp = dataFactory.makeEmployee(UserInfo.getFirstName(), UserInfo.getLastName(), UserInfo.getUserId());
        insert emp;

        T1C_Base__Call_Report__c cr = new T1C_Base__Call_Report__c(T1C_Base__Client__c = acct.Id, T1C_Base__Interaction_Type__c = 'Call', T1C_Base__Subject_Meeting_Objectives__c = 'Morning Call', 
                                                                    T1C_Base__Meeting_Date__c = Date.today(), T1C_Base__Status__c = 'Completed', T1C_Base__Notes__c = 'Had a good call');
        insert cr;
       
        T1C_Base__Call_Report_Attendee_Client__c crca = new T1C_Base__Call_Report_Attendee_Client__c(T1C_Base__Call_Report__c = cr.Id, T1C_Base__Contact__c = cont.Id);
        insert crca;
        
        T1C_Base__Call_Report_Attendee_Internal__c crai = new T1C_Base__Call_Report_Attendee_Internal__c(T1C_Base__Call_Report__c = cr.Id, T1C_Base__Employee__c = emp.Id);
        insert crai;
                 
        Task tsk = new Task(T1C_Base__Interaction_Type__c = 'Call', Subject = 'Morning Call', ActivityDate = Date.today().addDays(-1), Status = 'Completed', WhoId = cont.Id, WhatId = acct.Id, Description = 'Had a good call');
        insert tsk;
        
        Event evt = new Event(T1C_Base__Interaction_Type__c = 'Call', Subject = 'Morning Call', ActivityDate = Date.today().addDays(-2), StartDateTime = DateTime.now().addDays(-2), DurationInMinutes = 30, WhoId = cont.Id, WhatId = acct.Id, Description = 'Had a good call'); 
        insert evt;
    }
}
