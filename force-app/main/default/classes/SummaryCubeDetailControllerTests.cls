/*
 * Name:    SummaryCubeDetailControllerTests
 *
 * Confidential & Proprietary, 2021 Tier1 Financial Solutions Inc.
 * Property of Tier1 Financial Solutions Inc.
 * This document shall not be duplicated, transmitted or used in whole or in part
 * without written permission from Tier1 Financial Solutions.
 */
@isTest
public with sharing class SummaryCubeDetailControllerTests 
{
    private static Account acct;
    private static Contact cont;

    @IsTest
    static void detailTest()
    {
        setupData();
        
        Test.startTest();

            SummaryCubeDetailController.CubeDetail detail = SummaryCubeDetailController.getInfo(acct.Id, UserInfo.getUserId(), 'ACE.CubeConfig.SummaryCube');
        
        Test.stopTest();        
    }

    static void setupData()
    {
        T1C_FR__Feature__c ACE = new T1C_FR__Feature__c(Name = 'ACE', T1C_FR__Name__c = 'ACE');
        T1C_FR__Feature__c ACESecurity = new T1C_FR__Feature__c(Name = 'Security', T1C_FR__Name__c = 'ACE.Security');
        T1C_FR__Feature__c ACESecurityDisabled = new T1C_FR__Feature__c(Name = 'DisabledTriggers', T1C_FR__Name__c = 'ACE.Security.DisabledTriggers');
        T1C_FR__Feature__c ACECubeConfig = new T1C_FR__Feature__c(Name = 'CubeConfig', T1C_FR__Name__c = 'ACE.CubeConfig');
        T1C_FR__Feature__c ACECubeConfigSummaryCube = new T1C_FR__Feature__c(Name = 'SummaryCube', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccountButtons = new T1C_FR__Feature__c(Name = 'Buttons', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.Buttons');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccountButtonsClickToDial = new T1C_FR__Feature__c(Name = 'ClickToDial', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.Buttons.ClickToDial');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccountSection = new T1C_FR__Feature__c(Name = 'Fields', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.Fields');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccountSectionAccountDetailsLink = new T1C_FR__Feature__c(Name = 'AccountDetailsLink', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.Fields.AccountDetailsLink');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccountSectionAccountName = new T1C_FR__Feature__c(Name = 'AccountName', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.Fields.AccountName');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccountSectionBillingAddress = new T1C_FR__Feature__c(Name = 'BillingAddress', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.Fields.BillingAddress');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccountSectionIndustry = new T1C_FR__Feature__c(Name = 'Industry', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.Fields.Industry');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccountSectionInVotingPeriod = new T1C_FR__Feature__c(Name = 'InVotingPeriod', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.Fields.InVotingPeriod');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccountSectionLogo = new T1C_FR__Feature__c(Name = 'Logo', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.Fields.Logo');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccountSectionPhone = new T1C_FR__Feature__c(Name = 'Phone', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.Fields.Phone');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccountSectionRecordType = new T1C_FR__Feature__c(Name = 'RecordType', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.Fields.RecordType');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccountSectionTier = new T1C_FR__Feature__c(Name = 'Tier', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.Fields.Tier');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccountSectionWebsite = new T1C_FR__Feature__c(Name = 'Website', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.Fields.Website');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeLookups = new T1C_FR__Feature__c(Name = 'Lookups', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.Lookups');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeLookupsEmployee = new T1C_FR__Feature__c(Name = 'Employee', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.Lookups.Employee');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeLookupsUser = new T1C_FR__Feature__c(Name = 'User', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.Lookups.User');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeFieldVisibilities = new T1C_FR__Feature__c(Name = 'FieldVisibilities', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.FieldVisibilities');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeFieldVisibilitiesAccountDetailsLink = new T1C_FR__Feature__c(Name = 'AccountDetailsLink', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.FieldVisibilities.AccountDetailsLink');

        insert new list<T1C_FR__Feature__c>{ACE,ACESecurity,ACESecurityDisabled,ACECubeConfig,ACECubeConfigSummaryCube,ACECubeConfigSummaryCubeAccountButtons,ACECubeConfigSummaryCubeAccountButtonsClickToDial,
        ACECubeConfigSummaryCubeAccountSection,ACECubeConfigSummaryCubeAccountSectionAccountDetailsLink,ACECubeConfigSummaryCubeAccountSectionAccountName,
        ACECubeConfigSummaryCubeAccountSectionBillingAddress,ACECubeConfigSummaryCubeAccountSectionIndustry,
        ACECubeConfigSummaryCubeAccountSectionInVotingPeriod,ACECubeConfigSummaryCubeAccountSectionLogo,ACECubeConfigSummaryCubeAccountSectionPhone,
        ACECubeConfigSummaryCubeAccountSectionRecordType,ACECubeConfigSummaryCubeAccountSectionTier,ACECubeConfigSummaryCubeAccountSectionWebsite,
        ACECubeConfigSummaryCubeLookups,ACECubeConfigSummaryCubeLookupsEmployee,ACECubeConfigSummaryCubeLookupsUser,ACECubeConfigSummaryCubeFieldVisibilities,ACECubeConfigSummaryCubeFieldVisibilitiesAccountDetailsLink};
        
         insert new list<T1C_FR__Attribute__c>{
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACESecurityDisabled.Id, Name = 'MIGRATION', T1C_FR__Value__c = 'true'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCube.Id, Name = 'ShowRelatedAddresses', T1C_FR__Value__c = 'true'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCube.Id, Name = 'RelatedAccountFieldForAddresses', T1C_FR__Value__c = 'T1C_Base__Billing_Address_Id__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCube.Id, Name = 'ObjectName', T1C_FR__Value__c = 'Account'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountButtonsClickToDial.Id, Name = 'DataField', T1C_FR__Value__c = 'account.Phone'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountButtonsClickToDial.Id, Name = 'Label', T1C_FR__Value__c = 'Click to Dial'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountButtonsClickToDial.Id, Name = 'Order', T1C_FR__Value__c = '0'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountButtonsClickToDial.Id, Name = 'SVGIcon', T1C_FR__Value__c = '<svg style="width:20px;height:20px"/></svg>'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountButtonsClickToDial.Id, Name = 'URLPrefix', T1C_FR__Value__c = 'tel:'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountButtonsClickToDial.Id, Name = 'URLTarget', T1C_FR__Value__c = ''),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionAccountDetailsLink.Id, Name = 'DataField', T1C_FR__Value__c = 'Id'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionAccountDetailsLink.Id, Name = 'Handler', T1C_FR__Value__c = 'Link'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionAccountDetailsLink.Id, Name = 'Label', T1C_FR__Value__c = 'Details...'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionAccountDetailsLink.Id, Name = 'Order', T1C_FR__Value__c = '1'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionAccountDetailsLink.Id, Name = 'Section', T1C_FR__Value__c = '5'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionAccountDetailsLink.Id, Name = 'URLPrefix', T1C_FR__Value__c = '/'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionAccountDetailsLink.Id, Name = 'URLSuffix', T1C_FR__Value__c = '?isdtp=vw'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionAccountDetailsLink.Id, Name = 'URLTarget', T1C_FR__Value__c = '_blank'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionAccountName.Id, Name = 'AccessLevel', T1C_FR__Value__c = 'Edit'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionAccountName.Id, Name = 'DataField', T1C_FR__Value__c = 'Name'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionAccountName.Id, Name = 'Handler', T1C_FR__Value__c = 'AccountNameHeader'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionAccountName.Id, Name = 'Order', T1C_FR__Value__c = '0'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionAccountName.Id, Name = 'Section', T1C_FR__Value__c = '0'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionBillingAddress.Id, Name = 'DataField', T1C_FR__Value__c = 'BillingAddress'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionBillingAddress.Id, Name = 'Handler', T1C_FR__Value__c = 'Address'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionBillingAddress.Id, Name = 'Order', T1C_FR__Value__c = '0'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionBillingAddress.Id, Name = 'Section', T1C_FR__Value__c = '4'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionBillingAddress.Id, Name = 'SecurityField', T1C_FR__Value__c = 'BillingStreet'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionBillingAddress.Id, Name = 'AddressType', T1C_FR__Value__c = 'Billing'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionIndustry.Id, Name = 'AccessLevel', T1C_FR__Value__c = 'Read'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionIndustry.Id, Name = 'DataField', T1C_FR__Value__c = 'Industry'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionIndustry.Id, Name = 'Handler', T1C_FR__Value__c = 'Picklist'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionIndustry.Id, Name = 'Order', T1C_FR__Value__c = '1'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionIndustry.Id, Name = 'Section', T1C_FR__Value__c = '1'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionInVotingPeriod.Id, Name = 'AccessLevel', T1C_FR__Value__c = 'Read'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionInVotingPeriod.Id, Name = 'DataField', T1C_FR__Value__c = 'Owner.Name'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionInVotingPeriod.Id, Name = 'Label', T1C_FR__Value__c = 'Owner'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionInVotingPeriod.Id, Name = 'Order', T1C_FR__Value__c = '1'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionInVotingPeriod.Id, Name = 'ParentField', T1C_FR__Value__c = 'OwnerId'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionInVotingPeriod.Id, Name = 'Required', T1C_FR__Value__c = 'FALSE'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionInVotingPeriod.Id, Name = 'Section', T1C_FR__Value__c = '2'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionInVotingPeriod.Id, Name = 'SecurityField', T1C_FR__Value__c = 'OwnerId'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionLogo.Id, Name = 'AdditionalClassNames', T1C_FR__Value__c = 'headerAccountLogoContainer accountLogo'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionLogo.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Logo__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionLogo.Id, Name = 'ImageField', T1C_FR__Value__c = 'T1C_Base__Logo__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionLogo.Id, Name = 'Handler', T1C_FR__Value__c = 'Image'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionLogo.Id, Name = 'Order', T1C_FR__Value__c = '10'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionLogo.Id, Name = 'Section', T1C_FR__Value__c = '0'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionPhone.Id, Name = 'DataField', T1C_FR__Value__c = 'Phone'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionPhone.Id, Name = 'Handler', T1C_FR__Value__c = 'HeaderItem'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionPhone.Id, Name = 'HelpText', T1C_FR__Value__c = 'Ths Phone Nunmber to Call them'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionPhone.Id, Name = 'Order', T1C_FR__Value__c = '3'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionPhone.Id, Name = 'Section', T1C_FR__Value__c = '0'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionRecordType.Id, Name = 'DataField', T1C_FR__Value__c = 'RecordTypeId'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionRecordType.Id, Name = 'Handler', T1C_FR__Value__c = 'RecordType'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionRecordType.Id, Name = 'Label', T1C_FR__Value__c = 'Record Type'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionRecordType.Id, Name = 'Order', T1C_FR__Value__c = '5'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionRecordType.Id, Name = 'Section', T1C_FR__Value__c = '1'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionTier.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Tier__c'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionTier.Id, Name = 'Handler', T1C_FR__Value__c = 'Picklist'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionTier.Id, Name = 'Order', T1C_FR__Value__c = '2'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionTier.Id, Name = 'Section', T1C_FR__Value__c = '1'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionWebsite.Id, Name = 'DataField', T1C_FR__Value__c = 'Website'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionWebsite.Id, Name = 'Handler', T1C_FR__Value__c = 'HeaderItem'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionWebsite.Id, Name = 'IsURL', T1C_FR__Value__c = 'TRUE'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionWebsite.Id, Name = 'order', T1C_FR__Value__c = '1'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccountSectionWebsite.Id, Name = 'Section', T1C_FR__Value__c = '0'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeFieldVisibilitiesAccountDetailsLink.Id, Name = 'CompareFieldName', T1C_FR__Value__c = 'Website'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeFieldVisibilitiesAccountDetailsLink.Id, Name = 'CompareObjectName', T1C_FR__Value__c = 'Account'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeFieldVisibilitiesAccountDetailsLink.Id, Name = 'CompareType', T1C_FR__Value__c = 'notequals'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeFieldVisibilitiesAccountDetailsLink.Id, Name = 'CompareValue', T1C_FR__Value__c = ''),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeFieldVisibilitiesAccountDetailsLink.Id, Name = 'CompareValueType', T1C_FR__Value__c = 'null'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeFieldVisibilitiesAccountDetailsLink.Id, Name = 'SubjectField', T1C_FR__Value__c = 'Id'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeFieldVisibilitiesAccountDetailsLink.Id, Name = 'SubjectObjectName', T1C_FR__Value__c = 'Account')            
        };
        
        Id clientRTId;
        for(RecordType rt : [Select Id from RecordType where Name = 'Client' and SObjectType = 'Account' limit 1])
        {
            clientRTId = rt.Id;
        }
        
        acct = new Account(Name = 'Paper Street Soap Company');
        acct.RecordTypeId = clientRTId;
        acct.T1C_Base__Logo__c = '<br><img src="https://google.com/image.jpg"/>';
		insert acct;
		
        cont = new Contact(AccountId = acct.Id, FirstName  = 'Marla', LastName = 'Singer', Email = 'msinger@paperstreet.com');
        insert cont;
    }

}
