/** 
 * Name: SummaryCubeNavBarController 
 *
 * Confidential & Proprietary, 2021 Tier1 Financial Solutions Inc.
 * Property of Tier1 Financial Solutions Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1 Financial Solutions.
 * 
 * Summary Cube Ellipsis 1.0.9
 *
 */

public with sharing class SummaryCubeNavBarController 
{
    @RemoteAction
    public static list<NavBarItem> getNavBar(String userId, String featName)
    {
        if(userId == null)
        {
            userId = UserInfo.getUserId();
        }

        T1C_FR.Feature settingsFeature = T1C_FR.featureCacheWebSvc.getAttributeTreeRecursive(userId, featName);

        if(settingsFeature != null)
        {
            NavBar nb = new NavBar(settingsFeature);

            return nb.navItems;
        }

        return null;
    }


    public class NavBar
    {   
        public list<NavBarItem> navItems;

        public NavBar(T1C_FR.Feature feat)
        {
            navItems = new list<NavBarItem>();

            if(feat != null && feat.SubFeatures != null)
            {
                for(T1C_FR.Feature feature : feat.SubFeatures)
                {
                    navItems.add(new NavBarItem(feature));
                }

                navItems.sort();
            }
        }
    }

    public class NavBarItem implements Comparable
    {   
        public String anchor;
        public String label;
        public Integer order;
        public Boolean isContact;

        public NavBarItem(T1C_FR.Feature feat)
        {
            anchor = feat.getAttributeValue('AnchorTarget', '');
            label = feat.getAttributeValue('AnchorLabel', '');
            order = feat.getIntegerAttributeValue('Order', 0);
            isContact = feat.getBooleanAttributeValue('IsContact', false);
        }

        public Integer compareTo(Object compareTo)
         {
            NavBarItem b = (NavBarItem)compareTo;           
            
            if (order > b.order)
            {
                return 1;
            }
            else if (b.order < order)
            {
                return -1;
            }

            return 0;
         }
    }


}