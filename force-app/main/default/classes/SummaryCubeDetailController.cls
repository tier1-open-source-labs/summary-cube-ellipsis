/** 
 * Name: SummaryCubeDetailController 
 *
 * Confidential & Proprietary, 2021 Tier1 Financial Solutions Inc.
 * Property of Tier1 Financial Solutions Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1 Financial Solutions.
 * 
 * Summary Cube Ellipsis 1.0.9
 *
 */

public with sharing class SummaryCubeDetailController 
{
    private static String namespace = 'T1C_Base__';//ACECoreController.NAMESPACE

    //  =========================================================
    //  Method: saveInfo
    //  Desc:   Method that updates the Account and Contact
    //  Args:   account and contact objects
    //  Return: DetailInfo      
    //  =========================================================   
    @RemoteAction
    public static String saveInfo(Account acct, Contact cont, List<String> acctNullFields, List<String> contNullFields)
    {        
        String errorMessage = '';
        
        if(acct != null && acct.Id != null)
        {
            for (String nullField : acctNullFields)
            {
                acct.put(nullField, null);
            }

            try
            {
                update acct;
            }
            catch(DmlException e)
            {
                errorMessage += e.getDmlMessage(0) + '\n';
            }
            catch(Exception e)
            {
                errorMessage += e.getMessage() + '\n';
            }
        }

        if(cont != null && cont.Id != null)
        {
            for (String nullField : contNullFields)
            {
                cont.put(nullField, null);
            }
            
            try
            {
                update cont;
            }
            catch(DmlException e)
            {
                errorMessage += e.getDmlMessage(0) + '\n';
            }
            catch(Exception e)
            {
                errorMessage += e.getMessage() + '\n';
            }
        }

        return errorMessage;
    }

    //  =========================================================
    //  Method: getInfo
    //  Desc:   Method that updates the Account and Contact
    //  Args:   record Id, UserId and Feature Name
    //  Return: CubeDetail      
    //  ========================================================= 
    @RemoteAction
    public static CubeDetail getInfo(Id recId, Id loggedInUserId, String configFeatureName) 
    {
        Boolean canEdit = false;
        Boolean getAddresses = false;
        String addressRelationshipField;
        String objectName = recId.getSobjectType().getDescribe().getName();
        String configObjectName;
        map<String, Schema.SObjectField> fieldsMap = recId.getSobjectType().getDescribe().fields.getMap();
        list<FieldInfo> fieldConfigs = new list<FieldInfo>();

        T1C_FR.Feature settingsFeature = T1C_FR.featureCacheWebSvc.getAttributeTreeRecursive(loggedInUserId , configFeatureName);
        Set<String> queryFields = new Set<String>{'Id'};

        for(UserRecordAccess ura : [Select RecordId, HasEditAccess from UserRecordAccess where UserId = :loggedInUserId and RecordId = :recId])
        {
            canEdit = ura.HasEditAccess;
        }

        if(settingsFeature != null)
        {  
            configObjectName = settingsFeature.getAttributeValue('ObjectName', null);         

            if(configObjectName != objectName)
            {
                return null;
            }

            addressRelationshipField = settingsFeature.getAttributeValue('RelatedAccountFieldForAddresses', null);
            getAddresses = settingsFeature.getBooleanAttributeValue('ShowRelatedAddresses', (addressRelationshipField != null));

            if(addressRelationshipField != null)
            {
                queryFields.add(addressRelationshipField);
            }

            T1C_FR.Feature lookupSettings = settingsFeature.SubFeatMap.get(configFeatureName + '.Lookups');
            T1C_FR.Feature fieldVisibilitySettings = settingsFeature.SubFeatMap.get(configFeatureName + '.FieldVisibilities');
            T1C_FR.Feature sectionSettings = settingsFeature.SubFeatMap.get(configFeatureName + '.Fields');

            if(fieldVisibilitySettings != null)
            {
                for(T1C_FR.Feature feat : fieldVisibilitySettings.SubFeatures)
                {
                    String compareField = feat.getAttributeValue('CompareFieldName', null);
                    String compareFieldValue = feat.getAttributeValue('CompareValueField', null);

                    queryFields.add(compareField);
                    if(compareFieldValue != null)
                    {
                        queryFields.add(compareFieldValue);
                    }
                }
            }

            T1C_FR.Feature buttonSettingsFeature = settingsFeature.SubFeatMap.get(settingsFeature.Name + '.Buttons');

            if(buttonSettingsFeature != null && !buttonSettingsFeature.SubFeatures.isEmpty())
            {
                for(T1C_FR.Feature feat : buttonSettingsFeature.SubFeatures)
                {
                    Button btn = new Button(feat);

                    if(btn.fieldName != null)
                    {
                        queryFields.add(btn.fieldName);
                    }
                }
            }
            
            if(sectionSettings != null)
            {
                for(T1C_FR.Feature feat : sectionSettings.SubFeatures)
                {
                    FieldInfo fldInfo = new FieldInfo(feat,objectName,lookupSettings,fieldVisibilitySettings,fieldsMap);
                    String fieldName = feat.getAttributeValue('DataField',null);
                    
                    if(fldInfo.enabled && fieldName != null)
                    {
                        queryFields.add(fieldName);
                        String parentField = feat.getAttributeValue('ParentField',null);
                        if(parentField != null)
                        {
                            queryFields.add(parentField);
                        }
                        fieldConfigs.add(fldInfo);
                    }
                    else if(fldInfo.enabled && fldInfo.bannerHTML != null)
                    {
                        fieldConfigs.add(fldInfo);
                    }

                    if(fldInfo.handler == 'ContactNameHeader')
                    {
                        queryFields.add('FirstName');
                        queryFields.add('LastName');
                    }

                    if(fldInfo.handler == 'Address' && fldInfo.addressTypePrefix != null)
                    {
                        queryFields.add(fldInfo.addressTypePrefix + 'City');
                        queryFields.add(fldInfo.addressTypePrefix + 'Country');
                        queryFields.add(fldInfo.addressTypePrefix + 'PostalCode');
                        queryFields.add(fldInfo.addressTypePrefix + 'State');
                        queryFields.add(fldInfo.addressTypePrefix + 'Street');
                        queryFields.add(namespace + fldInfo.addressTypePrefix + '_Address_Id__c');
                        queryFields.add(namespace + fldInfo.addressTypePrefix + '_Address_Id__r.Name');
                        queryFields.add(namespace + fldInfo.addressTypePrefix + '_Address_Id__r.' + namespace+'Address_Name__c');
                    } 
                }

                fieldConfigs.sort();
            }            
        }

        String queryString = 'Select ';
            
        for(String field : queryFields)
        {
            queryString += field + ', '; 
        }

        queryString = queryString.subString(0,queryString.length()-2);
        queryString += ' from ' + objectName + ' where Id = :recId limit 1';
        Sobject retRecord;

        for(Sobject rec : DataBase.query(queryString))
        {
            retRecord = rec;
        }

        list<SObject> addresses;

        if(getAddresses && addressRelationshipField != null && retRecord != null)
        {
            String accLookupValue = String.valueOf(retRecord.get(addressRelationshipField));

            String addressQuery = 'Select Id, ' +
                                namespace+'Address_Name__c, ' +
                                namespace+'Account__c, ' +
                                namespace+'City__c,' +
                                namespace+'Country__c, ' +
                                namespace+'Phone__c, ' +
                                namespace+'State_Province__c, ' +
                                namespace+'Street__c, ' +
                                namespace+'Zip_Postal_Code__c ' +
                                ' From ' + namespace+ 'Address__c where ' + namespace+ 'Account__c = :accLookupValue';

            addresses = Database.query(addressQuery);
        }
        

        return new CubeDetail(recId, retRecord, settingsFeature, fieldConfigs, addresses, canEdit);
    }

    //  =========================================================
    //  Class: CubeDetail
    //  Desc:   holds all the data used by the cube
    //  =========================================================
    
    public Class CubeDetail
    {
        public String t1cNS = 'T1C_Base';
        public String objectName;
        public Boolean canEdit;
        public Boolean showAnchor;
        public Boolean showAddressManagerLink = false;
        public String addressManagerLink;
        public list<SObject> addresses;
        public String recordStateName;
        public SObject record;
        public list<SectionConfig> sectionConfigs;     
        public Integer detailOrder;
        public String anchorLabel;
        public String imgURL;
        public list<FieldInfo> fieldConfig;
        public list<Button> buttons = new list<Button>();

        public CubeDetail(Id subjectId, SObject rec, T1C_FR.Feature settingsFeat, list<FieldInfo> fieldConfigs, list<SObject> addrs, Boolean canEditRec)
        {   
            objectName = subjectId.getSobjectType().getDescribe().getName();
            canEdit = canEditRec;
            t1cNS = namespace;  
            record = rec;
            addresses = addrs;

            fieldConfig = SummaryCubeDetailController.applyRecordSecurity(canEdit, fieldConfigs);

            for(FieldInfo fi : fieldConfigs)
            {
                if(fi.handler == 'Image')
                {
                    if(fi.imgField != null && record != null)
                    {
                        fi.imgURL = getImageUrl((String)record.get(fi.imgField));
                    }
                }
            }
            
            if(settingsFeat != null)
            {
                recordStateName = settingsFeat.getAttributeValue('StateName', objectName);
                detailOrder = settingsFeat.getIntegerAttributeValue('Order',0);
                anchorLabel = settingsFeat.getAttributeValue('AnchorLabel',subjectId.getSobjectType().getDescribe().getName() + ' Details');
                showAnchor = settingsFeat.getBooleanAttributeValue('ShowAnchor',true);
                showAddressManagerLink = settingsFeat.getBooleanAttributeValue('ShowAddressManagerLink',true);
                addressManagerLink = settingsFeat.getAttributeValue('AddressManagerLink','/apex/' + namespace + 'ACECoreWrapper?path=ACE.CoreWrappers.AddressManager&recordId=');
                
                sectionConfigs = new list<SectionConfig>();
                T1C_FR.Feature sectionFeature = settingsFeat.SubFeatMap.get(settingsFeat + '.SectionConfig');
                
                if(sectionFeature != null && !sectionFeature.SubFeatures.isEmpty())
                {
                    for(T1C_FR.Feature feat : sectionFeature.SubFeatures)
                    {
                        sectionConfigs.add(new SectionConfig(feat));
                    }
                }

                T1C_FR.Feature buttonSettingsFeature = settingsFeat.SubFeatMap.get(settingsFeat.Name + '.Buttons');

                if(buttonSettingsFeature != null && !buttonSettingsFeature.SubFeatures.isEmpty())
                {
                    for(T1C_FR.Feature feat : buttonSettingsFeature.SubFeatures)
                    {
                        buttons.add(new Button(feat));
                    }

                    buttons.sort();
                }
            } 
        }

        //  =========================================================
        //  Method: getImageUrl
        //  Desc:   Method that returns a the image URL
        //  Args:   image field data
        //  Return: image URL      
        //  =========================================================
        
        String getImageUrl(String url)
        {
            String returnedURL;
            if(url != null)
            {
                url = url.replaceAll('<br>', '').replaceAll('</br>', '').replace('<br/>', '');
                try
                {
                    Dom.Document doc = new Dom.Document();                
                    doc.load(url);
                    Dom.XmlNode root = doc.getRootElement();
                    if (root != null)
                    {
                        returnedURL = root.getAttributeValue('src', null);
                        if (returnedURL != null)
                        {
                              returnedURL = returnedURL.replaceAll('&amp;', '&');     
                        }
                    } 
                }
                catch (XmlException e)
                {
                    returnedURL = '';
                }
            }           
            return returnedURL;
        }
    }

    //  =========================================================
    //  Class: SectionConfig
    //  Desc:   holds all the data used by the cube
    //  =========================================================
    public Class SectionConfig
    {
        public Integer sectionNumber;
        public String additionalClassNames;

        public SectionConfig(T1C_FR.Feature sectionFeat)
        {
            sectionNumber = sectionFeat.getIntegerAttributeValue('Section',1);
            additionalClassNames = sectionFeat.getAttributeValue('AdditionalClassNames',null);
        }
    }

    //  =========================================================
    //  Class:  Button
    //  Desc:   Class that holds button Info
    //  =========================================================
    
    public Class Button implements Comparable
    {
        public Integer order;
        public String buttonId;
        public String action;
        public String fieldName;
        public String objectName;
        public String classNames;
        public String label;
        public String urlPrefix;
        public String urlSuffix;
        public String urlTarget;
        public String svgIcon;
        public String type;

        public Button(T1C_FR.Feature feat)
        {
            String dataFieldInfo = feat.getAttributeValue('DataField',null);
            Boolean enabled = feat.getBooleanAttributeValue('Enabled',true);

            if(enabled && dataFieldInfo != null)
            {
                objectName = dataFieldInfo.split('\\.')[0];

                if(dataFieldInfo.split('\\.').size()>1)
                {
                    fieldName = dataFieldInfo.split('\\.')[1];
                }
                else 
                {
                    fieldName = objectName;
                }
                
                buttonId = feat.getAttributeValue('ButtonId',null);
                action = feat.getAttributeValue('Action',null);
                type = feat.getAttributeValue('Type','URL');
                order = feat.getIntegerAttributeValue('Order', 0);
                label = feat.getAttributeValue('Label',null);
                classNames = feat.getAttributeValue('ClassNames',null);
                urlPrefix = feat.getAttributeValue('URLPrefix','/');
                urlSuffix = feat.getAttributeValue('URLSuffix','');
                urlTarget = feat.getAttributeValue('URLTarget','');
                svgIcon = feat.getAttributeValue('SVGIcon',null);             
            }
        }

        public Integer compareTo(Object compareTo)
        {
            Button b = (Button)compareTo;
            
            if (order > b.order)
            {
                return 1;
            }
            else if (b.order < order)
            {
                return -1;
            }

            return 0;           
        }
    }

    //  =========================================================
    //  Method: applyRecordSecurity
    //  Desc:   If the user can't edit a record, make all field read only
    //  Args:   image field data
    //  Return: image URL      
    //  =========================================================

    public static list<FieldInfo> applyRecordSecurity(Boolean hasEditAccess, list<FieldInfo> fieldConfigs)
    {
        for(FieldInfo fi : fieldConfigs)
        {
            if(fi.accessLevel == 'None')
            {
                continue;
            }
            
            if(!hasEditAccess)
            {
                fi.accessLevel = 'Read';
                fi.StreetAccess = 'Read';
                fi.CityAccess = 'Read';
                fi.StateAccess = 'Read';
                fi.CountryAccess = 'Read';
                fi.PostalCodeAccess = 'Read';
            }                
        }

        return fieldCOnfigs;
    } 

    //  =========================================================
    //  Class: FieldInfo
    //  Desc:  holds the config info for the fields
    //  =========================================================
    
    public Class FieldInfo implements Comparable
    {
        public Integer section;
        public Integer order;
        public String fieldName;
        public String objectName;
        public String accessLevel;
        public String handler;
        public String mode;
        public String label;
        public String additionalClassNames;
        public String helpText;
        public Schema.DescribeFieldResult controllingField;
        public Boolean required;
        public Boolean isURL;
        public String urlPrefix;
        public String urlSuffix;
        public String urlTarget;
        public String editIconSVG;
        public String mapIconSVG;
        public String bannerHTML;
        public String mapsSearchPlaceHolder;
        public Boolean hideMap = false;
        public Boolean hideMapIcon = false;
        public Boolean isLookup = false;
        public String lookupFeatureName;
        public String additionalWhereClause;
        public String lookupObjectName;
        public String lookupNameField;
        public String parentField;
        public list<String> lookupSearchFields = new list<String>{'Name'};
        public String nullPicklistLabel;
        public list<PickOption> picklistValues;
        public Boolean clearAddressFieldsOnNull = false;
        public Boolean showAddressPicker = true;
        public String addressTypePrefix;
        public String StreetAccess = 'Edit';
        public String CityAccess = 'Edit';
        public String StateAccess = 'Edit';
        public String CountryAccess = 'Edit';
        public String PostalCodeAccess = 'Edit';
        public String StreetLabel;
        public String CityLabel;
        public String StateLabel;
        public String CountryLabel;
        public String PostalCodeLabel;
        public Boolean enabled;
        public Boolean compareIsShow;
        public String compareField;
        public String compareType;
        public String compareValue;
        public String compareValueType;
        public String compareValueField;
        public String imgUrl;
        public String imgField;
        public String picklistLabelField; 
        public String applyTo;
        
        public FieldInfo(T1C_FR.Feature feat, String objName, T1C_FR.Feature lookupFeat, T1C_FR.Feature fieldVisibility, map<String, Schema.SObjectField> fieldsMap)
        {
            fieldName = feat.getAttributeValue('DataField',null);
            objectName = objName;
            enabled = feat.getBooleanAttributeValue('Enabled',true);
            bannerHTML = feat.getAttributeValue('BannerHTML',null);
            mode = feat.getAttributeValue('MultiSelectMode', 'dropdown');

            imgField = feat.getAttributeValue('ImageField', null);

            if(enabled && bannerHTML != null)
            {
                section = feat.getIntegerAttributeValue('Section',0);
                order = feat.getIntegerAttributeValue('Order', 0);
                handler = 'Banner';
            }
            else if(enabled && fieldName != null)
            {
                String flsFieldName = feat.getAttributeValue('SecurityField',null);
                String descFieldName = fieldName;
                parentField = feat.getAttributeValue('ParentField',null);
                lookupNameField = feat.getAttributeValue('LookupNameField','Name'); 
                
                if(fieldName.split('\\.').size()>0 && parentField != null)
                {
                    descFieldName = parentField;
                }

                if(flsFieldName == null)
                {
                    flsFieldName = descFieldName;
                }

                if(fieldVisibility != null)
                {
                    for(T1C_FR.Feature fieldVis : fieldVisibility.SubFeatures)
                    {
                        String subjectObjectName = fieldVis.getAttributeValue('SubjectObjectName',null);
                        String subjectField = fieldVis.getAttributeValue('SubjectField',null);

                        if(subjectObjectName != null && subjectObjectName == objName && subjectField != null && subjectField == fieldName)
                        {
                            compareIsShow = fieldVis.getBooleanAttributeValue('ShowOrHide',true);
                            compareField = fieldVis.getAttributeValue('CompareFieldName',null);
                            compareType = fieldVis.getAttributeValue('CompareType','equals');
                            compareValue = fieldVis.getAttributeValue('CompareValue',null);
                            compareValueField = fieldVis.getAttributeValue('CompareValueField',null);
                            compareValueType = fieldVis.getAttributeValue('CompareValueType','Boolean');

                            if(compareType != null)
                            {
                                compareType = compareType.toLowerCase();
                            }

                            if(compareValueType != null)
                            {
                                compareValueType = compareValueType.toLowerCase();
                            }

                            break;
                        }
                    }
                }

                Schema.SObjectField sobjFld = fieldsMap.get(descFieldName);
                Schema.SObjectField sobjSecFld = fieldsMap.get(flsFieldName);
                Schema.DescribeFieldResult fldDesc;
                Schema.DescribeFieldResult fldSecDesc;

                if(sobjFld != null)
                {
                    fldDesc = sobjFld.getDescribe();
                } 

                if(sobjSecFld != null)
                {
                    fldSecDesc = sobjSecFld.getDescribe();
                }
                    
                if((fldDesc != null && fldSecDesc != null) || fieldName.split('\\.').size()>0)
                {
                    String fldAccess = 'None';
                   
                    if(fldDesc != null)
                    {
                        if(fldSecDesc.isUpdateable())
                        {
                            fldAccess = 'Edit';
                        }
                        else if(fldSecDesc.isAccessible())
                        {
                            fldAccess = 'Read';
                        }
                        
                        required = feat.getBooleanAttributeValue('Required',!fldSecDesc.isNillable());
                        helpText = feat.getAttributeValue('HelpText',fldDesc.getInlineHelpText());
                        label = feat.getAttributeValue('Label',fldDesc.getLabel());
                        
                        if(fldDesc.getType().name() == 'REFERENCE')
                        {
                            isLookup = true;
    
                            if(fldDesc.isNamePointing() == false) 
                            {   // Name pointing is not supported on Contact and Account custom fields  
                                lookupObjectName = fldDesc.getReferenceTo()[0].getDescribe().getName();
    
                                for(T1C_FR.Feature lookupFeatConfig : lookupFeat.SubFeatures)
                                {
                                    String objectName = lookupFeatConfig.getAttributeValue('SObjectName',null);
                                    if(objectName != null && objectName == lookupObjectName)
                                    {
                                        lookupFeatureName = lookupFeatConfig.Name;
                                        additionalWhereClause = lookupFeatConfig.getAttributeValue('AdditionalWhereClause',null);
                                        
                                        String lookupSearchFieldString = lookupFeatConfig.getAttributeValue('SearchFields',null);
                                        if(lookupSearchFieldString != null)
                                        {
                                            lookupSearchFields = lookupSearchFieldString.split(',');
                                        }
                                    }
                                }
                            }
                        }
                        
                        if(fldDesc.getType().name() == 'Picklist' || fldDesc.getType().name() == 'MultiPicklist')
                        {
                            nullPicklistLabel = feat.getAttributeValue('NullPicklistLabel','-- Select One --');
                            picklistValues = new list<PickOption>();
                            for(Schema.PicklistEntry ple : fldDesc.getPicklistValues())
                            {
                                picklistValues.add(new PickOption(ple));
                            }
    
                            if(fldDesc.isDependentPicklist())
                            {
                                controllingField = fldDesc.getController().getDescribe();
                            }   
                        }
                    }
                    else if(fieldName.split('\\.').size()>0)
                    {
                        fldAccess = 'Read';
                        required = false;
                        label = feat.getAttributeValue('Label',null);
                    }
   
                    isURL = feat.getBooleanAttributeValue('IsURL',false);
                    
                    section = feat.getIntegerAttributeValue('Section',0);
                    order = feat.getIntegerAttributeValue('Order', 0);
                    accessLevel = feat.getAttributeValue('AccessLevel',fldAccess);
                    handler = feat.getAttributeValue('Handler','Input');

                    urlPrefix = feat.getAttributeValue('URLPrefix','/');
                    urlSuffix = feat.getAttributeValue('URLSuffix',null);
                    urlTarget = feat.getAttributeValue('URLTarget',null);
                    additionalClassNames = feat.getAttributeValue('AdditionalClassNames',null);

                    addressTypePrefix = feat.getAttributeValue('AddressType',null);

                    hideMap = feat.getBooleanAttributeValue('HideMap',false);
                    hideMapIcon = feat.getBooleanAttributeValue('HideMapIcon',false);
                    mapsSearchPlaceHolder = feat.getAttributeValue('MapsPlaceholder','Search Google Maps');
                    editIconSVG = feat.getAttributeValue('EditIconSVG','<svg version="1.1" style="width:14px;height:14px;cursor:pointer;" id="Capa_1" xmlns="http://www.w3.org/2000/svg" ' +
                        'xmlns:xlink="http://www.w3.org/1999/xlink" style="color:#797979" x="0px" y="0px" viewBox="0 0 528.899 528.899"' +
                        ' style="enable-background:new 0 0 528.899 528.899;" xml:space="preserve"> <g> <path fill="#797979" d="M328.883,89.125l107.59,107.589l-272.34,272.34L56.604,' +
                        '361.465L328.883,89.125z M518.113,63.177l-47.981-47.981 c-18.543-18.543-48.653-18.543-67.259,0l-45.961,45.961l107.59,107.59l53.611-53.611 C532.495,' +
                        '100.753,532.495,77.559,518.113,63.177z M0.3,512.69c-1.958,8.812,5.998,16.708,14.811,14.565l119.891-29.069 L27.473,390.597L0.3,512.69z"/></g></svg>');
                    mapIconSVG = feat.getAttributeValue('MapIconSVG','<svg style="width:20px;height:20px;cursor: pointer;" viewBox="0 0 24 24">' +
                        '<path fill="#797979" d="M15.5,4.5C15.5,5.06 15.7,5.54 16.08,5.93C16.45,6.32 16.92,6.5 17.5,6.5C18.05,6.5 18.5,6.32 18.91,5.93C19.3,5.54' + 
                        ' 19.5,5.06 19.5,4.5C19.5,3.97 19.3,3.5 18.89,3.09C18.5,2.69 18,2.5 17.5,2.5C16.95,2.5 16.5,2.69 16.1,3.09C15.71,3.5 15.5,3.97 15.5,4.5M22,4.5C22,5.5' +
                        ' 21.61,6.69 20.86,8.06C20.11,9.44 19.36,10.56 18.61,11.44L17.5,12.75C17.14,12.38 16.72,11.89 16.22,11.3C15.72,10.7 15.05,9.67 14.23,8.2C13.4,6.73 13,5.5' +
                        ' 13,4.5C13,3.25 13.42,2.19 14.3,1.31C15.17,0.44 16.23,0 17.5,0C18.73,0 19.8,0.44 20.67,1.31C21.55,2.19 22,3.25 22,4.5M21,11.58V19C21,19.5' +
                        ' 20.8,20 20.39,20.39C20,20.8 19.5,21 19,21H5C4.5,21 4,20.8 3.61,20.39C3.2,20 3,19.5 3,19V5C3,4.5 3.2,4 3.61,3.61C4,3.2 4.5,3 5,3H11.2C11.08,3.63' +
                        ' 11,4.13 11,4.5C11,5.69 11.44,7.09 12.28,8.7C13.13,10.3 13.84,11.5 14.41,12.21C15,12.95 15.53,13.58 16.03,14.11L17.5,15.7L19,14.11C20.27,12.5 20.94,11.64' +
                        ' 21,11.58M9,14.5V15.89H11.25C11,17 10.25,17.53 9,17.53C8.31,17.53 7.73,17.28 7.27,16.78C6.8,16.28 6.56,15.69 6.56,15C6.56,14.31 6.8,13.72 7.27,13.22C7.73,12.72' +
                        ' 8.31,12.47 9,12.47C9.66,12.47 10.19,12.67 10.59,13.08L11.67,12.05C10.92,11.36 10.05,11 9.05,11H9C7.91,11 6.97,11.41 6.19,12.19C5.41,12.97 5,13.91 5,15C5,16.09' +
                        ' 5.41,17.03 6.19,17.81C6.97,18.59 7.91,19 9,19C10.16,19 11.09,18.63 11.79,17.91C12.5,17.19 12.84,16.25 12.84,15.09C12.84,14.81 12.83,14.61 12.8,14.5H9Z" />' +
                        '</svg>');
                    if(handler == 'Checkbox')
                    {
                        required = false;
                    }
                    else if(handler == 'Address')
                    {
                        clearAddressFieldsOnNull = feat.getBooleanAttributeValue('ClearAddressFieldsOnNull',false);
                        fieldName = fieldName.replace('Address','');system.debug(namespace + fieldName + '_Address_Id__c'); system.debug(fieldsMap.get(namespace + fieldName + '_Address_Id__c'));
                        showAddressPicker = fieldsMap.get(namespace + fieldName + '_Address_Id__c').getDescribe().isUpdateable();
                        StreetAccess = feat.getAttributeValue('StreetAccess',fieldsMap.get(fieldName + 'Street').getDescribe().isUpdateable() ? 'Edit' : (fieldsMap.get(fieldName + 'Street').getDescribe().isAccessible() ? 'Read' : 'None'));
                        CityAccess = feat.getAttributeValue('CityAccess',fieldsMap.get(fieldName + 'City').getDescribe().isUpdateable() ? 'Edit' : (fieldsMap.get(fieldName + 'City').getDescribe().isAccessible() ? 'Read' : 'None'));
                        StateAccess = feat.getAttributeValue('StateAccess',fieldsMap.get(fieldName + 'State').getDescribe().isUpdateable() ? 'Edit' : (fieldsMap.get(fieldName + 'State').getDescribe().isAccessible() ? 'Read' : 'None'));
                        CountryAccess = feat.getAttributeValue('CountryAccess',fieldsMap.get(fieldName + 'Country').getDescribe().isUpdateable() ? 'Edit' : (fieldsMap.get(fieldName + 'Country').getDescribe().isAccessible() ? 'Read' : 'None'));
                        PostalCodeAccess = feat.getAttributeValue('PostalCodeAccess',fieldsMap.get(fieldName + 'PostalCode').getDescribe().isUpdateable() ? 'Edit' : (fieldsMap.get(fieldName + 'PostalCode').getDescribe().isAccessible() ? 'Read' : 'None'));
                        // ST-4490: Custom labels for the address subfields
                        StreetLabel = feat.getAttributeValue('StreetLabel', null);
                        CityLabel = feat.getAttributeValue('CityLabel', null);
                        StateLabel = feat.getAttributeValue('StateLabel', null);
                        CountryLabel = feat.getAttributeValue('CountryLabel', null);
                        PostalCodeLabel = feat.getAttributeValue('PostalCodeLabel', null);
                    }    
                }  
                else 
                {
                    enabled = false;
                    system.debug('Field does not exist');
                }  
                
                // todo 
                applyTo = feat.getAttributeValue('ApplyTo');
                picklistLabelField = feat.getAttributeValue('LabelField');
            }
        }

        public Integer compareTo(Object compareTo)
        {
            FieldInfo b = (FieldInfo)compareTo;
            
            if(section > b.section)
            {
                return 1;
            }
            else if(section < b.section)
            {
               return -1;
            }
            else 
            {
               if (order > b.order)
                {
                    return 1;
                }
                else if (b.order < order)
                {
                    return -1;
                }
                return 0;
            }            
        }
    }

    public Class PickOption
    {
        public String value;
        public String label;
        public Boolean isDefault;

        public PickOption(Schema.PicklistEntry pl)
        {
            if(pl.isActive())
            {
                label = pl.getLabel();
                value = pl.getValue();
                isDefault = pl.isDefaultValue();
            }
        }
    }
}