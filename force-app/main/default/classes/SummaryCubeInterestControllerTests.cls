/*
 * Name:    SummaryCubeInterestControllerTests
 *
 * Confidential & Proprietary, 2021 Tier1 Financial Solutions Inc.
 * Property of Tier1 Financial Solutions Inc.
 * This document shall not be duplicated, transmitted or used in whole or in part
 * without written permission from Tier1 Financial Solutions.
 */
@isTest
public with sharing class SummaryCubeInterestControllerTests 
{
    private static Account acct;
    private static Contact cont;

    @IsTest
    static void interestTest()
    {
        setupData();
        
        Test.startTest();

        SummaryCubeInterestController.InterestProvider intProv = SummaryCubeInterestController.getInterests(cont.Id, UserInfo.getUserId(), 'ACE.CubeConfig.SummaryCube');
        
        Test.stopTest();        
    }

    static void setupData()
    {
        T1C_FR__Feature__c ACE = new T1C_FR__Feature__c(Name = 'ACE', T1C_FR__Name__c = 'ACE');
        T1C_FR__Feature__c ACESecurity = new T1C_FR__Feature__c(Name = 'Security', T1C_FR__Name__c = 'ACE.Security');
        T1C_FR__Feature__c ACESecurityDisabled = new T1C_FR__Feature__c(Name = 'DisabledTriggers', T1C_FR__Name__c = 'ACE.Security.DisabledTriggers');
        T1C_FR__Feature__c ACECubeConfig = new T1C_FR__Feature__c(Name = 'CubeConfig', T1C_FR__Name__c = 'ACE.CubeConfig');
        T1C_FR__Feature__c ACECubeConfigSummaryCube = new T1C_FR__Feature__c(Name = 'SummaryCube', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube');

        insert new list<T1C_FR__Feature__c>{ACE,ACESecurity,ACESecurityDisabled,ACECubeConfig,ACECubeConfigSummaryCube};
        
         insert new list<T1C_FR__Attribute__c>{
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACESecurityDisabled.Id, Name = 'MIGRATION', T1C_FR__Value__c = 'true')
        };
        
        Id clientRTId;
        for(RecordType rt : [Select Id from RecordType where Name = 'Client' and SObjectType = 'Account' limit 1])
        {
            clientRTId = rt.Id;
        }
        
        acct = new Account(Name = 'Paper Street Soap Company');
        acct.RecordTypeId = clientRTId;
        acct.T1C_Base__Logo__c = '<br><img src="https://google.com/image.jpg"/>';
		insert acct;
		
        cont = new Contact(AccountId = acct.Id, FirstName  = 'Marla', LastName = 'Singer', Email = 'msinger@paperstreet.com');
        insert cont;
                
        T1C_Base__Interest_Subject_Type__c ist = new T1C_Base__Interest_Subject_Type__c(Name = 'Tickers');
        insert ist;

        T1C_Base__Interest_Subject__c intSub = new T1C_Base__Interest_Subject__c(Name = 'Mayhem Corp', T1C_Base__Interest_Subject_Type__c = ist.Id);
        insert intSub;      

        T1C_Base__Interest__c interest = new T1C_Base__Interest__c(T1C_Base__Account__c = acct.Id,T1C_Base__Contact__c  = cont.Id,T1C_Base__Interest_Subject_Type__c  = ist.Id,T1C_Base__Interest_Subject__c = intSub.Id);
        insert interest;        
    }
}
