/** 
 * Name: SummaryCubeRelatedActivityController 
 *
 * Confidential & Proprietary, 2021 Tier1 Financial Solutions Inc.
 * Property of Tier1 Financial Solutions Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1 Financial Solutions.
 * 
 * Summary Cube Ellipsis 1.0.9
 *
 */

public with sharing class SummaryCubeRelatedActivityController
{
    private static String namespace = 'T1C_Base__';//ACECoreController.NAMESPACE
    private static Boolean multiWhoEnabled = Schema.getGlobalDescribe().keySet().Contains('taskrelation');

    //  =========================================================
    //  Class:  RecentActivityInfo
    //  Desc:   Class that holds RecentActivity Data and Config
    //  =========================================================
    
    public Class RecentActivityInfo
    {
        public String t1cNS = namespace;
        public String sectionLabel;
        public Integer detailOrder;
        public Integer recordLimit;
        public list<Column> activityColumns;
        public list<Button> buttons;
        public list<RecentActivity> activities;
        public Boolean hideSection = false;
        public Boolean viewRecords = true;
        public String gridCallReportIcon;
        public String gridTaskIcon;
        public String gridEventIcon;

        public RecentActivityInfo()
        {
            activityColumns = new list<Column>();
            buttons = new list<Button>();
            activities = new list<RecentActivity>();          
        }
    }

     //  =========================================================
    //  Class:  Column
    //  Desc:   Class that holds coverage column info
    //  =========================================================
    
    public Class Column implements Comparable
    {
         public String dataField;
         public String linkField;
         public Integer orderBy;
         public String label;
         public String formatter;
         public String featureName;
         public String fieldType;
         public Boolean resizable;
         public Integer width;
         
         Column(T1C_FR.Feature column)
         {
            dataField = column.getAttributeValue('DataField');
            linkField = column.getAttributeValue('LinkField');
            orderBy = column.getIntegerAttributeValue('Order',0);
            label = column.getAttributeValue('Label','');
            formatter = column.getAttributeValue('Formatter');
            fieldType = column.getAttributeValue('Type','String');
            resizable = column.getBooleanAttributeValue('Resizable',true);
            width = column.getIntegerAttributeValue('Width',100);
            featureName = column.Name;
         }
         
         public Integer compareTo(Object compareTo)
         {
            Column b = (Column)compareTo;
            
            if (orderBy > b.orderBy)
            {
                return 1;
            }
            else if (b.orderBy < orderBy)
            {
                return -1;
            }
            return 0;
         }
    }

    //  =========================================================
    //  Class:  Button
    //  Desc:   Class that holds button config
    //  =========================================================
    
    public Class Button implements Comparable
    {
        public Integer order;
        public String buttonId;
        public String action;
        public String fieldName;
        public String objectName;
        public String classNames;
        public String label;
        public String urlPrefix;
        public String urlSuffix;
        public String urlTarget;
        public String svgIcon;
        public String type;

        public Button(T1C_FR.Feature feat)
        {
            String dataFieldInfo = feat.getAttributeValue('DataField',null);
            Boolean enabled = feat.getBooleanAttributeValue('Enabled',true);

            if(enabled && dataFieldInfo != null)
            {
                objectName = dataFieldInfo.split('\\.')[0];

                if(dataFieldInfo.split('\\.').size()>1)
                {
                    fieldName = dataFieldInfo.split('\\.')[1];
                }
                
                buttonId = feat.getAttributeValue('ButtonId',null);
                action = feat.getAttributeValue('Action',null);
                type = feat.getAttributeValue('Type','URL');
                order = feat.getIntegerAttributeValue('Order', 0);
                label = feat.getAttributeValue('Label',null);
                classNames = feat.getAttributeValue('ClassNames',null);
                urlPrefix = feat.getAttributeValue('URLPrefix','/');
                urlSuffix = feat.getAttributeValue('URLSuffix','');
                urlTarget = feat.getAttributeValue('URLTarget','');
                svgIcon = feat.getAttributeValue('SVGIcon',null);             
            }
        }

        public Integer compareTo(Object compareTo)
        {
            Button b = (Button)compareTo;
            
            if (order > b.order)
            {
                return 1;
            }
            else if (b.order < order)
            {
                return -1;
            }

            return 0;           
        }
    }



    //  =========================================================
    //  Class:  RecentActivity
    //  Desc:   Class that holds normalized Task Info
    //  =========================================================
    
    public Class RecentActivity implements Comparable
    {
        public String id;
        public String owner;
        public String subject;
        public Date activityDate;
        public String details;
        public String activityType;
        public String launchEvent;
        public String contacts;
        public SObject data;
        public String type;
        public Boolean isLaunchURL;
        
        public RecentActivity(SObject callReport, String typeField, String lEvent, Boolean isURL)
        {
            if (callReport.get(namespace + 'Meeting_Date__c') != null)
            {
            	activityDate = Date.valueOf(String.valueOf(callReport.get(namespace + 'Meeting_Date__c')));
            }
            else
            {
                activityDate = null;
            }
            activityType = String.valueOf(callReport.get(typeField));
            subject = String.valueOf(callReport.get(namespace + 'Subject_Meeting_Objectives__c'));
            details = String.valueOf(callReport.get(namespace + 'Notes__c'));
            if(callReport.getSObject('Owner') != null)
            {
                owner = String.valueOf(callReport.getSObject('Owner').get('Name'));
            }
            
            id = callReport.Id;
            launchEvent = lEvent;   
            data = callReport;
            type = namespace + 'Call_Report__c';
            contacts = '';
            isLaunchURL = isURL;
            
            for(SObject attendee : callReport.getSObjects(namespace + 'Call_Report_Attendee_Client__r'))
            {
                // US15204 Fix error Attempt to de-reference a null object for bad data 
                if (attendee.getSObject(namespace + 'Contact__r') != null)
                {
                	contacts += String.valueOf(attendee.getSObject(namespace + 'Contact__r').get('Name')) + ', ';	
                }                
            }
                
            if(contacts.length()>2)
            {
                contacts = contacts.subString(0,contacts.length()-2);
            }
        }
        
        public RecentActivity(Task tsk, String typeField, String lEvent, Boolean isURL)
        {
            activityDate = tsk.ActivityDate;
            activityType = String.valueOf(tsk.get(typeField));
            subject = tsk.Subject;
            owner = tsk.Owner.Name;     
            id = tsk.Id;
            launchEvent = lEvent;
            details = '';
            isLaunchURL = isURL;

            if(tsk.Description != null)
            {
                details = tsk.Description;
            }
            //data = (SObject)tsk;
            type = 'Task';
            
            contacts = '';
  
            if(multiWhoEnabled)
            {
                for(SObject attendee : ((SObject)tsk).getSObjects('TaskRelations'))
                {
                    contacts += String.valueOf(attendee.getSObject('Relation').get('Name')) + ', ';
                }                	
            }
            else if(tsk.WhoId != null)
            {
                contacts += tsk.Who.Name + ', '; 
            }
            
            Map<String, Object> dataMap = new Map<String, Object>(tsk.getPopulatedFieldsAsMap());
            dataMap.remove('TaskRelations');
            data = (SObject) JSON.deserialize(JSON.serialize(dataMap), Task.class);

            if(contacts.length()>2)
            {
                contacts = contacts.subString(0,contacts.length()-2);
            }
        }
        
        public RecentActivity(Event evt, String typeField, String lEvent, Boolean isURL)
        {
            activityType = String.valueOf(evt.get(typeField));
            subject = evt.Subject;          
            owner = evt.Owner.Name;     
            activityDate = evt.ActivityDate;
            id = evt.Id;
            launchEvent = lEvent;
            details = '';           
            isLaunchURL = isURL;
         
            if(evt.Description != null)
            {
                details = evt.Description;
            }
            //data = (SObject)evt;
            type = 'Event';

            contacts = '';
            
            if(multiWhoEnabled)
            {
                for(SObject attendee : ((SObject)evt).getSObjects('EventRelations'))
                {
                    contacts += String.valueOf(attendee.getSObject('Relation').get('Name')) + ', ';
                }
            }
            else if(evt.WhoId != null)
            {
                contacts += evt.Who.Name + ', '; 
            }
            
            Map<String, Object> dataMap = new Map<String, Object>(evt.getPopulatedFieldsAsMap());
            dataMap.remove('EventRelations');
            data = (SObject) JSON.deserialize(JSON.serialize(dataMap), Event.class);
                             
            if(contacts.length()>2)
            {
                contacts = contacts.subString(0,contacts.length()-2);
            }
        }
        
        public virtual Integer compareTo(Object compareTo)
        {
            RecentActivity comp = (RecentActivity)compareTo;
            
            if (activityDate != null && comp.activityDate != null)
            {
                if (activityDate > comp.activityDate)
                {
                        return -1;
                }   
                else if (activityDate < comp.activityDate)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }           
            }
         
            return 0;
        }       
    }

    @RemoteAction @ReadOnly
    public static RecentActivityInfo getRecentActivities(Id recordId, Id userId, Integer recordLimit, String featPath)
    {
        RecentActivityInfo recentActivitySectionInfo = new RecentActivityInfo();
        
        set<Id> taskIDs = new set<Id>();
        set<Id> eventIDs = new set<Id>();
        set<Id> actSecIds = new set<Id>();

        T1C_FR.Feature settingsFeature = T1C_FR.featureCacheWebSvc.getAttributeTreeRecursive(userId, featPath); 

        if(settingsFeature != null)
        {
            recentActivitySectionInfo.sectionLabel = settingsFeature.getAttributeValue('SectionLabel','{Locale.INTERACTIONS}');
            recentActivitySectionInfo.gridCallReportIcon = settingsFeature.getAttributeValue('CallReportIcon','<svg title="Call Report" style="width:20px;height:20px" viewBox="0 0 24 24"><path fill="#515151" d="M20,2A2,2 0 0,1 22,4V16A2,2 0 0,1 20,18H6L2,22V4C2,2.89 2.9,2 4,2H20M4,4V17.17L5.17,16H20V4H4M6,7H18V9H6V7M6,11H15V13H6V11Z" /></svg>');
            recentActivitySectionInfo.gridEventIcon = settingsFeature.getAttributeValue('EventIcon','<svg title="Meeting" style="width:20px;height:20px" viewBox="0 0 24 24"><path fill="#515151" d="M19,4H18V2H16V4H8V2H6V4H5C3.89,4 3,4.9 3,6V20A2,2 0 0,0 5,22H19A2,2 0 0,0 21,20V6A2,2 0 0,0 19,4M19,20H5V10H19V20M19,8H5V6H19V8M12,13H17V18H12V13Z" /></svg>');
            recentActivitySectionInfo.gridTaskIcon = settingsFeature.getAttributeValue('TaskIcon','<svg title="Task" style="width:20px;height:20px" viewBox="0 0 24 24"><path fill="#000000" d="M19,19H5V5H15V3H5C3.89,3 3,3.89 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V11H19M7.91,10.08L6.5,11.5L11,16L21,6L19.59,4.58L11,13.17L7.91,10.08Z" /></svg>');

            recentActivitySectionInfo.detailOrder = settingsFeature.getIntegerAttributeValue('Order',0);
            recentActivitySectionInfo.recordLimit = settingsFeature.getIntegerAttributeValue('RecordLimit',500);

            String taskTypeField = settingsFeature.getAttributeValue('TaskTypeField',namespace + 'Interaction_Type__c');
            String callReportTypeField = settingsFeature.getAttributeValue('CallReportTypeField',namespace + 'Interaction_Type__c');
            String eventTypeField = settingsFeature.getAttributeValue('EventTypeField',namespace + 'Interaction_Type__c');

            String callReportLaunchEvent = settingsFeature.getAttributeValue('CallReportLaunchEvent','');
            String meetingLaunchEvent = settingsFeature.getAttributeValue('MeetingLaunchEvent','');            
            String taskLaunchEvent = settingsFeature.getAttributeValue('TaskLaunchEvent','');
            
            Boolean isCallReportLaunchEventURL = settingsFeature.getBooleanAttributeValue('IsCallReportLaunchEventaURL',false);
            Boolean isMeetingLaunchEventURL = settingsFeature.getBooleanAttributeValue('IsMeetingLaunchEventaURL',false);            
            Boolean isTaskLaunchEventURL = settingsFeature.getBooleanAttributeValue('IsTaskLaunchEventaURL',false);
            
            list<String> taskExcludedTypes = excludedTypes(settingsFeature.getAttributeValue('TaskExcludedTypes',null));
            list<String> eventExcludedTypes = excludedTypes(settingsFeature.getAttributeValue('EventExcludedTypes',null));
            list<String> callReportExcludedTypes = excludedTypes(settingsFeature.getAttributeValue('CallReportExcludedTypes',null));

            recentActivitySectionInfo.viewRecords = settingsFeature.getBooleanAttributeValue('ViewRecords',true);

            T1C_FR.Feature activitiesColumnsFeature = settingsFeature.SubFeatMap.get(featPath + '.Columns');

            if(activitiesColumnsFeature != null)
            {
                for(T1C_FR.Feature feat : activitiesColumnsFeature.SubFeatures)
                {
                    recentActivitySectionInfo.activityColumns.add(new Column(feat));
                }
                
                recentActivitySectionInfo.activityColumns.sort();
            } 
            else 
            {
                recentActivitySectionInfo.hideSection = false;
            }  

            T1C_FR.Feature buttonSettingsFeature = settingsFeature.SubFeatMap.get(featPath + '.Buttons');
            
            if(buttonSettingsFeature != null)
            {
                for(T1C_FR.Feature feat : buttonSettingsFeature.SubFeatures)
                {
                    recentActivitySectionInfo.buttons.add(new Button(feat));
                }

                recentActivitySectionInfo.buttons.sort();
            } 

            for(Task t : Database.query(getTaskQueryString(taskExcludedTypes,recordId,taskTypeField,taskIDs,actSecIds)))
            {
               recentActivitySectionInfo.activities.add(new RecentActivity(t, taskTypeField, taskLaunchEvent, isTaskLaunchEventURL));            
            }

            for(Event evt : Database.query(getEventQueryString(eventExcludedTypes,recordId, eventTypeField,eventIDs,actSecIds)))
            {
                recentActivitySectionInfo.activities.add(new RecentActivity(evt, eventTypeField, meetingLaunchEvent, isMeetingLaunchEventURL));  
            }
            
            for(SObject cr : Database.query(callReportQueryString(callReportExcludedTypes,recordId,callReportTypeField)))
            {
                recentActivitySectionInfo.activities.add(new RecentActivity(cr, callReportTypeField, callReportLaunchEvent, isCallReportLaunchEventURL));
            }  

            recentActivitySectionInfo.activities.sort();       
        }
        else 
        {
            recentActivitySectionInfo.hideSection = false;
        }    
 
        return recentActivitySectionInfo;
    }

    public static String getTaskQueryString(list<String> taskExcludedTypes, String recordId, String filterField, set<Id> taskIDs, set<Id> actSecIds)
    {        
        String taskQueryString = 'Select Id, ActivityDate, Subject, Description, Owner.Name, Who.Name, ' + filterField;
        
        if(multiWhoEnabled)
        {
            taskQueryString += ', (Select Relation.Name from TaskRelations where IsWhat = false) ';
        }
        
        taskQueryString += ' from Task ';
        
        if(recordId.startsWith('001'))
        {
            taskQueryString += ' Where (AccountId =  \'' + recordId + '\'';
            
            if(!actSecIds.isEmpty())
            {
                taskQueryString += ' or WhatId in :actSecIds ';
            }
            
            if(multiWhoEnabled)
            {
                for(SObject t : Database.query('select TaskId from TaskRelation where IsWhat = false and AccountId = \'' + recordId + '\''))
                {
                    taskIDs.add(String.valueOf(t.get('TaskId')));
                }
                
                taskQueryString += ' or Id in :taskIDs';
            }
        }
        else
        {
            taskQueryString += ' Where (WhoId =  \'' + recordId + '\'';
            if(multiWhoEnabled)
            {
                for(SObject t : Database.query('select TaskId from TaskRelation where IsWhat = false and RelationId = \'' + recordId +'\''))
                {
                    taskIDs.add(String.valueOf(t.get('TaskId')));
                }
                
                taskQueryString += ' or Id in :taskIDs';
            }
        }
        
        taskQueryString += ') ';
        
        if(!taskExcludedTypes.isEmpty())
        {
            taskQueryString += ' and ' + filterField + ' not in :taskExcludedTypes ';
        }
        
        taskQueryString +=  ' order by ActivityDate desc limit :recordLimit';        

        return taskQueryString;
    }
    
    public static String getEventQueryString(list<String> eventExcludedTypes, String recordId, String filterField, set<Id> eventIDs, set<Id> actSecIds)
    {
        String queryString = 'Select Id, ActivityDate, Subject, Description, Owner.Name, Who.Name, ' + filterField;
        Boolean hasChildEvents = false;

        if(Schema.SObjectType.Event.fields.getMap().get('IsChild') != null)
        {
            hasChildEvents = true;
            queryString += ', IsChild';
        }
        
        if(multiWhoEnabled)
        {
            queryString += ', (Select Relation.Name from EventRelations where IsWhat = false) ';
        }
        
        queryString += ' from Event ';
        
        if(recordId.startsWith('001'))
        {
            queryString += ' Where (AccountId =  \'' + recordId + '\'';
            if(multiWhoEnabled)
            {
                for(SObject t : Database.query('select EventId from EventRelation where IsWhat = false and AccountId = \'' + recordId +'\''))
                {
                    eventIDs.add(String.valueOf(t.get('EventId')));
                }
                
                queryString += ' or Id in :eventIDs';               
            }
            
            if(!actSecIds.isEmpty())
            {
                queryString += ' or WhatId in :actSecIds ';
            }
        }
        else
        {
            queryString += ' Where (WhoId =  \'' + recordId + '\'';
            
            if(multiWhoEnabled)
            {
                for(SObject t : Database.query('select EventId from EventRelation where IsWhat = false and RelationId = \'' + recordId +'\''))
                {
                    eventIDs.add(String.valueOf(t.get('EventId')));
                }
                
                queryString += ' or Id in :eventIDs';               
            }
        }
        
        queryString += ') ';

        
        if(!eventExcludedTypes.isEmpty())
        {
            queryString += ' and ' + filterField + ' not in :eventExcludedTypes';
        }   
                
        if(hasChildEvents)
        {
            queryString += ' and IsChild = false ';
        }
                
        return  queryString + ' order by ActivityDate desc limit :recordLimit';
    }
    
    public static String callReportQueryString(list<String> callReportExcludedTypes, String recordId, String filterField)
    {
        String queryString = 'Select Id,';
        queryString += namespace + 'Meeting_Date__c,';
        queryString += namespace + 'Subject_Meeting_Objectives__c,';
        queryString += namespace + 'Notes__c,';
        queryString += filterField + ', ';        
        queryString += ' Owner.Name, (select ' + namespace + 'Contact__r.Name from ' + namespace + 'Call_Report_Attendee_Client__r) from ' + namespace + 'Call_Report__c ';
        
        String callReportWhereClause = '';
        Set<Id> callReportIds = new set<Id>();

        if(recordId.startsWith('001'))
        {
            String aggQueryString = 'Select ' + namespace + 'Call_Report__c cr, ' + 
                                    namespace + 'Call_Report__r.' + namespace + 'Meeting_Date__c' + 
                                    ' from ' + namespace + 'Call_Report_Attendee_Client__c where ' +
                                    namespace + 'Contact__r.AccountId = :recordId ' +
                                    ' group by ' + namespace + 'Call_Report__c, ' + namespace + 'Call_Report__r.' + namespace + 'Meeting_Date__c ' +
                                    ' order by ' + namespace + 'Call_Report__r.' + namespace + 'Meeting_Date__c desc limit 2000';

            for(AggregateResult crac : Database.query(aggQueryString))
            {
                String crId = String.valueOf(crac.get('cr'));
                if(crId != null)
                {
                    callReportIds.add(Id.valueOf(crId));
                }                
            }               
        }           
        else
        {
            String aggQueryString = 'Select ' + namespace + 'Call_Report__c cr ' + 
                                    ' from ' + namespace + 'Call_Report_Attendee_Client__c where ' +
                                    namespace + 'Contact__c = :recordId ' +
                                    ' group by ' + namespace + 'Call_Report__c limit 2000';

            for(AggregateResult crac : Database.query(aggQueryString))
            {
                String crId = String.valueOf(crac.get('cr'));
                if(crId != null)
                {
                    callReportIds.add(Id.valueOf(crId));
                }    
            }
        }   
        
        if(!callReportIds.isEmpty())
        {
            callReportWhereClause = ' where (' + namespace + 'Client__c  = :recordId or Id in (';
            for(Id crId : callReportIds)
            {
                callReportWhereClause += '\'' + crId + '\',';
            }
            callReportWhereClause = callReportWhereClause.subString(0, callReportWhereClause.length()-1) + ' ))';
        }   
        else
        {
            callReportWhereClause = ' where ' + namespace + 'Client__c  = :recordId ';
        }                           

        if(!callReportExcludedTypes.isEmpty())
        {
            callReportWhereClause += ' and ' + filterField + ' not in :callReportExcludedTypes ';
        }
        
        return queryString + callReportWhereClause + ' order by ' + namespace + 'Meeting_Date__c desc limit :recordLimit';
    }
            
    private static list<String> excludedTypes(String stringToParse)
    {
        list<String> types = new list<String>();
        
        if(stringToParse != null)
        {
            return  stringToParse.split(';');
        }
        
        return types;
    }

}