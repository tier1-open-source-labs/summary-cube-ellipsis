/** 
 * Name: SummaryCubeInterestController 
 *
 * Confidential & Proprietary, 2021 Tier1 Financial Solutions Inc.
 * Property of Tier1 Financial Solutions Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1 Financial Solutions.
 * 
 * Summary Cube Ellipsis 1.0.9
 *
 */
public with sharing class SummaryCubeInterestController 
{
    private static String namespace = 'T1C_Base__';//ACECoreController.NAMESPACE

    public Class InterestProvider
    {
        public list<SObject> interests;
        public Integer interestSectionOrder = 5;
        public String sectionLabel = '{Locale.INTERESTS}';
        public Boolean enabled = true;
        public String t1cNS = 'T1C_Base__';

        public InterestProvider(List<SObject> sobjs)
        {
            interests = sobjs;
        }
    }

    @RemoteAction  @ReadOnly
    public static InterestProvider getInterests(String recordId, String userId, String featPath)
    {
        String interestQueryString = 'select ' + namespace + 'Interest_Subject__c, ' 
                                               + namespace + 'Interest_Subject__r.Name, ' 
                                               + namespace + 'Is_Disinterest__c, ' 
                                               + namespace + 'Reason__c, ' 
                                               + namespace + 'Source__c from ' 
                                               + namespace + 'Interest__c where ';
        
        Id recId = Id.valueOf(recordId);
        
        if(recId.getSObjectType().getDescribe().getName() == 'Contact')
        {
            interestQueryString += ' ' + namespace + 'Contact__c = :recId';
        }
        else
        {
            interestQueryString += ' ' + namespace + 'Account__c = :recId and ' + namespace + 'Contact__c = null ';
        }
                
        interestQueryString += ' and ' + namespace + 'Interest_Subject__c != null and ' + namespace + 'Expired__c = false order by ' + namespace + 'Last_Discussed__c DESC NULLS LAST, Id DESC limit 10';
    
        map<String, SObject> interestUniqueness = new map<String, SObject>();

        for(SObject interest : database.query(interestQueryString))
        {
            String key = String.valueOf(interest.get(namespace + 'Interest_Subject__c'))  + ':' + String.valueOf(interest.get(namespace + 'Is_Disinterest__c'));
            interestUniqueness.put(key, interest);
        }

        T1C_FR.Feature settingsFeature = T1C_FR.featureCacheWebSvc.getAttributeTreeRecursive(userId , featPath);

        InterestProvider intProv = new InterestProvider(interestUniqueness.values());

        if(settingsFeature != null)
        {
            intProv.interestSectionOrder = settingsFeature.getIntegerAttributeValue('Order', intProv.interestSectionOrder);
            intProv.sectionLabel = settingsFeature.getAttributeValue('SectionLabel',intProv.sectionLabel);
            intProv.enabled = settingsFeature.getBooleanAttributeValue('Enabled', intProv.enabled);
        }
        
        return intProv;
    }
}