/** 
 * Name: SummaryCubeCoverageController 
 *
 * Confidential & Proprietary, 2021 Tier1 Financial Solutions Inc.
 * Property of Tier1 Financial Solutions Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1 Financial Solutions.
 * 
 * Summary Cube Ellipsis 1.0.9
 *
 */

public with sharing class SummaryCubeCoverageController 
{
    private static String namespace = 'T1C_Base__';//ACECoreController.NAMESPACE
    private static map<String, map<String, Schema.SObjectField>> fieldsMap = new map<String, map<String, Schema.SObjectField>>
    {
        namespace+'Account_Coverage__c' => Schema.getGlobalDescribe().get(namespace+'Account_Coverage__c').getDescribe().fields.getMap() 
    };
    //  =========================================================
    //  Class:  PickOption
    //  Desc:   Class that hold Picklist Info
    //  =========================================================
    
    public Class PickOption
    {
        public String value;
        public String label;
        public Boolean isDefault;

        public PickOption(Schema.PicklistEntry pl)
        {
            if(pl.isActive())
            {
                label = pl.getLabel();
                value = pl.getValue();
                isDefault = pl.isDefaultValue();
            }
        }
    }

    //  =========================================================
    //  Class:  Column
    //  Desc:   Class that holds coverage column info
    //  =========================================================
    
    public Class Column implements Comparable
    {
         public String dataField;
         public String linkField;
         public Integer orderBy;
         public String label;
         public String formatter;
         public String featureName;
         public String fieldType;
         public Boolean resizable;
         public Integer width;
         
         Column(T1C_FR.Feature column)
         {
            dataField = column.getAttributeValue('DataField');
            linkField = column.getAttributeValue('LinkField');
            orderBy = column.getIntegerAttributeValue('Order',0);
            label = column.getAttributeValue('Label','');
            formatter = column.getAttributeValue('Formatter');
            fieldType = column.getAttributeValue('Type','String');
            resizable = column.getBooleanAttributeValue('Resizable',true);
            width = column.getIntegerAttributeValue('Width',100);
            featureName = column.Name;
         }
         
         public Integer compareTo(Object compareTo)
         {
            Column b = (Column)compareTo;
            
            if (orderBy > b.orderBy)
            {
                return 1;
            }
            else if (b.orderBy < orderBy)
            {
                return -1;
            }
            return 0;
         }
    }

    //  =========================================================
    //  Class:  RecentActivity
    //  Desc:   Class that holds normalized Task Info
    //  =========================================================
    
    public Class Button implements Comparable
    {
        public Integer order;
        public String buttonId;
        public String action;
        public String fieldName;
        public String objectName;
        public String classNames;
        public String label;
        public String urlPrefix;
        public String urlSuffix;
        public String urlTarget;
        public String svgIcon;
        public String type;

        public Button(T1C_FR.Feature feat)
        {
            String dataFieldInfo = feat.getAttributeValue('DataField',null);
            Boolean enabled = feat.getBooleanAttributeValue('Enabled',true);

            if(enabled && dataFieldInfo != null)
            {
                objectName = dataFieldInfo.split('\\.')[0];

                if(dataFieldInfo.split('\\.').size()>1)
                {
                    fieldName = dataFieldInfo.split('\\.')[1];
                }
                
                buttonId = feat.getAttributeValue('ButtonId',null);
                action = feat.getAttributeValue('Action',null);
                type = feat.getAttributeValue('Type','URL');
                order = feat.getIntegerAttributeValue('Order', 0);
                label = feat.getAttributeValue('Label',null);
                classNames = feat.getAttributeValue('ClassNames',null);
                urlPrefix = feat.getAttributeValue('URLPrefix','/');
                urlSuffix = feat.getAttributeValue('URLSuffix','');
                urlTarget = feat.getAttributeValue('URLTarget','');
                svgIcon = feat.getAttributeValue('SVGIcon',null);             
            }
        }

        public Integer compareTo(Object compareTo)
        {
            Button b = (Button)compareTo;
            
            if (order > b.order)
            {
                return 1;
            }
            else if (b.order < order)
            {
                return -1;
            }

            return 0;           
        }
    }

    //  =========================================================
    //  Class:  CoverageInfo
    //  Desc:   Class that holds Coverage Data and Config
    //  =========================================================
    
    public Class CoverageInfo
    {
        public Integer coverageSectionOrder;
        public String coverageSectionLabel;
        public String acctCoverageGridLabel;
        public String contCoverageGridLabel;
        public String acctId;
        public String contId;
        public String coverageURLPrefix;
        public String coverageURLSuffix;
        public String coverageLaunchOptions;
        public String coverageLaunchTarget;
        public String acAdditionalInfoPrefix = '';
        public String ccAdditionalInfoPrefix = '';
        public list<Column> acColumns;
        public list<Column> ccColumns;
        public list<Button> buttons;
        public list<SObject> accountCoverages;
        public list<SObject> contactCoverages;
        public Boolean copyEmails = true;
        public Boolean hideSection = false;
        public String emailTooLargeError = 'Could not create email link, too many recipients';
        public String copyEmailsMessage = 'Copied Emails to clipboard';
        public String coverageTeamEmails = '';
        public String additionalTo;
        public String additionalCC;
        public String additionalBCC;
        public String emailSubject;
        public Integer coverageTeamEmailMaxLength = 2000;
        public Boolean showEmailSettings = true;
        public List<FieldInfo> filterFields; 

        public CoverageInfo()
        {
            acColumns = new list<Column>();
            ccColumns = new list<Column>();
            buttons = new list<Button>();
            accountCoverages = new list<SObject>();
            contactCoverages = new list<SObject>();  
            coverageSectionOrder = 0;          
        }
    }

    @RemoteAction  @ReadOnly
    public static CoverageInfo getCoverages(Id recordId, Id userId, Integer recordLimit, String featPath)
    {                   
        CoverageInfo covInfo = new CoverageInfo();
        Boolean getACAI = false;
        Boolean getCCAI = false;

        String acAdditionalInfoPrefix = '';
        String ccAdditionalInfoPrefix = '';

        list<FieldInfo> accountCoverageFieldConfig = new list<FieldInfo>();
        set<String> ccColumnAPINames;
        set<String> acColumnAPINames;

        String recObjectName = recordId.getSobjectType().getDescribe().getName();

        if(recObjectName == 'Contact')
        {
            covInfo.contId = recordId;

            for(Contact cont : [Select AccountId from Contact where Id = :recordId])
            {
                covInfo.acctId = cont.AccountId;
            }
        }
        else if(recObjectName == 'Account')
        {
            covInfo.acctId = recordId;
        }

        String acctId = covInfo.acctId;
        String contId = covInfo.contId;

        T1C_FR.Feature coverageSettingsFeature = T1C_FR.featureCacheWebSvc.getAttributeTreeRecursive(userId , featPath);

        String acSortBy = namespace + 'Employee__r.Name';
        Boolean acSortAsc = true;
        String ccSortBy = namespace + 'Employee__r.Name';
        Boolean ccSortAsc = true;

        set<String> accountCoverageFields = new set<String>
        {
            'Id',namespace+'Role__c'
        };

        if(coverageSettingsFeature != null)
        {
            covInfo.coverageSectionOrder = coverageSettingsFeature.getIntegerAttributeValue('Order',0);
            covInfo.coverageTeamEmailMaxLength = coverageSettingsFeature.getIntegerAttributeValue('EmailLinkMaxLength',covInfo.coverageTeamEmailMaxLength);
            covInfo.copyEmails = coverageSettingsFeature.getBooleanAttributeValue('CopyEmailsOnOverMaxLength',covInfo.copyEmails);
            covInfo.emailTooLargeError = coverageSettingsFeature.getAttributeValue('TooManyRecipientsErrorMessage',covInfo.emailTooLargeError);
            covInfo.copyEmailsMessage = coverageSettingsFeature.getAttributeValue('CopiedEmailsToClipboardMessage',covInfo.copyEmailsMessage);
            covInfo.coverageSectionLabel = coverageSettingsFeature.getAttributeValue('SectionLabel','{Locale.COVERAGE}');
            covInfo.acctCoverageGridLabel = coverageSettingsFeature.getAttributeValue('AccountCoverageGridLabel','{Locale.ACCOUNT_COVERAGE_LABEL}');
            covInfo.contCoverageGridLabel = coverageSettingsFeature.getAttributeValue('ContactCoverageGridLabel','{Locale.CONATCT_COVERAGE_LABEL}');
            covInfo.coverageURLPrefix = coverageSettingsFeature.getAttributeValue('URLPrefix','/');
            covInfo.coverageURLSuffix = coverageSettingsFeature.getAttributeValue('URLSuffix','');
            covInfo.coverageLaunchTarget = coverageSettingsFeature.getAttributeValue('URLTarget','_blank');
            covInfo.coverageLaunchOptions = coverageSettingsFeature.getAttributeValue('URLOptions','toolbar=no, menubar=no, resizable=yes width=500 height=400');

            T1C_FR.Feature accountCoverageFilters = coverageSettingsFeature.SubFeatMap.get(featPath + '.Filters'); 

            if (accountCoverageFilters != null)
            {
                for(T1C_FR.Feature feat : accountCoverageFilters.SubFeatures)
                {
                    FieldInfo fldInfo = new FieldInfo(feat,namespace+'Account_Coverage__c');
                    String fieldName = feat.getAttributeValue('DataField',null);
                    
                    if(fldInfo.enabled && fieldName != null)
                    {
                        accountCoverageFields.add(fieldName);
                        accountCoverageFieldConfig.add(fldInfo);
                    }
                }

                accountCoverageFieldConfig.sort();
                
                covInFo.filterFields = accountCoverageFieldConfig;
            }
            T1C_FR.Feature buttonSettingsFeature = coverageSettingsFeature.SubFeatMap.get(featPath + '.Buttons');
            T1C_FR.Feature ccSettingsFeature = coverageSettingsFeature.SubFeatMap.get(featPath + '.ContactCoverageColumns');
            T1C_FR.Feature acSettingsFeature = coverageSettingsFeature.SubFeatMap.get(featPath + '.AccountCoverageColumns');
            
            covInfo.additionalTo = coverageSettingsFeature.getAttributeValue('AdditionalTo','');
            covInfo.additionalCC = coverageSettingsFeature.getAttributeValue('AdditionalCC','');
            covInfo.additionalBCC = coverageSettingsFeature.getAttributeValue('AdditionalBCC','');
            covInfo.emailSubject = coverageSettingsFeature.getAttributeValue('EmailSubject','');
            covInfo.showEmailSettings = coverageSettingsFeature.getBooleanAttributeValue('ShowEmailSettings',true);

            if(buttonSettingsFeature != null)
            {
                for(T1C_FR.Feature feat : buttonSettingsFeature.SubFeatures)
                {
                    covInfo.buttons.add(new Button(feat));
                }

                covInfo.buttons.sort();
            }

            if(ccSettingsFeature != null)
            {
                getCCAI = ccSettingsFeature.getBooleanAttributeValue('GetAdditionalInfo', false);

                if(getCCAI)
                {
                    ccAdditionalInfoPrefix = namespace + 'Contact_Coverage__r.';  
                    covInfo.ccAdditionalInfoPrefix = ccAdditionalInfoPrefix;             
                }

                ccSortBy = ccAdditionalInfoPrefix + ccSortBy;

                ccSortBy = ccSettingsFeature.getAttributeValue('SortBy', ccSortBy);
                ccSortAsc = ccSettingsFeature.getBooleanAttributeValue('SortAscending' , true);

                ccColumnAPINames = new set<String>{'Id',ccAdditionalInfoPrefix + namespace + 'Employee__r.' + namespace + 'Email__c',
                                                        ccAdditionalInfoPrefix + namespace + 'Employee__r.' + namespace + 'User__c'};

                for(T1C_FR.Feature feat : ccSettingsFeature.SubFeatures)
                {
                    String columnAPIName = feat.getAttributeValue('DataField');
                    String linkFieldAPIName = feat.getAttributeValue('LinkField');
                    
                    if(!String.isEmpty(columnAPIName))
                    {
                        ccColumnAPINames.add(columnAPIName);
                    }
                    
                    if(!String.isEmpty(linkFieldAPIName))
                    {
                        ccColumnAPINames.add(linkFieldAPIName);
                    }                   
                    
                    covInfo.ccColumns.add(new Column(feat));
                }
                
                covInfo.ccColumns.sort();
            }
        
            if(acSettingsFeature != null)
            {
                getACAI = acSettingsFeature.getBooleanAttributeValue('GetAdditionalInfo', false);

                if(getACAI)
                {
                    acAdditionalInfoPrefix = namespace + 'Account_Coverage__r.';
                    covInfo.acAdditionalInfoPrefix = acAdditionalInfoPrefix;
                }

                acSortBy = acAdditionalInfoPrefix + acSortBy;

                acSortBy = acSettingsFeature.getAttributeValue('SortBy', acSortBy);
                acSortAsc = acSettingsFeature.getBooleanAttributeValue('SortAscending' , true);
                
                acColumnAPINames = new set<String>{'Id',acAdditionalInfoPrefix + namespace + 'Employee__r.' + namespace + 'Email__c',
                                                        acAdditionalInfoPrefix + namespace + 'Employee__r.' + namespace + 'User__c'};  

                for(T1C_FR.Feature feat : acSettingsFeature.SubFeatures)
                {
                    String columnAPIName = feat.getAttributeValue('DataField');
                    String linkFieldAPIName = feat.getAttributeValue('LinkField');
                    if(!String.isEmpty(columnAPIName))
                    {
                        acColumnAPINames.add(columnAPIName);
                    }
                    
                    if(!String.isEmpty(linkFieldAPIName))
                    {
                        acColumnAPINames.add(linkFieldAPIName);
                    }                   
                    
                    covInfo.acColumns.add(new Column(feat));
                }
                covInfo.acColumns.sort();
            }
        }  
        else 
        {
            covInfo.hideSection = true;
        } 
    
        if(covInfo.acctId != null && acColumnAPINames != null)
        {
            String acQuery = 'Select ';
        
            for(String field : acColumnAPINames)
            {
                acQuery += field + ',';
            }

            String objectName = namespace + 'Account_Coverage__c';

            if(getACAI)
            {
                objectName = namespace + 'Account_Coverage_Additional_Info__c';
            }

            acQuery = acQuery.subString(0,acQuery.length()-1) + ' from ' + objectName + ' where ' + acAdditionalInfoPrefix + namespace + 'Account__c = :acctId and ' 
                + acAdditionalInfoPrefix + namespace + 'Inactive__c = \'false\' and ' + acAdditionalInfoPrefix + namespace + 'Employee__r.' + namespace + 'Inactive__c = false ';
            
            if(getACAI)
            {
                acQuery += ' and ' + namespace + 'Inactive__c = \'false\'';
            }
            
            acQuery += ' order by ' + acSortBy;

            if(acSortAsc)
            {
                acQuery += ' asc ';
            }
            else 
            {
                acQuery += ' desc ';
            }

            acQuery += ' LIMIT :recordLimit';
        
            covInfo.accountCoverages = database.query(acQuery);
            
            if(covInfo.accountCoverages != null)
            {
                set<String> covTeamEmails = new set<String>();

                for(SObject ac : covInfo.accountCoverages)
                {
                    String emailField = acAdditionalInfoPrefix + namespace + 'Employee__r.' + namespace + 'Email__c';
                    list<String> fieldSplice = emailField.split('\\.');
                    Integer levels = fieldSplice.size();
                    String email;

                    if(!getACAI)
                    {
                        email = String.valueOf(ac.getSObject(fieldSplice[0]).get(fieldSplice[1]));
                    }
                    else if (getACAI)
                    {
                        email = String.valueOf(ac.getSObject(fieldSplice[0]).getSObject(fieldSplice[1]).get(fieldSplice[2]));
                    }
                    
                    if(email != null)
                    {
                        covTeamEmails.add(email);
                    }
                }

                for(String covEmail : covTeamEmails)
                {
                    covInfo.coverageTeamEmails += covEmail + ';';
                }    
            }
        }

        if(covInfo.contId != null && ccColumnAPINames != null)
        {      
            String ccQuery = 'Select ';
            
            for(String field : ccColumnAPINames)
            {
                ccQuery += field + ',';
            }

            String objectName = namespace + 'Contact_Coverage__c';

            if(getCCAI)
            {
                objectName = namespace + 'Contact_Coverage_Additional_Info__c';
            }
            
            ccQuery = ccQuery.subString(0,ccQuery.length()-1) + ' from ' + objectName + ' where ' + ccAdditionalInfoPrefix + namespace + 'Contact__c = :contId and '
            + ccAdditionalInfoPrefix + namespace + 'Inactive__c = \'false\' and ' + ccAdditionalInfoPrefix + namespace + 'Employee__r.' + namespace + 'Inactive__c = false ';
            
            if(getCCAI)
            {
                ccQuery += ' and ' + namespace + 'Inactive__c = \'false\'';
            }
            
            ccQuery += ' order by ' + ccSortBy;

            if(ccSortAsc)
            {
                ccQuery += ' asc ';
            }
            else 
            {
                ccQuery += ' desc ';
            }

            ccQuery += ' LIMIT :recordLimit';

            covInfo.contactCoverages = database.query(ccQuery);   
        }  

        return covInfo;  
    }

    //  =========================================================
    //  Method: saveEmailInfo
    //  Desc:   Method that commits the Email User Attributs
    //  Args:   additional to, cc, bcc and subject strings
    //  =========================================================   
    @RemoteAction
    public static void saveEmailInfo(String userId, String addTo, String cc, String bcc, String subject)
    {
        list<T1C_FR.Attribute> attribList = new list<T1C_FR.Attribute>
        {
            new T1C_FR.Attribute('AdditionalTo', addTo),
            new T1C_FR.Attribute('AdditionalCC', cc),
            new T1C_FR.Attribute('AdditionalBCC', bcc),
            new T1C_FR.Attribute('EmailSubject', subject)
        };

        T1C_FR.FeatureCacheWebSvc.setUserAttributes(userId, 'ACE.CubeConfig.SummaryCube.CoverageConfig', attribList);
    }


     //  =========================================================
    //  Class: FieldInfo
    //  Desc:  holds the config info for the fields
    //  =========================================================
    
    public Class FieldInfo implements Comparable
    {
        public Integer section;
        public Integer order;
        public String fieldName;
        public String objectName;
        public String handler;
        public String label;
        public String additionalClassNames;
        public String helpText;
        public Schema.DescribeFieldResult controllingField;
        public Boolean required;
        public String parentField;
        public String nullPicklistLabel;
        public list<PickOption> picklistValues;
        public Boolean enabled;

        public String picklistLabelField; 
        public String applyTo;
        
        public FieldInfo(T1C_FR.Feature feat, String objName)
        {
            fieldName = feat.getAttributeValue('DataField',null);
            objectName = objName;
            enabled = feat.getBooleanAttributeValue('Enabled',true);
                 
            applyTo = feat.getAttributeValue('ApplyTo');
            picklistLabelField = feat.getAttributeValue('LabelField');

            if(enabled && fieldName != null)
            {
                String flsFieldName = feat.getAttributeValue('SecurityField',null);
                String descFieldName = fieldName;
                parentField = feat.getAttributeValue('ParentField',null);
                
                if(fieldName.split('\\.').size()>0 && parentField != null)
                {
                    descFieldName = parentField;
                }

                if(flsFieldName == null)
                {
                    flsFieldName = descFieldName;
                }

                Schema.SObjectField sobjFld = fieldsMap.get(objName).get(descFieldName);
                Schema.SObjectField sobjSecFld = fieldsMap.get(objName).get(flsFieldName);
                Schema.DescribeFieldResult fldDesc;
                Schema.DescribeFieldResult fldSecDesc;

                if(sobjFld != null)
                {
                    fldDesc = sobjFld.getDescribe();
                } 

                if(sobjSecFld != null)
                {
                    fldSecDesc = sobjSecFld.getDescribe();
                }
                    
                if((fldDesc != null && fldSecDesc != null) || fieldName.split('\\.').size()>0)
                {
                    String fldAccess = 'None';
                   
                    if(fldDesc != null)
                    {
                        if(fldSecDesc.isUpdateable())
                        {
                            fldAccess = 'Edit';
                        }
                        else if(fldSecDesc.isAccessible())
                        {
                            fldAccess = 'Read';
                        }
                        
                        required = feat.getBooleanAttributeValue('Required',!fldSecDesc.isNillable());
                        helpText = feat.getAttributeValue('HelpText',fldDesc.getInlineHelpText());
                        label = feat.getAttributeValue('Label',fldDesc.getLabel());
                        
                        if(fldDesc.getType().name() == 'Picklist' || fldDesc.getType().name() == 'MultiPicklist')
                        {
                            nullPicklistLabel = feat.getAttributeValue('NullPicklistLabel','-- Select One --');
                            picklistValues = new list<PickOption>();
                            for(Schema.PicklistEntry ple : fldDesc.getPicklistValues())
                            {
                                picklistValues.add(new PickOption(ple));
                            }
    
                            if(fldDesc.isDependentPicklist())
                            {
                                controllingField = fldDesc.getController().getDescribe();
                            }   
                        }
                    }
                    else if(fieldName.split('\\.').size()>0)
                    {
                        fldAccess = 'Read';
                        required = false;
                        label = feat.getAttributeValue('Label',null);
                    }
   
                    section = feat.getIntegerAttributeValue('Section',0);
                    order = feat.getIntegerAttributeValue('Order', 0);
                    handler = feat.getAttributeValue('Handler','Input');

                    additionalClassNames = feat.getAttributeValue('AdditionalClassNames',null);

                    if(handler == 'Checkbox')
                    {
                        required = false;
                    }
                }  
                else 
                {
                    enabled = false;
                    system.debug('Field does not exist');
                }  
            }
        }

        public Integer compareTo(Object compareTo)
         {
            FieldInfo b = (FieldInfo)compareTo;
            
            if(section > b.section)
            {
                return 1;
            }
            else if(section < b.section)
            {
               return -1;
            }
            else 
            {
               if (order > b.order)
                {
                    return 1;
                }
                else if (b.order < order)
                {
                    return -1;
                }
                return 0;
            }
            
         }
    }

}