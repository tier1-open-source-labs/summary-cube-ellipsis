/*
 * Name:    SummaryCubeNavBarControllerTests
 *
 * Confidential & Proprietary, 2021 Tier1 Financial Solutions Inc.
 * Property of Tier1 Financial Solutions Inc.
 * This document shall not be duplicated, transmitted or used in whole or in part
 * without written permission from Tier1 Financial Solutions.
 */
@isTest
public with sharing class SummaryCubeNavBarControllerTests 
{
    private static Account acct;
    private static Contact cont;

    @IsTest
    static void interestTest()
    {
        setupData();
        
        Test.startTest();

        list<SummaryCubeNavBarController.NavBarItem> navItems = SummaryCubeNavBarController.getNavBar(UserInfo.getUserId(), 'ACE.CubeConfig.SummaryCube');
        
        Test.stopTest();        
    }


    static void setupData()
    {
        T1C_FR__Feature__c ACE = new T1C_FR__Feature__c(Name = 'ACE', T1C_FR__Name__c = 'ACE');
        T1C_FR__Feature__c ACESecurity = new T1C_FR__Feature__c(Name = 'Security', T1C_FR__Name__c = 'ACE.Security');
        T1C_FR__Feature__c ACESecurityDisabled = new T1C_FR__Feature__c(Name = 'DisabledTriggers', T1C_FR__Name__c = 'ACE.Security.DisabledTriggers');
        T1C_FR__Feature__c ACECubeConfig = new T1C_FR__Feature__c(Name = 'CubeConfig', T1C_FR__Name__c = 'ACE.CubeConfig');
        T1C_FR__Feature__c ACECubeConfigSummaryCube = new T1C_FR__Feature__c(Name = 'SummaryCube', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeAccount = new T1C_FR__Feature__c(Name = 'Account', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.Account');
        T1C_FR__Feature__c ACECubeConfigSummaryCubeContact = new T1C_FR__Feature__c(Name = 'Contact', T1C_FR__Name__c = 'ACE.CubeConfig.SummaryCube.Contact');

        insert new list<T1C_FR__Feature__c>{ACE,ACESecurity,ACESecurityDisabled,ACECubeConfig,ACECubeConfigSummaryCube,ACECubeConfigSummaryCubeAccount,ACECubeConfigSummaryCubeContact};
        
         insert new list<T1C_FR__Attribute__c>{
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACESecurityDisabled.Id, Name = 'MIGRATION', T1C_FR__Value__c = 'true'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccount.Id, Name = 'AnchorTarget', T1C_FR__Value__c = 'AccountDetails'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccount.Id, Name = 'AnchorLabel', T1C_FR__Value__c = 'Account'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccount.Id, Name = 'Order', T1C_FR__Value__c = '2'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeAccount.Id, Name = 'IsContact', T1C_FR__Value__c = 'false'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeContact.Id, Name = 'AnchorTarget', T1C_FR__Value__c = 'ContactDetails'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeContact.Id, Name = 'AnchorLabel', T1C_FR__Value__c = 'Contact'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeContact.Id, Name = 'Order', T1C_FR__Value__c = '1'),
            new T1C_FR__Attribute__c(T1C_FR__Feature__c = ACECubeConfigSummaryCubeContact.Id, Name = 'IsContact', T1C_FR__Value__c = 'true')
        };                
    }
}
