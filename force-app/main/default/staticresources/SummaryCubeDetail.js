/**
 * Name: SummaryCubeDetail.js 
 *
 * Confidential & Proprietary, 2021 Tier1 Financial Solutions Inc.
 * Property of Tier1 Financial SolutionsInc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1 Financial Solutions.
 */
//=============================================================================
(function($)	
{			
	var _super = ACE.inherit(SummaryCubeDetail, ALM.AbstractCubeFace);

	/**
	 * Constructor
	 *
	 * @class      SummaryCubeDetail (ellipsis context)
	 */
	
	function SummaryCubeDetail(context)
	{
		_super.constructor.call(this, context);
		
		this._loadLayout(context);
		this._context = context;
		this.config = context.options;
		this.mapsKey = context.options.mapsKey;
		var _this = this;

		if(typeof sforce === 'undefined')
		{
			$("head").append('<script type="text/javascript" src="/soap/ajax/30.0/connection.js" />');
			$("head").append('<script type="text/javascript" src="/soap/ajax/30.0/apex.js" />');				
		}

		if(typeof google === 'undefined')
		{
			if(this.mapsKey == null)
			{
				this.mapsKey = 'AIzaSyAV0QZNL1-69Raqo9XxZTs9EBkSG1Nue24';
			}
			try{
				$("head").append('<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=' + this.mapsKey + '&libraries=places" />');
			}
			catch (error)
			{
				//
			}
			
		}

		this._context.local.eventDispatcher.on('ec-CubePopulate', function(e, eventData)
        {
			if(eventData != null)
			{
				_this.selectedItem = eventData.data;
				_this.selectedType = eventData.type;
				_this.setData(eventData.data);  
			}			   
		});

		this._context.global.eventDispatcher.on("saveSummaryCubeData", function(e, eventData)
		{
			_this.showButtonContainer(false);
		});
		
		this._context.global.eventDispatcher.on('cancelSummaryCube', function(e, eventData)
		{
			_this.showButtonContainer(false);
			_this._context.local.state.put("Contact" + "Updated", false);
			_this._context.local.state.put("Account" + "Updated", false);
			_this.setData(_this.dataProvider);
		});

		this.featurePath = this.config.path;
		this.subjectObject = this.config.object;

        this._context.readyHandler();
	}

	SummaryCubeDetail.prototype.setupRelatedListSectionHeader = function(order,label,type)
	{
		if(order == null)
		{
			return null;
		}

		var thisRelatedSectionHeader = this.componentMap[type];
		if(thisRelatedSectionHeader == null)
		{
			thisRelatedSectionHeader = $('<div Id="' + type.toLowerCase() + 'Details" order="' + order + '" class="section sectionDraggable ' + type.toLowerCase() + 'Section"></div>').appendTo(this.cubeContent);
			var sectHeader = $('<div class="sectionHeader ' + type + 'Header"></div>').appendTo(thisRelatedSectionHeader);
			var titleContainer = $('<div class="headerItem headerTitleContainer"></div>').appendTo(sectHeader);
			$('<div class="primaryLabel">' + label + '</div>').appendTo(titleContainer);
			var sectionActionButtons = $('<div class="headerItem actionButtons"></div>').appendTo(sectHeader);

			this.componentMap[type] = thisRelatedSectionHeader;
			this.buttonSectionMap[type] = sectionActionButtons;
		}

		return thisRelatedSectionHeader;
	}
	
	SummaryCubeDetail.prototype.setupSectionHeader = function(config, objectName)
	{
		if(config == null)
		{
			return null;
		}

		var order = config.detailOrder;

		if(this.sections[objectName + 'Section'] == null)
		{
			this.sections[objectName + 'Section'] = {};

			var thisSection = $('<div Id="' + objectName.toLowerCase() + 'Details" order="' + order + '" class="section ' + objectName.toLowerCase() + 'Summary"></div>').appendTo(this.cubeContent);
			this.sections[objectName + 'Section']['mainDiv'] = thisSection;
			
			var thisSectionHeader = $('<div class="sectionHeader ' + objectName + 'Header"></div>').appendTo(thisSection);
			this.sections[objectName + 'Section'][objectName + 'Header'] = thisSectionHeader;

			var thisSectionTitle = $('<div class="headerItem headerTitleContainer"></div>').appendTo(thisSectionHeader);
			this.sections[objectName + 'Section'][objectName + 'HeaderTitleContainer'] = thisSectionTitle;

			this.buttonSectionMap[objectName] = $('<div class="headerItem actionButtons"></div>').appendTo(thisSectionHeader);
		}

		return this.sections[objectName + 'Section'];
	} 

	SummaryCubeDetail.prototype.setupSection = function(config, objectName)
	{
		if(config == null || this.config.object != config.objectName)
		{
			return null;
		}

		var sectionName = objectName + 'Section';
		var section =  this.sections[sectionName];

		var thisRecord = config.record;

		var buttonSection = this.buttonSectionMap[objectName];

		if(thisRecord == null)
		{
			$(section).hide();
			return null;
		}
		else
		{
			$(section).show();
		}

		for(var x=0;x<config.fieldConfig.length;x++)
		{
			var thisFieldConfig = config.fieldConfig[x];
			thisFieldConfig.record = thisRecord;
			thisFieldConfig.options = {property : thisFieldConfig.objectName + '.' + thisFieldConfig.fieldName, value : null};

			if(thisFieldConfig.section == 0)
			{
				var handler = this.componentHandlers[thisFieldConfig.handler];
				handler(this,this.sections[sectionName][objectName + 'HeaderTitleContainer'],thisFieldConfig);
			}
			else
			{
				var thisSection = this.sections[sectionName][thisFieldConfig.section];
				
				var additionalClassNames = '';
				var sectionConfigs = objectName + 'SectionConfigs';

				if(config[sectionConfigs] != null)
				{
					var thisSectionConfigs = config[sectionConfigs];
					for(var i=0;i<thisSectionConfigs.length;i++)
					{
						var sectConf = thisSectionConfigs[i];
						if(thisFieldConfig.section == sectConf.sectionNumber)
						{
							additionalClassNames = sectConf.additionalClassNames;
						}
					}
				}

				if(thisSection == null)
				{
					thisSection = createDetailSectionContainer(thisFieldConfig.section,section.mainDiv,this,objectName,additionalClassNames);
					this.sections[sectionName][thisFieldConfig.section] = thisSection;
				}

				var handler = this.componentHandlers[thisFieldConfig.handler];
				if(handler == null)
				{
					handler = createInput;
				}
				handler(this,thisSection,thisFieldConfig);
			}
		}
		
		var buttons = config.buttons;

		var _this = this;
		
		$.each(buttons,function(key, value)
		{
			createButton(_this,buttonSection,value);
		});

		if(buttons.length == 0)
		{
			$(buttonSection).css({'maxWidth':0,'minWidth':0});
		}
		else
		{
			var buttonSectionWitdh = buttons.length * 30;
			$(buttonSection).css({'maxWidth':buttonSectionWitdh,'minWidth':buttonSectionWitdh});
		}
	} 

	function createDetailSectionContainer(divId,section,_this,objectName,additionalClassNames)
	{
		if(divId == null)
		{
			return createDetailSectionContainer;
		}
		
		var thisDetailSection = _this.containerMap[objectName + divId]; 
		if(thisDetailSection == null)
		{
			thisDetailSection = $('<div class="' + additionalClassNames + ' subSection detailComponents">').appendTo(section);
			_this.containerMap[objectName + divId] = thisDetailSection;
		}	
		
		return thisDetailSection;
	}

	function createBanner(_this,component,config)
	{
		if(component == null || config == null || config.accessLevel == 'None')
		{
			return null;
		}
		
		var elementId = config.section + ':' + config.order + ':Banner';
		var banner = _this.componentMap[elementId];
		
		if(banner == null)
		{
			// Only set on first load since it is always the same
			banner = $(config.bannerHTML).appendTo(component);
			_this.componentMap[elementId] = banner;
		}

		if(config.compareIsShow)
		{
			var show = getVisibility(config, _this);

			if(show)
			{
				$(banner).show();
			}
			else
			{
				$(banner).hide();
			}
		}

		return banner;
	}

	function createLink(_this,component, config)
	{
		if(component == null || config == null || config.accessLevel == 'None')
		{
			return null;
		}
		
		var value = getValue(config);
		var label = config.label;
		var elementId = getElementId(config);
		var urlPrefix = config.urlPrefix;
		var urlSuffix = config.urlSuffix;
		var urlTarget = config.urlTarget;

		if(urlTarget == null)
		{
			urlTarget = '';
		}

		if(urlSuffix == null)
		{
			urlSuffix = '';
		}

		var thisInput = _this.componentMap[elementId + 'Link'];

		if(thisInput == null)
		{
			var subSectionItem = $('<div id="' + elementId + 'subSectionItem" class="componentContainer subSectionItem"></div>').appendTo(component);
			var editContainer = $('<div  id="' + elementId + 'editContainer" class="cubeComponentContainer"></div>').appendTo(subSectionItem);
		
			thisInput = $('<a id="' + elementId + 'Link" class="detailsLink" target="' + urlTarget + '" href="' + urlPrefix + value + urlSuffix + '">' + label + '</a>').appendTo(editContainer);
			_this.componentMap[elementId + 'Link'] = thisInput;
		}
		else
		{
			$(thisInput).prop('href', urlPrefix + value + urlSuffix);
			$(thisInput).text(label);
		}

		if(config.compareIsShow)
		{
			var show = getVisibility(config, _this);

			if(show)
			{
				$(thisInput).show();
			}
			else
			{
				$(thisInput).hide();
			}
		}
	
		return thisInput;
	}

	// Function to create the Contact Name Header
	function createContactNameHeaderItem(_this,component, config)
	{
		if(component == null || config == null || config.accessLevel == 'None')
		{
			return null;
		}
		else
		{
			var disabled = (config.accessLevel == 'Read');

			var elementId = config.fieldName + ':' + config.objectName;;
			var elementIdHeader = elementId + ':Header';
			var labelEditContainer  = _this.componentMap[elementIdHeader + 'labelEditContainer'];
			var nameHiddenDiv = _this.componentMap[elementIdHeader + 'NameHiddenDiv'];

			var firstNameComponent = _this.componentMap[elementIdHeader + 'firstNameComponent'];
			var fnEditContainer = _this.componentMap[elementIdHeader + 'fnEditContainer'];
			var lastNameComponent = _this.componentMap[elementIdHeader + 'lastNameComponent'];
			var lnEditContainer = _this.componentMap[elementIdHeader + 'lnEditContainer'];

			if(labelEditContainer == null)
			{
				labelEditContainer = $('<div id="labelEditContainer" class="editableLabelContainer" id="' +config.fieldName + config.objectName + '"></div>').appendTo(component);	
				_this.componentMap[elementIdHeader + 'labelEditContainer']= labelEditContainer;
				
				nameHiddenDiv = $('<div id="nameHiddenDiv" style="display:none;" class="hiddenSubSection detailComponents"></div>').appendTo(component);
				_this.componentMap[elementIdHeader + 'NameHiddenDiv']= nameHiddenDiv;
				
				firstNameComponent = $('<div id="firstNameComponent" class="componentContainer subSectionItem"></div>').appendTo(nameHiddenDiv);			
				fnEditContainer = $('<div id="fnEditContainer" class="cubeComponentContainer"></div>').appendTo(firstNameComponent);
				_this.componentMap[elementIdHeader + 'firstNameComponent'] = firstNameComponent;
				_this.componentMap[elementIdHeader + 'fnEditContainer'] = fnEditContainer;

				lastNameComponent = $('<div id="lastNameComponent" class="componentContainer subSectionItem"></div>').appendTo(nameHiddenDiv);			
				lnEditContainer = $('<div id="lnEditContainer" class="cubeComponentContainer"></div>').appendTo(lastNameComponent);
				_this.componentMap[elementIdHeader + 'lastNameComponent'] = lastNameComponent;
				_this.componentMap[elementIdHeader + 'lnEditContainer'] = lnEditContainer;
			}
			
			var itemValue = _this.componentMap[elementIdHeader];

			if(itemValue == null)
			{
				itemValue = $('<div class="primaryLabel">' + config.record.Name +'</div>').appendTo(labelEditContainer);
				$(itemValue).attr('disabled',disabled);

				if(!disabled)
				{
					$(itemValue).addClass('editable');
				}

				_this.componentMap[elementIdHeader] = itemValue;
				
				$(itemValue).click(function()
				{		
					var isDisabled = ($(this).attr('disabled'));								
					if(isDisabled != 'disabled' || isDisabled == false)
					{
						var thisContainer = $(nameHiddenDiv)[0];
						if(thisContainer != null && thisContainer.style.display != 'none')
						{
							$(thisContainer).hide(333);
							$(thisContainer).removeClass('showHiddenContainer');
						}
						else
						{
							$(thisContainer).show(333);
							$(thisContainer).addClass('showHiddenContainer');
						}					
					}	
				});		
			}
			else
			{									
				$(itemValue).text(config.record.Name);
				$(itemValue).attr('disabled',disabled);
			}	
			
			if(!config.compareIsShow)
			{
				var compareCanEdit = getVisibility(config, _this);

				disabled = !compareCanEdit;	
			}

			var fnInput = _this.inputMap['FirstName:Contact'];
		
			if(fnInput == null)
			{
				var fieldDescResult = getFieldDescribeResult(config,'FirstName',_this);

				var fnConfig = config;
				fnConfig.label = fieldDescResult.label;
				fnConfig.fieldName = 'FirstName';

				fnInput = createInput(_this,fnEditContainer,fnConfig);
			}
			else
			{
				fnInput.value = config.record.FirstName;
				$(fnInput).prop('title',config.record.FirstName);				
			}

			var lnInput = _this.inputMap['LastName:Contact'];
		
			if(lnInput == null)
			{
				var fieldDescResult = getFieldDescribeResult(config,'LastName',_this);

				var lnConfig = config;
				lnConfig.label = fieldDescResult.label;
				lnConfig.fieldName = 'LastName';

				lnInput = createInput(_this,fnEditContainer,lnConfig);
			}
			else
			{
				lnInput.value = config.record.LastName;
				$(lnInput).prop('title',config.record.LastName);
			}

						
			if(!disabled)
			{
				$(itemValue).addClass('editable');
				fnInput.control[0].readOnly = false;
				lnInput.control[0].readOnly = false;
			}
			else
			{
				$(itemValue).removeClass('editable');
				fnInput.control[0].readOnly = true;
				lnInput.control[0].readOnly = true;
			}
		}
	}

	// Function to create the Account Name Header
	function createAccountNameHeaderItem(_this,component,config)
	{
		if(component == null || config == null)
		{
			return null;
		}
		else
		{
			var disabled = (config.accessLevel == 'Read');

			if(!config.compareIsShow)
			{
				var compareCanEdit = getVisibility(config, _this);

				disabled = !compareCanEdit;				
			}

			var elementId = config.fieldName + ':' + config.objectName;
			var thisElement = _this.componentMap[elementId + ':Header'];
			var editContainer = _this.componentMap[elementId + ':EditContainer'];
			if(thisElement == null)
			{
				thisElement = $('<div id="' + elementId + 'AccountNamelabelEditContainer" class="editableLabelContainer"></div>').appendTo(component);
				var labelHiddenDiv = $('<div id="' + elementId + 'AccountNamelabelHiddenDiv" class="hiddenSubSection detailComponents"></div>').appendTo(component);
				var labelComponent = $('<div id="' + elementId + 'AccountNamelabelComponent" class="componentContainer subSectionItem"></div>').appendTo(labelHiddenDiv);		
				editContainer = $('<div id="' + elementId + 'AccountNameeditContainer" class="cubeComponentContainer"></div>').appendTo(labelComponent);
				$(labelHiddenDiv).hide();

				_this.componentMap[elementId + ':Header'] = thisElement;
				_this.componentMap[elementId + ':EditContainer'] = editContainer;
			}

			var itemValue = _this.componentMap[elementId + ':itemValue'];
			if(itemValue == null)
			{
				itemValue = $('<div class="primaryLabel">' + config.record.Name +'</div>').appendTo(thisElement);
				_this.componentMap[elementId + ':itemValue'] = itemValue;

				if(!disabled)
				{
					$(itemValue).addClass('editable');
				}

				$(itemValue).click(function()
				{	
					var isDisabled = $(this).attr('disabled');									
					if(!isDisabled)
					{
						var thisContainer = $(labelHiddenDiv)[0];
						if(thisContainer != null && thisContainer.style.display != 'none')
						{
							$(thisContainer).hide(333);
							$(thisContainer).removeClass('showHiddenContainer');
						}
						else
						{
							$(thisContainer).show(333);
							$(thisContainer).addClass('showHiddenContainer');
						}
					}	
				});	
			}
			else
			{
				$(itemValue).text(config.record.Name);
			}

			var editInput = _this.inputMap[elementId];
		
			if(editInput == null)
			{	
				editInput = createInput(_this,editContainer,config);
				_this.inputMap[elementId] = editInput;
			}
			else
			{
				editInput.value = config.record.Name;
			}
			
			$(editInput).change(function()
			{
				var inputVal = this.value;	

				var titleValue = inputVal;
				var labelValue = titleValue;

				if(inputVal == '')
				{
					labelValue = config.label;					
					titleValue = '';
				}

				this.customTitle = titleValue;

				if(config.isURL)
				{
					$(itemValue).html(labelValue);
				}
				else
				{
					$(itemValue).text(labelValue);
				}
			});

			$(itemValue).attr('disabled',disabled);
			$(editInput).attr('disabled',disabled);
			$(editInput).prop('title',config.record.Name);

			if(!disabled)
			{
				$(itemValue).addClass('editable');
				editInput.control[0].readOnly = false;
			}
			else
			{
				$(itemValue).removeClass('editable');
				editInput.control[0].readOnly = true;
			}
		}
	}

	function createHeaderItem(_this,component,config)
	{
		if(config == null || config.fieldName == null || config.accessLevel == 'None')
		{
			return createHeaderItem;
		}

		var value = config.record[config.fieldName];
		var inputValue = value;
		var disabled = (config.accessLevel == 'Read');
		var hasValue = (value != null && value.length > 0);
		var hideElement = disabled && !hasValue;
		
		if(!config.compareIsShow)
		{
			var compareCanEdit = getVisibility(config, _this);

			disabled = !compareCanEdit;			
		}

		if(!disabled && !hasValue)
		{
			inputValue = config.label;			
		}
		else if(!hasValue)
		{
			inputValue = '';
		}

		if(value == null || value == undefined)
		{
			value = '';
		}

		var linkValue = inputValue;
		
		if(hasValue && config.isURL)
		{
			if(value.substr(0,4) != 'http')
			{
				linkValue  = '<a class="detailsLink" href="http://'+ value + '" target="_blank">'+ value + '</a>';	
			}	
			else
			{
				linkValue  = '<a class="detailsLink" href="'+ value + '" target="_blank">'+ value + '</a>';	
			}
		}

		var elementId = config.fieldName + ':' + config.objectName;

		var labelEditContainer  = _this.componentMap[elementId + ':Header'];
		var labelHiddenDiv = _this.componentMap[elementId + ':HeaderNameHiddenDiv'];
		var labelComponent = _this.componentMap[elementId + ':HeaderlabelComponent'];
		var editContainer = _this.componentMap[elementId + ':HeadereditContainer'];	

		if(labelEditContainer == null)
		{	
			labelEditContainer = $('<div id="' +elementId + 'labelEditContainer" class="editableLabelContainer"></div>').appendTo(component);
			_this.componentMap[elementId + ':Header']= labelEditContainer;
			
			labelHiddenDiv = $('<div id="' +elementId + 'labelHiddenDiv" class="hiddenSubSection detailComponents"></div>').appendTo(component);
			_this.componentMap[elementId + ':HeaderNameHiddenDiv'] = labelHiddenDiv;
			
			labelComponent = $('<div title="' + config.helpText  + '" id="' +elementId + 'labelComponent" class="componentContainer subSectionItem"></div>').appendTo(labelHiddenDiv);	
			_this.componentMap[elementId + ':HeaderlabelComponent'] = labelComponent;

			editContainer = $('<div id="' +elementId + 'editContainer" class="cubeComponentContainer"></div>').appendTo(labelComponent);
			_this.componentMap[elementId + ':HeadereditContainer'] = editContainer;

			$(labelHiddenDiv).hide();
			
			if(config.required)
			{
				$(labelEditContainer).addClass('required');
			}
		}
		
		if(hideElement)
		{
			$(labelEditContainer).hide();
		}
		else
		{
			$(labelEditContainer).show();
		}

		var itemValue  = _this.componentMap[elementId + ':itemValue'];
		if(itemValue == null)
		{
			itemValue = $('<div class="primaryLabel">' + linkValue +'</div>').appendTo(labelEditContainer);
			_this.componentMap[elementId + ':itemValue'] = itemValue;
	
			if(config.isURL)
			{
				$(itemValue).find('.detailsLink').click(function(event)
				{
					event.stopPropagation();
				});
			}

			$(itemValue).click(function()
			{		
				var disableAttrValue = $(this).attr('disabled')
				var isDisabled = (disableAttrValue == 'disabled' || disableAttrValue == true);								
				if(!isDisabled)
				{
					var thisContainer = $(labelHiddenDiv)[0];
					if(thisContainer != null && thisContainer.style.display != 'none')
					{
						$(thisContainer).hide(333);
						$(thisContainer).removeClass('showHiddenContainer');
					}
					else
					{
						$(thisContainer).show(333);
						$(thisContainer).addClass('showHiddenContainer');
					}
				}		
			});					
		}
		else
		{									
			if(hasValue && config.isURL)
			{
				$(itemValue).html(linkValue);
			}
			else
			{
				$(itemValue).text(linkValue);
			}								
		}

		var editInput = _this.inputMap[elementId];
		
		if(editInput == null)
		{	
			config.itemValue = itemValue;
			editInput = createInput(_this,editContainer,config);			
			_this.inputMap[elementId] = editInput;

			editInput._context.local.eventDispatcher.on(ACE.AML.Event.PROPERTY_CHANGE, function(e ,eventData)
			{
				if(eventData.property == editInput._options.property)
				{
					var itemValue = config.itemValue;
					var inputVal = eventData.newValue;
	
					var titleValue = inputVal;
					var labelValue = titleValue;
	
					if(inputVal == '' || inputVal == null)
					{
						labelValue = config.label;					
					}
	
					if(config.isURL)
					{
						$(itemValue).html(labelValue);
					}
					else
					{
						$(itemValue).text(labelValue);
					}
				}					
			});
		}
		else
		{
			editInput.value = value;
		}

		$(itemValue).attr('disabled',disabled);
		$(editInput).attr('disabled',disabled);
		$(editInput).prop('isRequired',config.required);
		$(editInput).prop('reqLabel',config.label);
		$(editInput).prop('title',value);		

		if(disabled)
		{
			$(itemValue).removeClass('editable');
			$(itemValue).addClass('readOnly');
			editInput.control[0].readOnly = true;
		}
		else
		{
			$(itemValue).addClass('editable');
			$(itemValue).removeClass('readOnly');
			editInput.control[0].readOnly = false;
		}
	}

	$(document).mouseup(function(e) 
	{
	    var container = $(".showHiddenContainer");
	    //hack - the multiselect is not a child of the container, so if it is clicked, we assume that it is in the container.
	    if (!container.is(e.target) && e.target.className != 'ui-selectable-helper' &&  container.has(e.target).length === 0 && container != undefined) 
	    {
	    	var thisContainer = container[0];
	    	if(thisContainer != undefined && thisContainer.style.display != 'none')
	    	{
		        $(thisContainer).hide(333);
		        $(thisContainer).removeClass('showHiddenContainer');
	    	}
	    }
	});

	function createCheckBoxItem(_this,component,config)
	{	
		if(config == null || config.fieldName == null || config.accessLevel == 'None')
		{
			return createCheckBoxItem;
		}

		var value = getValue(config);
		var disabled = (config.accessLevel == 'Read');
		var elementId = getElementId(config);
		var checked = '';
		if(value == true)
		{
			checked = 'checked';
		}

		var componentContainer = _this.componentMap[elementId];

		if(componentContainer == null)
		{
			componentContainer = new SummaryCubeComponentContainer(_this,component,config);
			_this.componentMap[elementId] = componentContainer;
		}

		if(config.compareIsShow)
		{
			var show = getVisibility(config, _this);

			if(show)
			{
				$(componentContainer.subSectionItem).show();
			}
			else
			{
				$(componentContainer.subSectionItem).hide();
			}
		}
		else
		{
			var compareCanEdit = getVisibility(config, _this);

			disabled = !compareCanEdit;			
		}

		var options = {};
		options.property = config.objectName + '.' + config.fieldName;
		options.defaultValue = value;
		options.readOnly = disabled;

		var thisInput = _this.inputMap[elementId];

		if(thisInput == null)
		{	
			
			thisInput = ACE.AML.ControllerManager.create(
				ACE.AML.Control.Checkbox.CONTROLLER_NAME, 
				new ACE.AML.ControllerContext(componentContainer.editContainer, options, _this._context.local)
			);

			thisInput._context.local.eventDispatcher.on(ACE.AML.Event.PROPERTY_CHANGE, function(e ,eventData)
			{
				if(eventData.property == thisInput._options.property)
				{
					_this.picklistValueTracker[elementId] = eventData.newValue;
					
					if(!_this.hasInit)
					{
						_this.showButtonContainer(true);
						_this._context.local.state.put(_this.config.object + "Updated", true);
					}	
				}					
			});

			_this.inputMap[elementId] = thisInput;
		}

		$(thisInput).prop('disabled', disabled);
	}

	SummaryCubeDetail.prototype.showButtonContainer = function(show)
	{
		if(show == true)
		{
			this._context.global.eventDispatcher.trigger('showSummaryCubeButtons');
		}
		else
		{
			this._context.global.eventDispatcher.trigger('hideSummaryCubeButtons');
		}
	}

	function createBoolDisplay(_this,component,config)
	{
		if(config == null || config.fieldName == null || config.accessLevel == 'None')
		{
			return createBoolDisplay;
		}

		var value = getValue(config);
		var disabled = (config.accessLevel == 'Read');
		var elementId = getElementId(config);

		var componentContainer = _this.componentMap[elementId];

		if(componentContainer == null)
		{
			componentContainer = new SummaryCubeComponentContainer(_this,component,config);
			_this.componentMap[elementId] = componentContainer;
		}

		// not going to put in the input map because it's not an input. 
		var thisInput;
		componentContainer.editContainer.empty();

		if (value == null)
		{
			thisInput = $('<div class="summaryCubeNullCircle"></div>' )
			.appendTo(componentContainer.editContainer);
		}
		else if (value == false)
		{
			thisInput = $('<div class="summaryCubeRedClose"></div>' )
			.appendTo(componentContainer.editContainer);
		}
		else if (value == true)
		{
			thisInput = $('<div class="summaryCubeCheckmark"></div>' )
			.appendTo(componentContainer.editContainer);
		}

		if(config.compareIsShow)
		{
			var show = getVisibility(config, _this);

			if(show)
			{
				$(componentContainer.subSectionItem).show();
			}
			else
			{
				$(componentContainer.subSectionItem).hide();
			}
		}
		else
		{
			var compareCanEdit = getVisibility(config, _this);

			disabled = !compareCanEdit;			
		}

		$(thisInput).attr("disabled", disabled);
	}

	function createIcon(_this,component,config)
	{
		if(config == null || config.fieldName == null || config.accessLevel == 'None')
		{
			return createIcon;
		}

		var value = config.record[config.fieldName];
		value = $(value).attr('src');

		if(value == null)
		{
			value = $(config.record[config.fieldName]).find('img').attr('src');
		}	

		var classNames = config.additionalClassNames;
		var parentComponent = component[0].parentElement;
		var elementId = getElementId(config);
		var componentContainer = _this.componentMap[elementId];
		var imgDiv = _this.componentMap[elementId + 'ImgDiv'];

		if(componentContainer == null)
		{
			componentContainer = new SummaryCubeComponentContainer(_this,component,config);
			_this.componentMap[elementId] = componentContainer;
		}

		componentContainer.editContainer.empty();

		if(classNames == null)
		{
			classNames = '';
		}

		if(componentContainer == null)
		{
			componentContainer = $('<div Id="' + elementId + '" class="headerItem ' + classNames + '"></div>').appendTo(parentComponent);
			_this.componentMap[elementId] = componentContainer;

			imgDiv = $('<div class="' + classNames + '" style="background:url(\'' + value + '\') no-repeat;height:' + $(componentContainer).height() + 'px;"></div>').appendTo(componentContainer);
			_this.componentMap[elementId + 'ImgDiv'] = imgDiv;
		}
		else
		{
			$(componentContainer).empty().hide();
			imgDiv = $('<div class="' + classNames + '" style="background:url(\'' + value + '\') no-repeat;height:16px;margin-top:3px"></div>').appendTo(componentContainer.editContainer);
		}

		$(imgDiv).show();
		$(componentContainer).show();
		$(componentContainer.subSectionItem).show();
	}

	function createLookup(_this,component,config)
	{
		if(config == null || config.fieldName == null || config.accessLevel == 'None')
		{
			return createLookup;
		}

		var value = getValue(config);

		var disabled = (config.accessLevel == 'Read');
		var elementId = config.parentField + ':' + config.objectName;

		var componentContainer = _this.componentMap[elementId];

		if(componentContainer == null)
		{
			componentContainer = new SummaryCubeComponentContainer(_this,component,config);
			_this.componentMap[elementId] = componentContainer;
		}

		if(config.compareIsShow)
		{
			var show = getVisibility(config, _this);

			if(show)
			{
				$(componentContainer.subSectionItem).show();
			}
			else
			{
				$(componentContainer.subSectionItem).hide();
			}
		}
		else
		{
			var compareCanEdit = getVisibility(config, _this);

			disabled = !compareCanEdit;	
		}

		var thisInput = _this.inputMap[elementId];
	
		var options  = {};
		options.type = config.lookupObjectName;
		options.dataProvider  = 'SEARCH';
		options.property = config.objectName + '.' + config.fieldName;//config.parentField;
		options.delay = 300;
		options.searchFields = config.lookupSearchFields;
		options.searchOptions = {'additionalWhereClause' : config.additionalWhereClause}; 
		options.readOnly = disabled;

		var initialCreate = false;

		if(thisInput == null)
		{	
			initialCreate = true;

			thisInput = ACE.AML.ControllerManager.create(
				ACE.AML.Control.AutoComplete.CONTROLLER_NAME, 
				new ACE.AML.ControllerContext(componentContainer.editContainer, options, _this._context.local)
			);

			thisInput._context.local.eventDispatcher.on(ACE.AML.Event.PROPERTY_CHANGE, function(e ,eventData)
			{
				if(eventData.property == thisInput._options.property)
				{
					if(!_this.hasInit)
					{
						_this.showButtonContainer(true);
						_this._context.local.state.put(_this.config.object + "Updated", true);
					}	
				}					
			});

			_this.inputMap[elementId] = thisInput;
		}

		if(value.length == 0)
		{
			thisInput.text = '';
		}
		
		if (!initialCreate)
		{
			thisInput.setOption("readOnly",disabled);
		}

		$(thisInput).attr('isRequired', config.required);
		$(thisInput).attr('reqLabel',config.label);

		return thisInput;
	}

	// Function to create text inputs
	function createInput(_this,component,config)
	{
		if(config == null || !config.enabled || config.fieldName == null || config.accessLevel == 'None')
		{
			return createInput;
		}

		var value = getValue(config);
		var disabled = (config.accessLevel == 'Read');
		var elementId = getElementId(config);

		var componentContainer = _this.componentMap[elementId];

		if(componentContainer == null)
		{
			componentContainer = new SummaryCubeComponentContainer(_this,component,config);
			_this.componentMap[elementId] = componentContainer;
		}

		if(config.compareIsShow)
		{
			var show = getVisibility(config, _this);

			if(show)
			{
				$(componentContainer.subSectionItem).show();
			}
			else
			{
				$(componentContainer.subSectionItem).hide();
			}
		}
		else
		{
			var compareCanEdit = getVisibility(config, _this);

			disabled = !compareCanEdit;	
		}

		var thisInput = _this.inputMap[elementId];

		if(thisInput == null)
		{	
			var textOptions = {};
			textOptions.property = config.objectName + '.' + config.fieldName;
			textOptions.value = value;
			textOptions.attributes = 
			{
				"disabled" : disabled.toString()
			};
			
			thisInput = ACE.AML.ControllerManager.create(
				ACE.AML.Control.TextInput.CONTROLLER_NAME, 
				new ACE.AML.ControllerContext(componentContainer.editContainer, textOptions, _this._context.local)
			);
			
			thisInput._context.local.eventDispatcher.on(ACE.AML.Event.PROPERTY_CHANGE, function(e ,eventData)
			{
				if(eventData.property == thisInput._options.property)
				{
					if(!_this.hasInit)
					{
						_this.showButtonContainer(true);
						_this._context.local.state.put(_this.config.object + "Updated", true);
					}	
				}					
			});

			_this.inputMap[elementId] = thisInput;
		}

		$(thisInput).attr('isRequired', config.required);
		$(thisInput).attr('reqLabel',config.label);
        $(thisInput).prop('disabled', disabled);
	
		return thisInput;
	}

    function createCurrencyInput(_this,component,config)
    {
        if(config == null || !config.enabled || config.fieldName == null || config.accessLevel == 'None')
		{
			return createCurrencyInput;
		}

		var value = getValue(config);
		var disabled = (config.accessLevel == 'Read');
		var elementId = getElementId(config);

		var componentContainer = _this.componentMap[elementId];

		if(componentContainer == null)
		{
			componentContainer = new SummaryCubeComponentContainer(_this,component,config);
			_this.componentMap[elementId] = componentContainer;
		}

		var thisInput = _this.inputMap[elementId];

		var thisInput = _this.inputMap[elementId];

		if(thisInput == null)
		{	
			var textOptions = {};
			textOptions.property = config.objectName + '.' + config.fieldName;
			textOptions.numericOnly = true;
			textOptions.value = value;
			textOptions.attributes = 
			{
				"disabled" : disabled.toString()
			};

			thisInput = ACE.AML.ControllerManager.create(
				ACE.AML.Control.TextInput.CONTROLLER_NAME, 
				new ACE.AML.ControllerContext(componentContainer.editContainer, textOptions, _this._context.local)
			);
			
			thisInput._context.local.eventDispatcher.on(ACE.AML.Event.PROPERTY_CHANGE, function(e ,eventData)
			{
				if(eventData.property == thisInput._options.property)
				{
					if(!_this.hasInit)
					{
						_this.showButtonContainer(true);
						_this._context.local.state.put(_this.config.object + "Updated", true);
					}	
				}					
			});

			_this.inputMap[elementId] = thisInput;

			$(thisInput).attr('isRequired', config.required);
			$(thisInput).attr('reqLabel',config.label);
		}

		thisInput._options.readOnly = disabled;

		return thisInput;
    }
    
	function createNotesItem(_this,component,config)
	{
		if(config == null || config.fieldName == null || config.accessLevel == 'None')
		{
			return createNotesItem;
		}

		var value = returnBlankIfNull(getValue(config));
		var disabled = (config.accessLevel == 'Read');
		var elementId = getElementId(config);

		var componentContainer = _this.componentMap[elementId];

		if(componentContainer == null)
		{
			componentContainer = new SummaryCubeComponentContainer(_this,component,config);
			
			$(componentContainer.subSectionItem).removeClass('componentContainer');
			$(componentContainer.subSectionItem).removeClass('subSectionItem');
			$(componentContainer.subSectionItem).addClass('subSection');
			$(componentContainer.subSectionItem).addClass('fullWidthComponent');
			$(componentContainer.subSectionItem).addClass('notesComponent');

			_this.componentMap[elementId] = componentContainer;
		}

		if(config.compareIsShow)
		{
			var show = getVisibility(config, _this);

			if(show)
			{
				$(componentContainer.subSectionItem).show();
			}
			else
			{
				$(componentContainer.subSectionItem).hide();
			}
		}
		else
		{
			var compareCanEdit = getVisibility(config, _this);

			disabled = !compareCanEdit;	
		}

		var thisInput = _this.inputMap[elementId];

		if(thisInput == null)
		{	
			var textOptions = {};
			textOptions.property = config.objectName + '.' + config.fieldName;
			textOptions.value = value;
			textOptions.readOnly = disabled;

			thisInput = ACE.AML.ControllerManager.create(
				ACE.AML.Control.TextArea.CONTROLLER_NAME, 
				new ACE.AML.ControllerContext(componentContainer.editContainer, textOptions, _this._context.local)
			);

			thisInput._context.local.eventDispatcher.on(ACE.AML.Event.PROPERTY_CHANGE, function(e ,eventData)
			{
				if(eventData.property == thisInput._options.property)
				{
					if(!_this.hasInit)
					{
						_this.showButtonContainer(true);
						_this._context.local.state.put(_this.config.object + "Updated", true);
					}	
				}					
			});

			_this.inputMap[elementId] = thisInput;
		}

		$(thisInput).attr('isRequired',config.required);		
		$(thisInput).attr('reqLabel',config.label);

		return thisInput;
	}

	function createPicklist(_this,component,config)
	{
		if(config == null || config.fieldName == null || config.accessLevel == 'None')
		{
			return createPicklist;
		}

		var value = getValue(config);
		var elementId = getElementId(config);

		var componentContainer = _this.componentMap[elementId];

		if(componentContainer == null)
		{
			componentContainer = new SummaryCubeComponentContainer(_this,component,config);
			_this.componentMap[elementId] = componentContainer;
		}
		var disabled = (config.accessLevel == 'Read');

		if(config.compareIsShow)
		{
			var show = getVisibility(config, _this);

			if(show)
			{
				$(componentContainer.subSectionItem).show();
			}
			else
			{
				$(componentContainer.subSectionItem).hide();
			}
		}
		else
		{
			var compareCanEdit = getVisibility(config, _this);

			disabled = !compareCanEdit;	
		}

		var thisInput = _this.inputMap[elementId];

		var picklistValues = [];

		if(config.controllingField != null)
   		{
			picklistValues = getPicklistValuesForDependantPicklist(_this,config,false); 
		}
		else
		{
			if(config.picklistValues != null && config.picklistValues.length > 0)
			{
				for(var x=0;x<config.picklistValues.length;x++)
				{	
					var thisPicklistValue = config.picklistValues[x];
					var isDefault = ((value == null && thisPicklistValue.isDefault) || (value != null && thisPicklistValue.value == value));
					picklistValues.push({data : thisPicklistValue.value, label : thisPicklistValue.label, defaultSelected : isDefault});
				}
			}
		}

		var options = {};
		options.property = config.objectName + '.' + config.fieldName;
		options.dataProvider = picklistValues;
		options.nullValueLabel = '{Locale.PICKLIST_SELECT_PROMPT}';
		options.attributes = {disabled : disabled};

		if(thisInput == null)
		{		
			thisInput = ACE.AML.ControllerManager.create(
				ACE.AML.Control.DropDown.CONTROLLER_NAME, 
				new ACE.AML.ControllerContext(componentContainer.editContainer, options, _this._context.local)
			);
			_this.picklistValueTracker[elementId] = value;
			thisInput.isPicklist = true;

			thisInput._context.local.eventDispatcher.on(ACE.AML.Event.PROPERTY_CHANGE, function(e ,eventData)
			{
				if(eventData.property == thisInput._options.property)
				{
					_this.picklistValueTracker[elementId] = eventData.newValue;

					if(eventData.newValue != eventData.oldValue)
					{
						if(!_this.hasInit)
						{
							_this.showButtonContainer(true);
						_this._context.local.state.put(_this.config.object + "Updated", true);
						}	
					}					
				}
			});
			
			_this.inputMap[elementId] = thisInput;
		}
		else
		{
			_this.picklistValueTracker[elementId] = value;	
			if(value.length == 0)
			{
				value = null;
			}
			thisInput.setOption("dataProvider", picklistValues);
			
			if(value != null)
			{
				thisInput.value = value;
			}			
		}

		$(thisInput).attr('isRequired',config.required);
		$(thisInput).attr('reqLabel',config.label);
		$(thisInput).prop('disabled', disabled);

		if(config.controllingField != null)
		{
			var controllingField = config.controllingField.name+ ':' + config.objectName;
			
			if(_this.dependentPicklists.hasOwnProperty(controllingField))
			{
				var pickers = _this.dependentPicklists[controllingField];
				pickers.push({config:config,picklist:thisInput});
			}
			else
			{
				var pickers = [{config:config,picklist:thisInput}];
				_this.dependentPicklists[controllingField] = pickers;
			}

			_this.picklistValueTracker.watchPicklistValue(controllingField,function(controllingField)
			{
				picklistValues = getPicklistValuesForDependantPicklist(_this,config,false);  
				var picklists = _this.dependentPicklists[controllingField];

				if(picklists != null && $.isArray(picklists))
				{
					for(var x=0;x<picklists.length;x++)
					{
						var pickerObject = picklists[x];
						var picker = pickerObject['picklist'];
						var pickConfig = pickerObject['config'];

						picklistValues = getPicklistValuesForDependantPicklist(_this,pickConfig,false);
						picker.set("dataProvider",picklistValues);
					}
				} 
				//thisInput.set("dataProvider",picklistValues);
			});
		}
	}

	function createDateTimePicker(_this,component,config)
	{
		if(config == null || config.fieldName == null || config.accessLevel == 'None')
		{
			return createDateTimePicker;
		}

		//var value = getDateTimeValue(config, _this);
		var elementId = getElementId(config);
		var componentContainer = _this.componentMap[elementId];
		var disabled = (config.accessLevel == 'Read');
		var joinDiv = _this.componentMap[elementId + 'JoinDiv'];

		if(componentContainer == null)
		{
			componentContainer = new SummaryCubeComponentContainer(_this,component,config);
			_this.componentMap[elementId] = componentContainer;
			joinDiv = $('<div style="display: flex;"/>').appendTo(componentContainer.editContainer);
			_this.componentMap[elementId + 'JoinDiv'] = joinDiv;
		}

		if(config.compareIsShow)
		{
			var show = getVisibility(config, _this);

			if(show)
			{
				$(componentContainer.subSectionItem).show();
			}
			else
			{
				$(componentContainer.subSectionItem).hide();
			}
		}
		else
		{
			var compareCanEdit = getVisibility(config, _this);

			disabled = !compareCanEdit;	
		}

		var datePicker = _this.inputMap[elementId];
		var dateTimePicker = _this.inputMap[elementId + 'Time'];
		var dateProperty = 'dummy' + config.objectName + '.' + config.fieldName;

		var value = _this._context.local.state.get(config.objectName + '.' + config.fieldName);
        var offsetValue = new Date().getTimezoneOffset();
		var millisecondOffsetValue = offsetValue * 60000;

		if(datePicker == null)
		{
			var dateOptions = {"property" : dateProperty, "readOnly" : disabled, "showButtonPanel": true, "closeText": ACE.Locale.get('CLEAR')};
			
			datePickerDiv = $('<div style="min-width:100px;max-width:50%" class="dateTimePicker"/>').appendTo(joinDiv);
			dateTimePickerDiv = $('<div style="min-width:90px;max-width:50%" class="dateTimePicker"/>').appendTo(joinDiv);

			datePicker = ACE.AML.ControllerManager.create(
				ACE.AML.Control.DatePicker.CONTROLLER_NAME, 
				new ACE.AML.ControllerContext(datePickerDiv, dateOptions, _this._context.local)
			);
			
			datePicker.isDateTime = true;
			datePicker.isDatePortion = true;

			_this.inputMap[elementId] = datePicker;
		
			datePicker._context.local.eventDispatcher.on(ACE.AML.Event.PROPERTY_CHANGE, function(e ,eventData)
			{
				if((eventData.newValue || eventData.oldValue) && eventData.property == datePicker._options.property && eventData.newValue != eventData.oldValue && value != eventData.newValue && eventData.oldValue !== undefined)
				{
					if(!_this.hasInit)
					{
						_this.showButtonContainer(true);
						_this._context.local.state.put(_this.config.object + "Updated", true);
					}	
				}
			});

			var options = {"property" : config.objectName + '.' + config.fieldName, "readOnly" : disabled};

			dateTimePicker = ACE.AML.ControllerManager.create(
				ACE.AML.Control.TimePicker.CONTROLLER_NAME, 
				new ACE.AML.ControllerContext(dateTimePickerDiv, options, _this._context.local)
			);

				dateTimePicker.isDateTime = true;
				dateTimePicker.isTimePortion = true;
				dateTimePicker.dateProperty = dateProperty,

			_this.inputMap[elementId + 'Time'] = dateTimePicker;
		
			dateTimePicker._context.local.eventDispatcher.on(ACE.AML.Event.PROPERTY_CHANGE, function(e ,eventData)
			{
				if(eventData.property == dateTimePicker._options.property && eventData.newValue != eventData.oldValue)
				{
					if(!_this.hasInit)
					{
						_this.showButtonContainer(true);
						_this._context.local.state.put(_this.config.object + "Updated", true);
					}	
				}
			});
		}

		if (value)
        {
            datePicker.value = value + millisecondOffsetValue;
        }
        else
        {
        	datePicker.value = null;
        }

		$(datePicker).attr('isRequired',config.required);
		$(dateTimePicker).attr('isRequired',config.required);	
		$(datePicker).attr('reqLabel',config.label);
		$(dateTimePicker).attr('reqLabel',config.label);
		$(datePicker).prop('disabled', disabled);
        $(dateTimePicker).prop('disabled', disabled);
	}

	function createDatePicker(_this,component,config)
	{
		if(config == null || config.fieldName == null || config.accessLevel == 'None')
		{
			return createDatePicker;
		}
		
		var elementId = getElementId(config);
		var componentContainer = _this.componentMap[elementId];
		var disabled = (config.accessLevel == 'Read');

		if(componentContainer == null)
		{
			componentContainer = new SummaryCubeComponentContainer(_this,component,config);
			_this.componentMap[elementId] = componentContainer;
		}

		if(config.compareIsShow)
		{
			var show = getVisibility(config, _this);

			if(show)
			{
				$(componentContainer.subSectionItem).show();
			}
			else
			{
				$(componentContainer.subSectionItem).hide();
			}
		}
		else
		{
			var compareCanEdit = getVisibility(config, _this);

			disabled = !compareCanEdit;	
		}

		var datePicker = _this.inputMap[elementId];
		var dateProperty = config.objectName + '.' + config.fieldName;
        var value = _this._context.local.state.get(dateProperty);
        var offsetValue = new Date().getTimezoneOffset();
		//var millisecondOffsetValue = offsetValue * 60000;// not needed since utcDate is true below

		if(datePicker == null)
		{
			var options = {"utcDate": true, "property" : dateProperty, "readOnly" : disabled, "showButtonPanel": true, "closeText": ACE.Locale.get('CLEAR')};

			datePicker = ACE.AML.ControllerManager.create(
				ACE.AML.Control.DatePicker.CONTROLLER_NAME, 
				new ACE.AML.ControllerContext(componentContainer.editContainer, options, _this._context.local)
			);

			datePicker.isDate = true;

			_this.inputMap[elementId] = datePicker;
		
			datePicker._context.local.eventDispatcher.on(ACE.AML.Event.PROPERTY_CHANGE, function(e ,eventData)
			{
				if(eventData.property == datePicker._options.property && eventData.newValue != eventData.oldValue)
				{
					_this.showButtonContainer(true);
						_this._context.local.state.put(_this.config.object + "Updated", true);
				}
			});
		}

		if (!value)
        {
			datePicker.value = null;
		}

		$(datePicker).attr('isRequired',config.required);
		$(datePicker).attr('reqLabel',config.label);
        $(datePicker).prop('disabled', disabled);
	}

	function returnBlankIfNull(value)
	{
		if(value == null)
		{
			value = '';
		}

		return value;
	}

	function createAddressItem(_this,component,config)
	{
		if(config == null || config.fieldName == null || config.accessLevel == 'None')
		{
			return createAddressItem;
		}
		var disabled = (config.accessLevel == 'Read');

		var elementId = getElementId(config);
		var componentContainer = _this.componentMap[elementId];
		var mapDiv = _this.componentMap[elementId + 'MapDiv'];
		var mapIframe = _this.componentMap[elementId + 'MapIframe'];		
		var thisEditForm = _this.componentMap[elementId + 'thisEditForm'];
		var addressInputMap = {};
		
		if(componentContainer == null)
		{
			componentContainer = $('<div class="subSection fullWidthComponent addressComponent" id="' + elementId + '"></div>').appendTo(component);	
			_this.componentMap[elementId] = componentContainer;

			$('<div  id="' + elementId + 'label" class="label addressLine">' + config.label + '</div>').appendTo(componentContainer);	    	
		}

		if(config.compareIsShow)
		{
			var show = getVisibility(config, _this);

			if(show)
			{
				$(componentContainer.subSectionItem).show();
			}
			else
			{
				$(componentContainer.subSectionItem).hide();
			}
		}
		else
		{
			var compareCanEdit = getVisibility(config, _this);

			disabled = !compareCanEdit;	
		}

		var lookupFieldName = _this.t1cNS + config.fieldName + '_Address_Id__c';
		var streetField = config.fieldName + 'Street';
		var cityField = config.fieldName + 'City';
		var stateField = config.fieldName + 'State';
		var zipField = config.fieldName + 'PostalCode';
		var countryField = config.fieldName + 'Country';
		
		var street = returnBlankIfNull(config.record[streetField]);
		var city = returnBlankIfNull(config.record[cityField]);
		var state = returnBlankIfNull(config.record[stateField]);
		var zip = returnBlankIfNull(config.record[zipField]);
		var country = returnBlankIfNull(config.record[countryField]);

		var addressText = street + ' ' + city + ' ' + state  + ' ' + country + ' ' + zip;	

		var linkAddress = _this.componentMap[elementId + 'linkAddress'];
		var copyAddressText = _this.componentMap[elementId + 'copyAddressText'];
		var addressPickerDiv = _this.componentMap[elementId + 'addressPickerDiv'];

		if(linkAddress == null)
		{
			copyAddressText = $('<svg title="Copy Address" class="addressLine" style="padding-top: 5px;cursor: copy;width:16px;height:16px" viewBox="0 0 24 24"><path fill="#797979" d="M19,21H8V7H19M19,5H8A2,2 0 0,0 6,7V21A2,2 0 0,0 8,23H19A2,2 0 0,0 21,21V7A2,2 0 0,0 19,5M16,1H4A2,2 0 0,0 2,3V17H4V3H16V1Z" /></svg>').appendTo(componentContainer);
			linkAddress = $('<div class="cubeAddress addressLine"><span  style="font-style:normal;color:#212121"  id="addressText">' +  addressText + '</span></div>').appendTo(componentContainer);
			
			_this.componentMap[elementId + 'linkAddress'] = linkAddress;
			_this.componentMap[elementId + 'copyAddressText'] = copyAddressText;

			$(linkAddress).click(function ()
			{
				$(thisEditForm).toggle();				
			});

			$(copyAddressText).click(function()
			{
				var text = $(linkAddress).get(0);
				var selection = window.getSelection();
				var range = document.createRange(); 
				range.selectNodeContents(text);
				selection.removeAllRanges();
				selection.addRange(range); 
				
				var succeed;
				try
				{
					succeed = document.execCommand("copy");
				} 
				catch(e) 
				{
					succeed = false;
					console.log('did not copy');
				}	
				
				selection.removeAllRanges();
			});
		}
		else
		{
			$(linkAddress).text(addressText);
		}

		var newAddressURL = _this._config.addressManagerLink + config.record.Id;
		var thisEditAddressLauncher = _this.componentMap[elementId + 'thisEditAddressLauncher'];
		var addressPicker = _this.inputMap[lookupFieldName + ':' + config.objectName];
		var mapsIcon = _this.componentMap[elementId + 'mapsIcon'];
		var picklistValues = [];
		
		var currentId = config.record[lookupFieldName];

		if(addressPickerDiv == null)
		{
			addressPickerDiv = $('<div id="' + elementId + 'addressPickerDiv" class="detailComponents addressLine"></div>').appendTo(componentContainer);
			_this.componentMap[elementId + 'addressPickerDiv'] = addressPickerDiv;
		}

		if(mapsIcon == null)
		{
			var mapsIconSpan = $('<div class="addressComponent" style="padding:0 5px" title="Toggle Google Maps"/>').appendTo(addressPickerDiv);
			mapsIcon = $(config.mapIconSVG).appendTo(mapsIconSpan);
			_this.componentMap[elementId + 'mapsIcon'] = mapsIcon;
			$(mapsIcon).click(function()
			{
				$(mapDiv).toggle(333);
			});

			if(config.hideMapIcon == true)
			{
				$(mapsIcon).hide();
			}
		}

		if(_this._config.showAddressManagerLink && config.showAddressPicker)
		{
			if(thisEditAddressLauncher == null)
			{
				thisEditAddressLauncher = $('<a target="_blank">' + config.editIconSVG + '</a>').appendTo(addressPickerDiv);
				_this.componentMap[elementId + 'thisEditAddressLauncher'] = thisEditAddressLauncher;
			}

			if(_this._config.addresses != null && _this._config.addresses.length > 0)
			{				
				var defaultFound = currentId != null;
				
				//picklistValues.push({label : 'Select an Address', data : '', defaultSelected : !defaultFound});

				$.each(_this._config.addresses, function(key,value)
				{
					picklistValues.push({label : value[_this.t1cNS +'Address_Name__c'], data : value.Id, defaultSelected : (currentId == value.Id)});
				});
			}
			else
			{
				//picklistValues.push({label : 'No Addresses Found', data : '', defaultSelected : true});
			}

			var options = {};
			options.property = config.objectName + '.' + lookupFieldName;
			options.dataProvider = picklistValues;
			options.nullValueLabel = '{Locale.PICKLIST_SELECT_PROMPT}';
			options.attributes = {disabled : disabled};

			if(addressPicker == null)
			{
				addressPicker = ACE.AML.ControllerManager.create(
					ACE.AML.Control.DropDown.CONTROLLER_NAME, 
					new ACE.AML.ControllerContext(addressPickerDiv, options, _this._context.local)
				);

				addressPicker.isPicklist = true;
				_this.inputMap[lookupFieldName + ':' + config.objectName] = addressPicker;

				addressPicker._context.local.eventDispatcher.on(ACE.AML.Event.PROPERTY_CHANGE, function(e ,eventData)
				{
					if(eventData.property == addressPicker._options.property)
					{
						if(eventData.newValue != eventData.oldValue)
						{
							if(!_this.hasInit)
							{
								_this.showButtonContainer(true);
						_this._context.local.state.put(_this.config.object + "Updated", true);
							}	

							populateAddressFieldsWithAddressObject(addStreet,addCity,addState,addCountry,addZip,eventData.newValue,config.clearAddressFieldsOnNull,_this);
						}
						
					}
				});
			}
			else if(config.showAddressPicker)
			{
				addressPicker._adhocAddedItems = null;
				addressPicker.setOption("dataProvider", picklistValues);
				addressPicker._context.local.state.put(options.property, currentId);
				//addressPicker._setValue(currentId);	
				addressPicker.value = currentId;			
			}
		}

		$(thisEditAddressLauncher).attr('href',newAddressURL);

		if(mapDiv == null)
		{
			mapDiv = $('<div id="' + elementId + 'MapDiv" style="width:100%"></div>').appendTo(componentContainer);
			_this.componentMap[elementId + 'MapDiv'] = mapDiv;
			
			
			var addressControls = $('<div id="addressControls" style="width:100%" class="detailComponents"></div>').appendTo(mapDiv);		
			var autoCompleteInputDiv = $('<div class="googeMaps" style="width:100%;margin-bottom:10px" id="pac-container"></div>').appendTo(addressControls);
			if(typeof google === 'undefined')
			{
				$(autoCompleteInputDiv).hide();
			}
			else
			{
				var autoCompleteInput = $('<input autocomplete="new-password" style="width:90%" id="pac-input" type="text" placeholder="' + config.mapsSearchPlaceHolder + '"></input>').appendTo(autoCompleteInputDiv);
				var infowindowContent = $('<div style="display: none;width:100%" id="infowindow-content"><img src="" width="16" height="16" id="place-icon"></img><span id="place-name" class="title"></span><br/><span id="place-address"></span></div>').appendTo(addressControls);

				addressText = addressText.replace('#','%23');
				addressText = addressText.replace(',',' ');
				mapIframe = $('<iframe id="' + elementId + 'MapIframe" class="googleMap addressLine" frameborder="0" height="250" src="https://www.google.com/maps/embed/v1/place?key=' + _this.mapsKey +'&q=' + encodeURI(addressText) +'" allowfullscreen tabindex="-1" width="100%"></iframe>').appendTo(mapDiv);
				_this.componentMap[elementId + 'MapIframe'] = mapIframe;
				var autocomplete = new google.maps.places.Autocomplete(autoCompleteInput[0]);

				// ST-4352: Hide Google Autocomplete if address is read only
				if (disabled)
				{
					$(autoCompleteInput).hide()
				}

				var infowindow = new google.maps.InfoWindow();

				var componentForm = {
					street_number: 'long_name',
					route: 'long_name',
					subpremise: 'long_name',
					locality: 'long_name',
					sublocality : 'long_name',
					postal_town: 'long_name',
					administrative_area_level_4 : 'long_name',
					administrative_area_level_1: 'long_name',
					country: 'long_name',
					postal_code: 'long_name'
				};                  
				
				autocomplete.setTypes([]);
				autocomplete.setOptions({strictBounds: false});

				autocomplete.addListener('place_changed', function() 
				{
					if(config.accessLevel != 'Edit')
					{
						return;
					}

					infowindow.close();
					$(thisEditForm).show(333);
					var place = autocomplete.getPlace();

					if(place.address_components.length != null)
					{
						var streetField = $(thisEditForm).find('.street');
						$(streetField).val('');

						for (var i = 0; i < place.address_components.length; i++) 
						{
							if(place.address_components[i].types.length != null)
							{
								for(var x=0;x<place.address_components[i].types.length;x++)
								{                                              
									var addressType = place.address_components[i].types[x];
									if (componentForm[addressType]) 
									{
										var val = place.address_components[i][componentForm[addressType]];
										
										var field = addressInputMap[addressType];
										var fieldVal = field.value;
												
										if(fieldVal != '')
										{
											fieldVal += ' ';
										}

										if(addressType == 'subpremise')
										{
											fieldVal += val;
										}
										else if(addressType == 'street_number')
										{
											fieldVal += val;
										}
										else if(addressType == 'route')
										{
											fieldVal += val;
										}
										else
										{
											fieldVal = val;
										}
						
										_this._context.local.state.put(field._options.property,fieldVal);
									}
								}
							}
						}
					}
					addressText = place.formatted_address;
					$(linkAddress).text(addressText);
					addressText = addressText.replace('#','%23');
					addressText = addressText.replace(',',' ');
					$(mapIframe).attr('src','https://www.google.com/maps/embed/v1/place?key=' + _this.mapsKey +'&q=' + encodeURI(addressText));
					
					if(!_this.hasInit)
					{
						_this.showButtonContainer(true);
							_this._context.local.state.put(_this.config.object + "Updated", true);
					}	
				});	
			}				

			if(config.hideMap)
			{
				$(mapDiv).hide();
			}
			else
			{
				$(mapDiv).show();
			}
		}
		else
		{
			$('#pac-input').val('');
			addressText = addressText.replace('#','%23');
			addressText = addressText.replace(',',' ');
			$(mapIframe).attr('src','https://www.google.com/maps/embed/v1/place?key=' + _this.mapsKey +'&q=' + encodeURI(addressText));
		}

		if(config.hideMapIcon == true)
		{
			$(mapIframe).hide();
		}
		
		if(thisEditForm == null)
		{
			thisEditForm = $('<div id="' + elementId + '" class="hiddenSubSection detailComponents"></div>').appendTo(componentContainer);
			_this.componentMap[elementId + 'thisEditForm'] = thisEditForm;
			$(thisEditForm).hide();
		}

		var addStreet = createAddressInput(_this,thisEditForm,streetField,config,'addressField street subpremise street_number route',mapIframe,linkAddress,addressInputMap);
		var addCity = createAddressInput(_this,thisEditForm,cityField,config,'addressField city locality postal_town sublocality administrative_area_level_4 city',mapIframe,linkAddress,addressInputMap);
		var addState = createAddressInput(_this,thisEditForm,stateField,config,'addressField state administrative_area_level_1 state',mapIframe,linkAddress,addressInputMap);
		var addCountry = createAddressInput(_this,thisEditForm,countryField,config,'addressField country',mapIframe,linkAddress,addressInputMap);
		var addZip = createAddressInput(_this,thisEditForm,zipField,config,'addressField zip postal_code',mapIframe,linkAddress,addressInputMap);

		var addressFields = [config.objectName + '.' + streetField, 
							 config.objectName + '.' + cityField, 
							 config.objectName + '.' + stateField, 
							 config.objectName + '.' + countryField, 
							 config.objectName + '.' + zipField];

		//Create a listener for the Address fields to input the address into the Google map div every time a field is changed
		//TODO: Create a latch because multiple fields trigger this and ideally we only want this to run once per address change
		_this._context.local.eventDispatcher.on(ACE.AML.Event.PROPERTY_CHANGE, function(e, eventData)
		{
			//addressFields
			if( addressFields.includes(eventData.property) )
			{
				var streetText = '';
		
				//The address fields are already ordered
				$.each(addressFields, function(index, field)
				{
					var addressComponent = _this._context.local.state.get(field)
					if(field != null && addressComponent != null && addressComponent.length > 0)
					{
						streetText += addressComponent + ' ';
					}
				});

				$(linkAddress).text(streetText);
				streetText = streetText.replace('#','%23');
				streetText = streetText.replace(',',' ');
				$(mapIframe).attr('src','https://www.google.com/maps/embed/v1/place?key=' + _this.mapsKey +'&q=' + encodeURI(streetText));
				
				if(!_this.hasInit)
				{
					_this.showButtonContainer(true);
						_this._context.local.state.put(_this.config.object + "Updated", true);
				}	
			}
		});

	}

	function populateAddressFieldsWithAddressObject(addStreet,addCity,addState,addCountry,addZip,addrId,clearOnNull,_this)
	{
		$.each(_this._config.addresses, function(key,value)
		{	
			if(value.Id == addrId)
			{
				//We are required 
				var street   = value[_this.t1cNS + 'Street__c'] ? value[_this.t1cNS + 'Street__c'] : null;
				var city     = value[_this.t1cNS + 'City__c'] ? value[_this.t1cNS + 'City__c'] : null;
				var province = value[_this.t1cNS + 'State_Province__c'] ? value[_this.t1cNS + 'State_Province__c'] : null;
				var country  = value[_this.t1cNS + 'Country__c'] ? value[_this.t1cNS + 'Country__c'] : null;
				var postCode = value[_this.t1cNS + 'Zip_Postal_Code__c'] ? value[_this.t1cNS + 'Zip_Postal_Code__c'] : null;

				addStreet._context.local.state.put(addStreet._options.property, street);
				addCity._context.local.state.put(addCity._options.property, city);
				addState._context.local.state.put(addState._options.property, province);
				addCountry._context.local.state.put(addCountry._options.property, country);
				addZip._context.local.state.put(addZip._options.property, postCode);
			}
			else if(clearOnNull && (addrId == '' || addrId == null))
			{
				addStreet._context.local.state.put(addStreet._options.property, '');
				addCity._context.local.state.put(addCity._options.property, '');
				addState._context.local.state.put(addState._options.property, '');
				addCountry._context.local.state.put(addCountry._options.property, '');
				addZip._context.local.state.put(addZip._options.property, '');
			}	
		});
	}

	function createAddressInput(_this,component,fieldName,config,classNames,mapDiv,linkAddress,addressInputMap)
	{
		if(config == null || fieldName == null || config.accessLevel == 'None')
		{
			return createAddressInput;
		}

		var customLabel;
		if (fieldName.indexOf("Street") > -1)
		{
			customLabel = config.StreetLabel;
		}
		else if (fieldName.indexOf("Country") > -1)
		{
			customLabel = config.CountryLabel;
		}
		else if (fieldName.indexOf("State") > -1)
		{
			customLabel = config.StateLabel;
		}
		else if (fieldName.indexOf("City") > -1)
		{
			customLabel = config.CityLabel;
		}
		else if (fieldName.indexOf("PostalCode") > -1)
		{
			customLabel = config.PostalCodeLabel;
		}

		var accessLevelField = fieldName.replace(config.fieldName, '') + 'Access';
		var value = returnBlankIfNull(config.record[fieldName]);
		var fieldDescResult = getFieldDescribeResult(config,fieldName,_this);

		var thisConfig = 
		{
			helpText : fieldDescResult.inlineHelpText,
			label : customLabel ? customLabel : fieldDescResult.label,
			fieldName : fieldName,
			objectName : config.objectName,
			accessLevel : config[accessLevelField],
			required : fieldDescResult.nillable == 'false'
		};

		var disabled = thisConfig.accessLevel == 'Read';

		if(!disabled)
		{
			var compareCanEdit = getVisibility(config, _this);

			disabled = !compareCanEdit;
		}			

		var elementId = getElementId(thisConfig);
		var componentContainer = _this.componentMap[elementId];

		if(componentContainer == null)
		{
			componentContainer = new SummaryCubeComponentContainer(_this,component,thisConfig);
			_this.componentMap[elementId] = componentContainer;
		}

		var thisInput = _this.inputMap[elementId];

		if(thisInput == null)
		{
			var options = {};
			options.property = config.objectName + '.' + fieldName;
			options.cssClassName  = classNames;
			options.readOnly = disabled;
			options.attributes = {'id' : elementId}

			thisInput = ACE.AML.ControllerManager.create(
				ACE.AML.Control.TextInput.CONTROLLER_NAME, 
				new ACE.AML.ControllerContext(componentContainer.editContainer, options, _this._context.local)
			);

			var classes = classNames.split(' ');

			for(var i=0;i<classes.length;i++)
			{
				if(classes[i] != 'addressField')
				{
					addressInputMap[classes[i]] = thisInput;
				}
			}

			_this.inputMap[elementId] = thisInput;
		}

		return thisInput;
	}

	function createImageItem(_this,component,config)
	{
		if(config == null || config.fieldName == null || config.accessLevel == 'None')
		{
			return createImageItem;
		}

		var value = config.record[config.fieldName];
		value = $(value).attr('src');

		if(value == null)
		{
			value = $(config.record[config.fieldName]).find('img').attr('src');
		}	

		var classNames = config.additionalClassNames;
		var parentComponent = component[0].parentElement;
		var elementId = getElementId(config);
		var componentContainer = _this.componentMap[elementId];
		var imgDiv = _this.componentMap[elementId + 'ImgDiv'];

		if(classNames == null)
		{
			classNames = '';
		}

		if(componentContainer == null)
		{
			componentContainer = $('<div Id="' + elementId + '" class="headerItem ' + classNames + '"></div>').appendTo(parentComponent);
			_this.componentMap[elementId] = componentContainer;

			imgDiv = $('<div class="' + classNames + '" style="background:url(\'' + value + '\') no-repeat;height:' + $(componentContainer).height() + 'px;"></div>').appendTo(componentContainer);
			_this.componentMap[elementId + 'ImgDiv'] = imgDiv;
		}
		else
		{
			$(componentContainer).empty().hide();
			imgDiv = $('<div class="' + classNames + '" style="background:url(\'' + value + '\') no-repeat;height:' + $(componentContainer).height() + 'px;"></div>').appendTo(componentContainer);
		}

		if(value != null)
		{
			var thisImage = new Image();
			var thisImageHeight = $(componentContainer).height();
			thisImage.src = value;
			$(thisImage).load(function()
			{
				var thisImageOriginalHeight = thisImage.naturalHeight;
				var imageWidth = ((thisImageHeight/thisImageOriginalHeight) * thisImage.naturalWidth);
				
				$(componentContainer).css('min-width', imageWidth);
				$(componentContainer).css('max-width', imageWidth);
			});

			$(imgDiv).show();

			$(componentContainer).show();
		}
		else
		{
			$(componentContainer).css('min-width', 0);
			$(componentContainer).css('max-width', 0);
		}
	}

	function createMultiSelectField(_this,component,config)
	{
		if(config == null || config.fieldName == null || config.accessLevel == 'None')
		{
			return createMultiSelectField;
		}

		var value = getValue(config);
		var elementId = getElementId(config);
		var componentContainer = _this.componentMap[elementId];

		if(componentContainer == null)
		{
			componentContainer = new SummaryCubeComponentContainer(_this,component,config);
			_this.componentMap[elementId] = componentContainer;
		}

		var disabled = (config.accessLevel == 'Read');
		var valueDivText = value || 'click here to add values';

		var thisInput = _this.inputMap[elementId];

		if(config.compareIsShow)
		{
			var show = getVisibility(config, _this);

			if(show)
			{
				$(componentContainer.subSectionItem).show();
			}
			else
			{
				$(componentContainer.subSectionItem).hide();
			}
		}
		else
		{
			var compareCanEdit = getVisibility(config, _this);

			disabled = !compareCanEdit;	
		}
		
		if(disabled)
		{
			valueDivText = '';
		}

		var picklistValues = [];
		var availPicklistValues = [];
		var selectedValues = [];

		if(config.controllingField != null)
   		{
			picklistValues = getPicklistValuesForDependantPicklist(_this,config,false);

			_this.picklistValueTracker.watchPicklistValue(config.controllingField.name+ ':' + config.objectName,function()
   			{
   				picklistValues = getPicklistValuesForDependantPicklist(_this,config,false);   				
   			});
		}
		else
		{
			if(config.picklistValues != null && config.picklistValues.length > 0)
			{
				picklistValues = config.picklistValues;
			}
		}

		var selectedItems = [];
		
		if(value != null && value != '')
		{
			var selItems = value.split(';');
			valueDivText = '';

			for(var i = 0; i<selItems.length;i++)
			{
				selectedItems.push(selItems[i]);
			}
		}

		if(picklistValues != null && picklistValues.length > 0)
		{
			for(var x=0;x<picklistValues.length;x++)
            {
				var thisPicklistValue = {id : picklistValues[x].value, data : picklistValues[x].value, label : picklistValues[x].label};
				
				var isSel = false;

				for(var i=0;i<selectedItems.length;i++)
				{
					var fldVal = selectedItems[i];
					
					if(fldVal == thisPicklistValue.data)
					{
						isSel = true;
						valueDivText += thisPicklistValue.label + ';';
					}
				}

				availPicklistValues.push(thisPicklistValue);

				if(isSel)
				{
					selectedValues.push(picklistValues[x].label);
				}
			}
		}

		var valueDiv = _this.componentMap[elementId + 'ValueDiv'];
		if(valueDiv != null)
		{
			$(valueDiv).text(valueDivText);
			$(valueDiv).prop('title',valueDivText);
			$(valueDiv).prop('textContent',valueDivText);
			$(valueDiv).prop('customTitle',valueDivText);
			$(valueDiv).prop('defaultValue',valueDivText);
			$(valueDiv).prop('innerHTML',valueDivText);
		}
		else
		{	
			valueDiv = $('<div id="' + elementId + 'ValueDiv" class="cubeComponentContainer multiSelectValue">' + valueDivText + '</div>').appendTo(componentContainer.editContainer);		
			_this.componentMap[elementId + 'ValueDiv'] = valueDiv;
		}	

		config.valueDiv = valueDiv;

		var displayDiv = _this.componentMap[elementId + 'displayDiv'];

		if(displayDiv == null)
		{
			displayDiv = $('<div class="multiSelectContainer"/>').appendTo(component);
			_this.componentMap[elementId + 'displayDiv'] = displayDiv;
			
        	$(valueDiv).click(function()
        	{
				var disabled = $(this).prop('disabled');
				if(!disabled)
				{					
			    	if($(displayDiv).is(':visible'))
					{	
				        $(displayDiv).hide(333);
				        $(displayDiv).removeClass('showHiddenContainer');
			    	}
			    	else
			    	{
			    		$(displayDiv).show(333);
				        $(displayDiv).addClass('showHiddenContainer');
			    	}
				}	
        	});
		}

		var options = {};
		
		options.property = config.objectName + '.' + config.fieldName;
		_this._context.local.state.put(options.property,selectedValues);
		options.dataProvider = availPicklistValues;
		options.mode = config.mode;
		//options.value = selectedValues;
		options.placeholder = "-- Select --";

		if(thisInput == null)
		{			
			thisInput = ACE.AML.ControllerManager.create(
				ACE.AML.Control.MultiSelectDropDown.CONTROLLER_NAME, 
				new ACE.AML.ControllerContext(displayDiv, options, _this._context.local)
			);

			thisInput.isMultiSelect = true;

			_this.inputMap[elementId] = thisInput;

			thisInput._context.local.eventDispatcher.on(ACE.AML.Event.PROPERTY_CHANGE, function(e ,eventData)
			{
				if(eventData.property == thisInput._options.property)
				{
					var valueDivText = '';
					var inputVal = eventData.newValue;

					for(var i=0;i<inputVal.length;i++)
					{
						var pkVal = inputVal[i].data || inputVal[i];
						for(var x=0;x<picklistValues.length;x++)
						{
							var pl = picklistValues[x];

							if(pkVal == pl.value)
							{
								valueDivText += pl.label + ';';
							}
						}						
					}

					if(valueDivText.length == 0)
					{
						valueDivText = 'click here to add values';
					}

					$(config.valueDiv).text(valueDivText);
					$(config.valueDiv).prop('title',valueDivText);
					$(config.valueDiv).prop('textContent',valueDivText);
					$(config.valueDiv).prop('customTitle',valueDivText);
					$(config.valueDiv).prop('defaultValue',valueDivText);
					$(config.valueDiv).prop('innerHTML',valueDivText);

					if(!_this.hasInit && 
						((eventData.oldValue && eventData.oldValue !== valueDivText.slice(0, valueDivText.length - 1)) || 
						(!eventData.oldValue && valueDivText !== 'click here to add values')))
					{
						_this.showButtonContainer(true);
						_this._context.local.state.put(_this.config.object + "Updated", true);
					}					
				}					
			});
		}

		thisInput.dataProvider = availPicklistValues;
		thisInput.value = selectedValues;
		//thisInput.selectedItems = selectedValues;

		$(displayDiv).hide();
		$(thisInput).attr('isRequired',config.required);
		$(thisInput).attr('reqLabel',config.label);		
		$(valueDiv).prop('disabled',disabled);		
	}

	function getObjectDescribeResult(config, _this)
	{
		var objectDescResult;
		for(var x=0;x<_this.describeSObjectResults.length;x++)
		{
			var objDesc = _this.describeSObjectResults[x];
			if(objDesc.name == config.objectName)
			{
				objectDescResult = objDesc;
				break;
			}
		}

		return objectDescResult;
	}

	function getFieldDescribeResult(config,fieldName,_this)
	{
		var objectDescResult = getObjectDescribeResult(config,_this);
		for(var i = 0; i < objectDescResult.fields.length; i++) 
        {
            var fieldDesc = objectDescResult.fields[i];
            var fieldDescName = fieldDesc.name;
     
            if(fieldDescName.toLowerCase() == fieldName.toLowerCase())
            {
				return fieldDesc;
            }
		}
	}

	function getPicklistValuesForDependantPicklist(_this,config,isMultiSelect)
	{
		var picklistValues = []; 
		var value = config.record[config.fieldName];
		var controllingFieldValue = config.record[config.controllingField.name];
		var selectedIndex;

		var objectDescResult = getObjectDescribeResult(config,_this);
		
		for(var i = 0; i < objectDescResult.fields.length; i++) 
        {
            var fieldDesc = objectDescResult.fields[i];
            var fieldName = fieldDesc.name;
     
            if(fieldName.toLowerCase() == config.fieldName.toLowerCase())
            {
				config.validForPicklist = fieldDesc.picklistValues;
				break;
            }
		}
		
		if(config.controllingField.name != null)
		{
			controllingFieldValue = _this.picklistValueTracker[config.controllingField.name + ':' + config.objectName];
			var isValueInPicklistMetadata = false;			
			var isParentValueValid = false;
			
			for(var t=0;t<config.validForPicklist.length;t++)
			{
				if(config.validForPicklist[t].value == value)
				{
					isValueInPicklistMetadata = true;
				}
			}
			
			if(controllingFieldValue == null)
			{
				selectedIndex = 0;
			}

			if(config.controllingField.type == 'boolean')
			{
				if(controllingFieldValue == 'true')
				{
					selectedIndex = 1;
				}
				else
				{
					selectedIndex = 0;
				}
			}
			else if(config.controllingField.type == 'picklist' && controllingFieldValue != '' && selectedIndex == null)
			{
				for(var e=0;e<config.controllingField.picklistValues.length;e++)
				{
					if(config.controllingField.picklistValues[e].value == controllingFieldValue)
					{
						selectedIndex = e;
						isParentValueValid = true;
					}
				}
			}
			
			if(isMultiSelect == false)
			{
				picklistValues.push({data : '', label : config.nullPicklistLabel, defaultSelected : (value == '')});
				if(!isValueInPicklistMetadata && value != null)
				{
					picklistValues.push({data : value, label : value, defaultSelected : true});
				}		
			}
						
			if(config.validForPicklist !== null && config.validForPicklist.length !== null)
			{		
				for(var t=0;t<config.validForPicklist.length;t++)
				{
					if(isDependentValueValidForSelectedParent(selectedIndex, config.validForPicklist[t]) == true)
					{
						var thisPicklistValue = config.validForPicklist[t];
						var isSelected = false;
						
						if(isMultiSelect == true && value != null && value.length != '')
						{
							var selVals = value.split(';');
							
							for(var i=0;i<selVals.length;i++)
							{
								var fldVal = selVals[i];
					
								if(fldVal == thisPicklistValue.value)
								{
									isSelected =true;
									break;
								}
							}
						}	
						else if(value != null)
						{
							isSelected = (thisPicklistValue.value == value);
						}
						
						picklistValues.push({data : thisPicklistValue.value, label : thisPicklistValue.label, defaultSelected : isSelected});
					}
				}
			}
		}
		
		return picklistValues;
	}

	function isDependentValueValidForSelectedParent(parentSelIndex, depValue)
	{
	    var b64 = new sforce.Base64Binary("");
	    var validFor = depValue.validFor;
	    var decodedVF = b64.decode(validFor);
	    var bits = decodedVF.charCodeAt(parentSelIndex >> 3);
	    if((bits & (0x80 >> (parentSelIndex%8))) != 0)
	    {
	        return true;
	    }
	    return false;
	}

	function Picklist(){}

	if (!Picklist.prototype.watchPicklistValue) {
		Object.defineProperty(Picklist.prototype, "watchPicklistValue", {
			  enumerable: false
			, configurable: true
			, writable: false
			, value: function (prop, handler) 
			{
				var oldval = this[prop], getter = function (){return oldval;}, setter = function (newval) 
				{
					if (oldval !== newval) 
					{						
						oldval = newval;
						handler.call(this, prop, oldval, newval);
					}
					else { return false }
				}
				;
				
				if (delete this[prop]) { // can't watch constants
					Object.defineProperty(this, prop, {
						  get: getter
						, set: setter
						, enumerable: true
						, configurable: true
					});
				}
			}
		});
	}

	if (!Picklist.prototype.unwatchPicklistValue) {
		Object.defineProperty(Picklist.prototype, "unwatchPicklistValue", {
			  enumerable: false
			, configurable: true
			, writable: false
			, value: function (prop) 
			{
				var val = this[prop];
				delete this[prop]; // remove accessors
				this[prop] = val;
			}
		});
	}

	function getElementId(config)
	{
		return config.fieldName + ':' + config.objectName;
	}

	function getDateTimeValue(config, _this)
	{
		var value = config.record[config.fieldName];
		
		if($.isNumeric(value) && value > 0)
		{
			if(config.handler == 'DateTime')
			{
				value = new Date(value);
			}
			else
			{
				var offsetValue = new Date().getTimezoneOffset();
				var millisecondOffsetValue = offsetValue * 60000;
				value = new Date(value + millisecondOffsetValue);
			}	
		}
		else if($.type(value) == 'string')
		{		
			var datevalue = value.split('T')[0];
			var time = value.split('T')[1];
			var thisYear = datevalue.split('-')[0];
			var thisMonth = datevalue.split('-')[1];
			var thisDay = datevalue.split('-')[2];
			var thisHour = 0;
			var thisMinute = 0;
			
			if(time !== null)
			{
				thisHour = time.split(':')[0];
				thisMinute = time.split(':')[1];
			}
			
			value = new Date(thisYear, thisMonth -1, thisDay,thisHour,thisMinute);
			//config.record[config.fieldName] = value;			
		}
		else
		{
			value = null;
		}

			_this._context.local.state.put(config.objectName + '.' + config.fieldName,value);

		return value;
	}

	function getValue(config)
	{
		var thisLinkFields = config.fieldName.split('.');
		thisData = config.record;
		
		for(var x=0;x<thisLinkFields.length;x++)
		{
			if(thisData != null)
			{
				thisData = thisData[thisLinkFields[x]];
			}
		}
		
		if(thisData == null)
		{
			thisData = '';
		}

		return thisData;
	}

	function getNameValue(config)
	{
		var thisLinkFields = config.lookupNameField.split('.');

		thisData = config.record;
	
		for(var x=0;x<thisLinkFields.length;x++)
		{
			if(thisData != null)
			{
				thisData = thisData[thisLinkFields[x]];
			}		
		}
		
		if(thisData == null)
		{
			thisData = '';
		}

		return thisData;
	}

	function SummaryCubeComponentContainer(_this,component,config)
	{
		this.helpText = config.helpText

		if(this.helpText == null)
		{
			this.helpText = '';
		}

		this.disabled = (config.accessLevel == 'Read');
		
		this.elementId = config.fieldName + ':' + config.objectName;

		if(this.subSectionItem == null)
		{			
			this.subSectionItem = $('<div id="' + this.elementId + 'subSectionItem" class="componentContainer subSectionItem"></div>').appendTo(component);
			this.labelComponent =  $('<div title="' + this.helpText  + '" class="label">' + config.label + '</div>').appendTo(this.subSectionItem);
			this.editContainer = $('<div  id="' + this.elementId + 'editContainer" class="cubeComponentContainer"></div>').appendTo(this.subSectionItem);

			if(config.required)
			{	
				$(this.subSectionItem).addClass('required');
			}
		}
		
		return this;
	}

	function createButton(_this,component,button)
	{
		var foundValue = true;
		var type = button.type;
		var objName = button.objectName;
		var fldName  = button.fieldName;
		var elementId = objName + ':' + fldName;
		var mergeVal;
		var urlTar = button.urlTarget;
		var urlPre = button.urlPrefix;
		var urlSuf = button.urlSuffix;
		var label  = button.label;
		var fullURL;

		if(button.buttonId != null)
		{
			elementId = button.buttonId;
		}

		if(label == null)
		{
			label = '';
		}

		if(urlTar == null)
		{
			urlTar = '';
		}

		if(urlPre == null)
		{
			urlPre = '';
		}

		if(urlSuf == null)
		{
			urlSuf = '';
		}

		if(objName != null && fldName != null)
		{
			var objVal = _this._config['record'];
			if(objVal != null)
			{
				mergeVal = objVal[fldName];
				fullURL = urlPre + mergeVal + urlSuf;
			}
			else
			{
				foundValue = false;
			}			
		}
		else if(objName != null && fldName == null)
		{
			mergeVal = _this._config[objName];
			fullURL = urlPre + mergeVal + urlSuf;
		}

		var thisButton = _this.buttonMap[elementId];

		if(thisButton == null)
		{
			thisButton = $('<button style="margin:0 5px" title="' + label + '"></button>').appendTo(component);
			_this.buttonMap[elementId] = thisButton;

			if(button.svgIcon != null)
			{
				$(thisButton).append(button.svgIcon);
				$(thisButton).attr('class','genericButton');
			}
			else if(button.classNames != null)
			{
				$(thisButton).attr('class',button.classNames);
			}

			if(type == 'Event')
			{
				$(thisButton).click(function()
				{
					var entity = $(this).attr('mergeVal');
					var action = $(this).attr('action');

					if(action != null && entity != null && action.length > 0 && entity.length > 0)
					{	
						var launchEvent  = action + entity;
						ACE.CustomEventDispatcher.trigger(launchEvent);
					}
					else if(action != null && action.length > 0)
					{	
						ACE.CustomEventDispatcher.trigger(action);
					}					
				});
			}
			else if(type == 'URL')
			{
				$(thisButton).click(function()
				{
					var fullURL = $(thisButton).attr('fullURL');
					var urlTar = $(thisButton).attr('target');
					window.open(fullURL, urlTar);
				});
			}
		}
			
		$(thisButton).attr('mergeVal',mergeVal);
		$(thisButton).attr('action',button.action);
		
		if(fullURL != null)
		{
			$(thisButton).attr('fullURL',fullURL);
		}
		
		$(thisButton).attr('target',urlTar);


		if(foundValue)
		{
			$(thisButton).show();
		}
		else
		{
			$(thisButton).hide();
		}		
	}


	function getVisibility(fieldConfig, _this)
	{
		var compareField = fieldConfig.compareField;
		var compareObjectName = fieldConfig.compareObjectName;
		var compareType = fieldConfig.compareType;
		var compareValue = fieldConfig.compareValue;
		var compareValueType = fieldConfig.compareValueType;
		var compareValueField = fieldConfig.compareValueField;

		if(compareField == null)
		{
			if(fieldConfig.accessLevel != null)
			{
				if(fieldConfig.accessLevel == 'Edit')
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return true;
			}
		}

		var fieldValue;
		
		if(_this._config.record != null)
		{
			var thisLinkFields = compareField.split('.');
			fieldValue = _this._config.account;

			for(var x=0;x<thisLinkFields.length;x++)
			{
				if(fieldValue != null)
				{
					fieldValue = fieldValue[thisLinkFields[x]];
				}
			}
		}
		else
		{
			fieldValue = null;
		}

		if(compareValueField != null)
		{
			if(_this._config.record != null)
			{
				compareValue = _this._config.record[compareValueField];
			}
		}
		else // Cast the Compare value to the proper type
		{
			if(compareValueType == 'string')
			{
				compareValue = '' + compareValue;
			}
			else if(compareValueType == 'boolean')
			{
				compareValue = (compareValue.toLowerCase() == 'true');
			}
			else if(compareValueType == 'date')
			{
				if(compareValue == 'today')
				{
					var todayDate = new Date();	
					compareValue = new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate()).getTime();
				}
				else if(compareValue == 'now')
				{
					compareValue = new Date().getTime();
				}
				else
				{
					compareValue = new Date(compareValue).getTime();
				}	
			}
			else if(compareValueType == 'number')
			{
				compareValue = Number(compareValue);
			}
			else if(compareValueType == 'null')
			{
				compareValue = null;
			}
		}

		if(compareType == 'equals')
		{
			return (fieldValue == compareValue);
		}
		else if(compareType == 'notequals')
		{
			return (fieldValue != compareValue);
		}
		else if(compareType == 'greaterthan')
		{
			return (fieldValue > compareValue);
		}
		else if(compareType == 'lessthan')
		{
			return (fieldValue < compareValue);
		}
		else if(compareType == 'greaterthanequals')
		{
			return (fieldValue >= compareValue);
		}
		else if(compareType == 'lessthanequals')
		{
			return (fieldValue <= compareValue);
		}
		else if(compareType == 'contains')
		{
			return (fieldValue.indexOf(compareValue) != -1);
		}
		else 
		{
			return true;
		}
	}

	SummaryCubeDetail.prototype._init = function(config)
	{
		if(config == null)
		{
			return SummaryCubeDetail.prototype._init;
		}

		this._config = config;
		
		var _this = this;

		if(this.hasInit == null)
		{
			this.sections = {};
			this.timezoneOffset;

			this.userLocale = ACE.Salesforce.userLocale.replace('_','-');

			this.t1cNS = config.t1cNS;

			// Figuring out the difference between SFDC Time and Machine Time, show in SFDC Time
			this.timezoneOffset = config.timeZoneOffset;
			this.timeZone = moment.tz.guess();
			this.machineOffset = new Date().getTimezoneOffset() * -60000;
			this.timezoneOffset = this.timezoneOffset - this.machineOffset;						

			sforce.connection.sessionId = ACE.Salesforce.sessionId;	
		
			this.describeSObjectResults = sforce.connection.describeSObjects([config.objectName]);
			
			this.componentHandlers = {
				'AccountNameHeader' : createAccountNameHeaderItem,
				'ContactNameHeader' : createContactNameHeaderItem,
				'Input' : createInput,
				'Lookup' : createLookup,
				'HeaderItem' : createHeaderItem,
				'Image' : createImageItem,
				'Picklist' : createPicklist,
				'MultiSelect': createMultiSelectField,
				'Date' : createDatePicker,
				'DateTime' : createDateTimePicker,
				'Checkbox' : createCheckBoxItem,
				'Link' : createLink,
				'Address' : createAddressItem,
				'TextArea' : createNotesItem,
				'Banner' : createBanner,
				'Currency' : createCurrencyInput,
				'BoolDisplay': createBoolDisplay,
				'Icon': createIcon
			}

			this.componentMap = {};
			this.buttonMap = {};
			this.buttonSectionMap = {};
			this.containerMap = {};
			this.inputMap = {};
			this.addressInputMap = {};
			this.picklistValueTracker = new Picklist();
			this.dependentPicklists = {};
			this.hasInit = true;
		}

		var sectionName = this._config.objectName + 'Section';
				
		if(this.sections[sectionName] == null)
		{	
			this.sections[sectionName] = this.setupSectionHeader(config,this._config.objectName);
		}

		this.hasInit = true;
		this.setupSection(config,this._config.objectName);
		this.hasInit = false;
	}

	// Cube Required Functions

	SummaryCubeDetail.prototype.populate = function(selectedItem)
	{
		this.selectedItem = selectedItem;
	
		if(this.isReady)
		{
			this._handle.setData(eventData.selectedItem.data);   
		}
	};

	SummaryCubeDetail.prototype._loadLayout = function(target)
	{		
		this._parentContainer = target.target;
		this.createChildren(target.target);
	}
		
	SummaryCubeDetail.prototype.cancelFunction = function()
	{		
		this.showButtonContainer(false);
		this.setData(this.dataProvider);
	}

	SummaryCubeDetail.prototype.cancel = function()
	{
		//Cancel the current pull transaction
	}

	SummaryCubeDetail.prototype.setData = function(dataProvider)
	{	
		var dataProviderId;

		for(var x=0;x<this.config.fields.length;x++)
		{
			if(dataProvider[this.config.fields[x]] != null)
			{
				dataProviderId = dataProvider[this.config.fields[x]];
				break;
			}
		}

		this.dataProvider = dataProvider;
		this.getInfo(dataProviderId);
	}

	SummaryCubeDetail.prototype.getInfo = function(dataProviderId)
	{
		var _this = this;

		if(dataProviderId != null)
		{	
			ACE.Remoting.call(/*ACE.Namespace.CORE_APEX +*/ "SummaryCubeDetailController.getInfo", [dataProviderId, ACE.Salesforce.userId, this.featurePath], function(result,event)
			{ 
				if(event.status)
				{	
					if(result == null)
					{	
						$(_this._context.target).hide();
					}	
					else
					{
						$(_this._context.target).show();
						_this._context.local.state.put(result.recordStateName, result.record);
						_this._init(result);

						if (_this.selectedType)
						{
							var target = "#" + _this.selectedType.toLowerCase() + "Details";
							if ($(target)[0])
							{
								$('.powerCubeContent').stop().animate(
								{
									scrollTop: $(target)[0].offsetTop - (65 + $(".cubeNavBar").height())
								}, 1000);
							}
						}
					}
				}
				else
				{
					console.log(result);
					console.log(event);
				}	
			});
		}
	}
	
	SummaryCubeDetail.prototype.resizeHandler = function()
	{	
		//Should resize the cube face elements will be called when elements are resized on the form
		var _this = this;
		$(_this.cubeContent).removeClass('doubleColumn');
		$(_this.cubeContent).removeClass('singleColumn');
		
		if($(_this.cubeContent).width() < 785)
		{
			$(_this.cubeContent).addClass('singleColumn');		
		}
		else 
		{
			$(_this.cubeContent).addClass('doubleColumn');
		}			
	}
	
	SummaryCubeDetail.prototype.createChildren = function(parent)
	{		
		this.cubeContent = parent;
		this.lastPullTimeout = null;
	}
	
	$.extend(true, window,
	{
		"Custom":
		{
			"SummaryCubeDetail": SummaryCubeDetail
		}	
	});

	ACE.AML.ControllerManager.register("alm:SummaryCubeDetail", SummaryCubeDetail);
//# sourceURL=SummaryCubeDetail.js	
})(jQuery);