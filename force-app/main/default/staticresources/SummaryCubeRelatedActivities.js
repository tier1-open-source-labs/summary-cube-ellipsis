/**
 * Name: SummaryCubeRelatedActivities.js 
 *
 * Confidential & Proprietary, 2021 Tier1 Financial Solutions Inc.
 * Property of Tier1 Financial SolutionsInc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1 Financial Solutions.
 */
//=============================================================================
(function($)	
{			
	var _super = ACE.inherit(SummaryCubeRelatedActivities, ALM.AbstractCubeFace);

	/**
	 * Constructor
	 *
	 * @class      SummaryCubeRelatedActivities (ellipsis context)
	 */
	
	function SummaryCubeRelatedActivities(context)
	{
		_super.constructor.call(this, context);
		
		this._loadLayout(context);
		this._context = context;
		this.config = context.options;
		
		var _this = this;

		this._context.local.eventDispatcher.on('ec-CubePopulate', function(e, eventData)
        {
			if(eventData != null)
			{
				_this.selectedItem = eventData.data;
				_this.setData(eventData.data);  
			}			   
		});

		this.featurePath = this.config.path;

        this._context.readyHandler();
	}

	
	SummaryCubeRelatedActivities.prototype._init = function(config)
	{
		if(config == null)
		{
			return SummaryCubeRelatedActivities.prototype._init;
		}

		this._config = config;
		
		var _this = this;

		if(this.hasInit == null)
		{
			this.sections = {};
			this.timezoneOffset;

			this.userLocale = ACE.Salesforce.userLocale.replace('_','-');

			this.t1cNS = config.t1cNS;

			// Figuring out the difference between SFDC Time and Machine Time, show in SFDC Time
			this.timezoneOffset = config.timeZoneOffset;
			this.timeZone = moment.tz.guess();
			this.machineOffset = new Date().getTimezoneOffset() * -60000;
			this.timezoneOffset = this.timezoneOffset - this.machineOffset;						

			this.componentMap = {};
			this.buttonMap = {};
			this.buttonSectionMap = {};
			this.containerMap = {};
			
			this.setupFormatterMap();
			var sectionName = 'activitySection';
				
			if(this.sections[sectionName] == null)
			{	
				this.sections[sectionName] = this.setupSectionHeader(config);
			}

			createGrid(this,this.sections[sectionName],config.activityColumns);

			this.hasInit = true;

			if(config.viewRecords)
			{
				this.grid.grid.onDblClick.subscribe(function(e, args) 
				{				   
					var data = _this.grid.grid.getData().getItems();
					
					if(data !== null && data.length !== null)
					{
						var rowData = data[args.row];        
						
						if(rowData.launchEvent == 'launchTask')
						{
							ACE.CustomEventDispatcher.trigger(ACE.Event.SHOW_FOLLOW_UP_TASK_DIALOG,{ "dataProvider" : {id: rowData.id, type: "Task"}});
						}
						else if(rowData.launchEvent == 'launchEvent')
						{
							ACE.CustomEventDispatcher.trigger(ACE.Event.SHOW_EVENT_DIALOG + '?id=' +  rowData.id + '&readOnly=false',null,rowData);
						}
						else if(rowData.launchEvent != null)
						{	
							var launchEvent  = rowData.launchEvent + rowData.id;

							if(rowData.isLaunchURL)
							{
								window.open(launchEvent,'window','toolbar=no, menubar=no, resizable=yes');
							}
							else
							{
								ACE.CustomEventDispatcher.trigger(launchEvent,null,rowData);
							}	
						}
						else
						{
							launchRecord(rowData.id);
						}
					}	
				});
			}
		}

		if(config.buttons != null)
		{
			for(var x=0; x<config.buttons.length;x++)
			{
				createButton(this, this.buttonSectionMap['activitySection'], config.buttons[x]);
			}
		}

		this.gridEventIcon = config.gridEventIcon;
		this.gridTaskIcon = config.gridTaskIcon;
		this.gridCallReportIcon = config.gridCallReportIcon;

		this.grid.dataProvider.setItems(cleanRows(config.activityColumns, config.activities,_this));
	}

	function launchRecord(pageId)
	{
		if(pageId != null)
		{
			window.open('/' + pageId + '?isdtp=vw','window','toolbar=no, menubar=no, resizable=yes');
		}
	}

	SummaryCubeRelatedActivities.prototype.setupSectionHeader = function(config, objectName)
	{
		if(config == null)
		{
			return null;
		}

		var order = config.detailOrder;

		if(this.sections['activitySection'] == null)
		{
			var thisRelatedSectionHeader = $('<div Id="activityDetails" order="' + order + '" class="section sectionDraggable p-all-m"></div>').appendTo(this.cubeContent);
			var sectHeader = $('<div class="sectionHeader activityHeader"></div>').appendTo(thisRelatedSectionHeader);
			var titleContainer = $('<div class="headerItem headerTitleContainer"></div>').appendTo(sectHeader);
			$('<div class="primaryLabel">' + config.sectionLabel + '</div>').appendTo(titleContainer);
			var sectionActionButtons = $('<div class="headerItem actionButtons"></div>').appendTo(sectHeader);

			this.componentMap['activitySection'] = thisRelatedSectionHeader;
			this.buttonSectionMap['activitySection'] = sectionActionButtons;

			this.sections['activitySection'] = {};

            var thisSection = $('<div Id="activityDetails" order="' + order + '" class="subSection cubeDataGridContainer section activitySummary" style="padding:0px;"></div>').appendTo(thisRelatedSectionHeader);
			this.sections['activitySection']['mainDiv'] = thisSection;
			
			var thisSectionHeader = $('<div class="sectionHeader activityHeader"></div>').appendTo(thisSection);
			this.sections['activitySection']['activityHeader'] = thisSectionHeader;

			var thisSectionTitle = $('<div class="headerItem headerTitleContainer"></div>').appendTo(thisSectionHeader);
			this.sections['activitySection']['activityHeaderTitleContainer'] = thisSectionTitle;

			this.buttonSectionMap['activity'] = $('<div class="headerItem actionButtons"></div>').appendTo(thisSectionHeader);

			var thisRelatedSectionHeader = this.componentMap['activitySection'];
		}

		return this.sections['activitySection'];
	} 

	SummaryCubeRelatedActivities.prototype.setupFormatterMap = function()
	{
		var _this = this;
		this.formatterMap = {};

		function defaultFormatter(row, cell, value, columnDef, dataContext)
		{
			if(row == null)
			{
				return defaultFormatter;
			}
	
			if (value == null) return ''; 
	
				return $p.escapeHTML(value.toString()); 
		}
		
		this.formatterMap['defaultFormatter'] = defaultFormatter;
		
		function gridLinkFormatter(row, cell, value, columnDef, dataContext)
		{
			if(row == null)
			{
				return gridLinkFormatter;
			}
	
			if(value == null) return '';
			
			return value.toString();
		}
		
		this.formatterMap['gridLinkFormatter'] = gridLinkFormatter;
		
		function gridFullLinkFormatter(row, cell, value, columnDef, dataContext)
		{
			if(row == null)
			{
				return gridFullLinkFormatter;
			}
	
			if(value == null) return '';
			
			return value.toString();
		}
		
		this.formatterMap['gridFullLinkFormatter'] = gridFullLinkFormatter;
	
		function dateFormatter(row, cell, value, columnDef, dataContext)
		{	
			if(row == null)
			{
				return dateFormatter;
			}		
			if (value == null || value == '') return ''; 
			var datesTimeZoneOffset = moment(value).tz(_this.timeZone)._offset;
			var valueWithOffset = value + (datesTimeZoneOffset * -60000);
			return (new Date(valueWithOffset)).toLocaleDateString(_this.userLocale); 
		}

		this.formatterMap['dateFormatter'] = dateFormatter;

		function dateTimeFormatter(row, cell, value, columnDef, dataContext)
		{	
			if(row == null)
			{
				return dateTimeFormatter;
			}		
			if (value == null || value == '') return ''; 

			var valueWithOffset = value + _this.timezoneOffset;
			var dateTimeString = new Date(valueWithOffset).toLocaleString(_this.userLocale);// + ' ' + new Date(valueWithOffset).toLocaleTimeString(userLocale);
			return dateTimeString;
		}

		this.formatterMap['dateTimeFormatter'] = dateTimeFormatter;

		function sobjectIconFormatter(row, cell, value, columnDef, dataContext)
		{
			if(row == undefined)
			{
				return sobjectIconFormatter;
			}

			if(value == 'Task')
			{
				return _this.gridTaskIcon.toString();
			}
			else if(value == 'Event')
			{
				return _this.gridEventIcon.toString();
			}
			else if(value == _this.t1cNS + 'Call_Report__c')
			{
				return _this.gridCallReportIcon.toString();
			}
			return null;
		}
		this.formatterMap['sobjectIconFormatter'] = sobjectIconFormatter;
	}
	// Cube Required Functions

	SummaryCubeRelatedActivities.prototype.populate = function(selectedItem)
	{
		this.selectedItem = selectedItem;
	
		if(this.isReady)
		{
			this._handle.setData(eventData.selectedItem.data);   
		}
	};

	SummaryCubeRelatedActivities.prototype._loadLayout = function(target)
	{		
		this._parentContainer = target.target;
		this.createChildren(target.target);
	}
		
	SummaryCubeRelatedActivities.prototype.cancelFunction = function()
	{		
		this.showButtonContainer(false);
		this.setData(this.dataProvider);
	}

	SummaryCubeRelatedActivities.prototype.cancel = function()
	{
		//Cancel the current pull transaction
	}

	SummaryCubeRelatedActivities.prototype.setData = function(dataProvider)
	{	
		this.dataProvider = dataProvider;
		this.getInfo(dataProvider.Id);
	}

	SummaryCubeRelatedActivities.prototype.getInfo = function(dataProviderId)
	{
		var _this = this;

		if(dataProviderId != null)
		{	
			ACE.Remoting.call(/*ACE.Namespace.CORE_APEX +*/ "SummaryCubeRelatedActivityController.getRecentActivities", [dataProviderId, ACE.Salesforce.userId, this.config.recordLimit, this.featurePath], function(result,event)
			{ 
				if(event.status)
				{	
					$(_this._context.target).show();
					_this._init(result);	
				}
				else
				{
					console.log(result);
					console.log(event);
				}	
			});
		}
	}
	
	SummaryCubeRelatedActivities.prototype.resizeHandler = function()
	{	
		//Should resize the cube face elements will be called when elements are resized on the form
		if(this.grid != null)
		{
			this.grid.resizeCanvas();				
		}
	}
	
	SummaryCubeRelatedActivities.prototype.createChildren = function(parent)
	{		
		this.cubeContent = parent;
		this.lastPullTimeout = null;
	}
	
	$.extend(true, window,
	{
		"Custom":
		{
			"SummaryCubeRelatedActivities": SummaryCubeRelatedActivities
		}	
	});

	function cleanRows(columnDefs,rows,_this)
	{
		var dateFieldMap = {};
		var linkFieldMap = {};

		for(var x=0;x<columnDefs.length;x++)
		{
			var columnDef = columnDefs[x];
			if(columnDef != null)
			{
				if(columnDef.fieldType == 'Date' ||columnDef.fieldType == 'DateTime')
				{
					dateFieldMap[columnDef.dataField] = columnDef.fieldType;
				}
				
				if(columnDef.formatter == 'gridLinkFormatter')
				{
					linkFieldMap[columnDef.dataField] = 'gridLinkFormatter:' + columnDef.linkField;
				}
				else if(columnDef.formatter == 'gridFullLinkFormatter')
				{
					linkFieldMap[columnDef.dataField] = 'gridFullLinkFormatter:' + columnDef.linkField;
				}
			}
		}

		for(var y=0; y < rows.length; y++)
		{
			var row = rows[y];
			if(row.id == null && row.Id != null)
			{
				row.id = row.Id;
			}

			for (var property in linkFieldMap) 
			{
				if (!linkFieldMap.hasOwnProperty(property)) continue;	
				
				var formatterName = linkFieldMap[property].split(':')[0];
				var thisLinkField = linkFieldMap[property].split(':')[1];
				var thisLinkFields = thisLinkField.split('.');
				var thisData = row;
				
				for(var x=0;x<thisLinkFields.length;x++)
				{
					thisData = thisData[thisLinkFields[x]];
				}
		
				var linkFld = 'recordHREF';
				if(thisLinkField != 'Id')
				{
					linkFld = thisLinkField;
				}

				if(formatterName == 'gridLinkFormatter')
				{
					row[linkFld] = _this.coverageURLPrefix + thisData + _this.coverageURLSuffix;
				}
				else if(formatterName == 'gridFullLinkFormatter')
				{
					row[linkFld] = thisData;
				}
			}
						
			for (var property in dateFieldMap) 
			{			
			    if (!dateFieldMap.hasOwnProperty(property)) continue;
			    
			    var thisData = row[property];
			    // Generically go through the SForce object to get at the property if needed
			    if(thisData == null)
			    {	
				    thisLinkFields = property.split('.');
					thisData = row;
					
					for(var x=0;x<thisLinkFields.length;x++)
					{
						thisData = thisData[thisLinkFields[x]];
					}
			    }
			    
			    var tempDate = new Date(thisData).getTime();
			
				if(isNaN(tempDate) || tempDate == 0)
				{
					row[property] = null;
				}
				else
				{
					row[property] = tempDate;
				}			   
			}
		}

		return rows;
	}

	function createGrid(_this,section,columnDefs)
	{
		var activityGrid = new ACEDataGrid($(section.mainDiv) ,{  
			forceFitColumns: true, 
			explicitInitialization: true,
			editable: true,																					
			headerRowHeight: 40
		});

		var columns = [];
		
		for(var x=0;x<columnDefs.length;x++)
		{
			var columnDef = columnDefs[x];
			if(columnDef != null)
			{
				var formatterName = columnDef.formatter;
				var column =  {id: columnDef.dataField, resizable: columnDef.resizable, field: columnDef.dataField, width: columnDef.width, name: columnDef.label, sortable: true, fieldType: columnDef.fieldType, linkField : columnDef.linkField, featureName : columnDef.featureName};
				if(formatterName != null && formatterName.length > 0)
				{
					column['formatter'] = _this.formatterMap[formatterName];
					if(formatterName == 'gridFullLinkFormatter' || formatterName == 'gridLinkFormatter')
					{
						column['cssClass'] = 'link';
					}	
				}
				else
				{
					column['formatter'] = _this.formatterMap['defaultFormatter'];
				}
				columns.push(column);
			}
		}		

		activityGrid.setColumns(columns);
		activityGrid.grid.init();
		
		_this.grid = activityGrid;
	}

	function createButton(_this,component,button)
	{
		var foundValue = false;
		var type = button.type;
		var objName = button.objectName;
		var fldName  = button.fieldName;
		var elementId = objName + ':' + fldName;
		var mergeVal;
		var urlTar = button.urlTarget;
		var urlPre = button.urlPrefix;
		var urlSuf = button.urlSuffix;
		var label  = button.label;
		var fullURL;

		if(button.buttonId != null)
		{
			elementId = button.buttonId;
		}

		if(label == null)
		{
			label = '';
		}

		if(urlTar == null)
		{
			urlTar = '';
		}

		if(urlPre == null)
		{
			urlPre = '';
		}

		if(urlSuf == null)
		{
			urlSuf = '';
		}

		if(objName != null && fldName != null)
		{
			mergeVal = _this._context.local.state.get(objName + '.' + fldName);

			if(mergeVal != null)
			{
				fullURL = urlPre + mergeVal + urlSuf;
				foundValue = true;
			}
		}		

		var thisButton = _this.buttonMap[elementId];

		if(thisButton == null)
		{
			thisButton = $('<button style="margin:0 5px" title="' + label + '"></button>').appendTo(component);
			_this.buttonMap[elementId] = thisButton;

			if(button.svgIcon != null)
			{
				$(thisButton).append(button.svgIcon);
				$(thisButton).attr('class','genericButton');
			}
			else if(button.classNames != null)
			{
				$(thisButton).attr('class',button.classNames);
			}

			if(type == 'Event')
			{
				$(thisButton).click(function()
				{
					var entity = $(this).attr('mergeVal');
					var action = $(this).attr('action');

					if(action != null && entity != null && action.length > 0 && entity.length > 0)
					{	
						var launchEvent  = action + entity;
						ACE.CustomEventDispatcher.trigger(launchEvent);
					}
					else if(action != null && action.length > 0)
					{	
						ACE.CustomEventDispatcher.trigger(action);
					}					
				});
			}
			else if(type == 'URL')
			{
				$(thisButton).click(function()
				{
					var fullURL = $(thisButton).attr('fullURL');
					var urlTar = $(thisButton).attr('target');
					window.open(fullURL, urlTar);
				});
			}
		}
			
		$(thisButton).attr('mergeVal',mergeVal);
		$(thisButton).attr('action',button.action);
		
		if(fullURL != null)
		{
			$(thisButton).attr('fullURL',fullURL);
		}
		
		$(thisButton).attr('target',urlTar);


		if(foundValue)
		{
			$(thisButton).show();
		}
		else
		{
			$(thisButton).hide();
		}		
	}


	ACE.AML.ControllerManager.register("alm:SummaryCubeRelatedActivities", SummaryCubeRelatedActivities);
//# sourceURL=SummaryCubeRelatedActivities.js	
})(jQuery);