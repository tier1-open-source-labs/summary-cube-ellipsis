/**
 * Name: SummaryCubeInterests.js 
 *
 * Confidential & Proprietary, 2021 Tier1 Financial Solutions Inc.
 * Property of Tier1 Financial SolutionsInc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1 Financial Solutions.
 */
//=============================================================================
(function($)	
{			
	var _super = ACE.inherit(SummaryCubeInterests, ALM.AbstractCubeFace);

	/**
	 * Constructor
	 *
	 * @class      SummaryCubeInterests (ellipsis context)
	 */
	
	function SummaryCubeInterests(context)
	{
		_super.constructor.call(this, context);
		
		this._loadLayout(context);
		this._context = context;
		this.config = context.options;
		
		var _this = this;

		this._context.local.eventDispatcher.on('ec-CubePopulate', function(e, eventData)
        {
			if(eventData != null)
			{
				_this.selectedItem = eventData.data;
				_this.setData(eventData.data);  
			}			   
		});

		this.featurePath = this.config.path;

        this._context.readyHandler();
	}

	SummaryCubeInterests.prototype.setupRelatedListSectionHeader = function(order,label,type)
	{
		if(order == null)
		{
			return null;
		}

		var thisRelatedSectionHeader = this.componentMap[type];
		var returnDiv;

		if(thisRelatedSectionHeader == null)
		{
			thisRelatedSectionHeader = $('<div Id="' + type + 'Details" order="' + order + '" class="section sectionDraggable p-all-m ' + type + 'Section"></div>').appendTo(this.cubeContent);
			var sectHeader = $('<div class="sectionHeader ' + type + 'Header"></div>').appendTo(thisRelatedSectionHeader);
			var titleContainer = $('<div class="headerItem headerTitleContainer"></div>').appendTo(sectHeader);
			$('<div class="primaryLabel">' + label + '</div>').appendTo(titleContainer);
			returnDiv = $('<div></div>').appendTo(thisRelatedSectionHeader);
			this.componentMap[type] = thisRelatedSectionHeader;
		}

		return returnDiv;
	}
	
	SummaryCubeInterests.prototype._init = function(config)
	{
		if(config == null || config.enabled == false)
		{
			return SummaryCubeInterests.prototype._init;
		}
		
		var _this = this;

		if(this.hasInit == null)
		{
			this.sections = {};
			this.t1cNS = config.t1cNS;

			this.componentMap = {};
				
			this.interestSection = this.setupRelatedListSectionHeader(config.interestSectionOrder,config.sectionLabel,'interests');

			this.hasInit = true;
		}

		$(this.interestSection).empty();

		if(config.interests.length == 0)
		{
			$('<div class="subSectionItem interestContainer"><div class="name">No Interests Found</div></div>').appendTo(this.interestSection);
		}
		else
		{
			$.each(config.interests,function(key,value)
			{
				var intName = value[_this.t1cNS + 'Interest_Subject__r']['Name'];
				var intReason = value[_this.t1cNS + 'Reason__c'];
				var interestItem = $('<div class="subSectionItem interestContainer"/>').appendTo(_this.interestSection);
				var tickerName = $('<div class="name">' + intName + '</div>').appendTo(interestItem);
				$('<div class="type">' + intReason + '</div>').appendTo(interestItem);
				
				if(value[_this.t1cNS + 'Is_Disinterest__c'] == true)
				{
					$(tickerName).css('color','red');
				}
			});
		}
	}

	// Cube Required Functions

	SummaryCubeInterests.prototype.populate = function(selectedItem)
	{
		this.selectedItem = selectedItem;
	
		if(this.isReady)
		{
			this._handle.setData(eventData.selectedItem.data);   
		}
	};

	SummaryCubeInterests.prototype._loadLayout = function(target)
	{		
		this._parentContainer = target.target;
		this.createChildren(target.target);
	}
		
	SummaryCubeInterests.prototype.cancelFunction = function()
	{		
		this.showButtonContainer(false);
		this.setData(this.dataProvider);
	}

	SummaryCubeInterests.prototype.cancel = function()
	{
		//Cancel the current pull transaction
	}

	SummaryCubeInterests.prototype.setData = function(dataProvider)
	{	
		this.dataProvider = dataProvider;
		this.getInfo(dataProvider.Id);
	}

	SummaryCubeInterests.prototype.getInfo = function(dataProviderId)
	{
		var _this = this;

		if(dataProviderId != null)
		{	
			ACE.Remoting.call(/*ACE.Namespace.CORE_APEX +*/ "SummaryCubeInterestController.getInterests", [dataProviderId, ACE.Salesforce.userId, this.featurePath], function(result,event)
			{ 
				if(event.status)
				{	
					$(_this._context.target).show();
					_this._init(result);	
				}
				else
				{
					console.log(result);
					console.log(event);
				}	
			});
		}
	}
	
	SummaryCubeInterests.prototype.resizeHandler = function(){}
	
	SummaryCubeInterests.prototype.createChildren = function(parent)
	{		
		this.cubeContent = parent;
		this.lastPullTimeout = null;
	}
	
	$.extend(true, window,
	{
		"Custom":
		{
			"SummaryCubeInterests": SummaryCubeInterests
		}	
	});

	ACE.AML.ControllerManager.register("alm:SummaryCubeInterests", SummaryCubeInterests);
//# sourceURL=SummaryCubeInterests.js	
})(jQuery);