/**
 * Name: SummaryCubeNavBar.js 
 *
 * Confidential & Proprietary, 2021 Tier1 Financial Solutions Inc.
 * Property of Tier1 Financial SolutionsInc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1 Financial Solutions.
 */

(function($)
{   
    var _super = ACE.inherit(SummaryCubeNavBar, ALM.AbstractCubeFace);

    function SummaryCubeNavBar(context)
    {
        _super.constructor.call(this, context);

        var _this = this;
		
		this._context = context;

        this.featurePath = context.options.path;

        this.nav = $('<nav class="cubeNavBar"></nav>').appendTo(this._context.target);

		this._context.local.eventDispatcher.on('ec-CubePopulate', function(e, eventData)
        {
			if(eventData != null)
			{
				_this.selectedItem = eventData.data;
				_this.setData(eventData.data);  
			}			   
		});
				
        this._context.readyHandler();
    }

    SummaryCubeNavBar.prototype._init = function(config)
	{
		if(config == null)
		{
			return SummaryCubeNavBar.prototype._init;
		}

        this.NavBar(config);
    }

    SummaryCubeNavBar.prototype.populate = function(selectedItem)
	{

		this.selectedItem = selectedItem;
	
		if(this.isReady)
		{
			this._handle.setData(eventData.selectedItem.data);   
		}
	};
	
	SummaryCubeNavBar.prototype.cancelFunction = function()
	{		
		this.showButtonContainer(false);
		this.setData(this.dataProvider);
	}

	SummaryCubeNavBar.prototype.cancel = function()
	{
		//Cancel the current pull transaction
	}

	SummaryCubeNavBar.prototype.setData = function(dataProvider)
	{	
		this.dataProvider = dataProvider;
		this.getInfo(dataProvider.Id);
	}

	SummaryCubeNavBar.prototype.getInfo = function(dataProviderId)
	{
		var _this = this;

        this.isContact = false;

        if(dataProviderId.substr(0,3) == '003')
        {
            this.isContact = true;
        }

		if(dataProviderId != null)
		{	
			ACE.Remoting.call(/*ACE.Namespace.CORE_APEX +*/ "SummaryCubeNavBarController.getNavBar", [ACE.Salesforce.userId, this.featurePath], function(result,event)
			{ 
				if(event.status)
				{	
					_this._init(result);	
				}
				else
				{
					console.log(result);
					console.log(event);
				}	
			});
		}
	}
	
	SummaryCubeNavBar.prototype.resizeHandler = function()
	{	
		//Should resize the cube face elements will be called when elements are resized on the form
	}
	
	$.extend(true, window,
	{
		"Custom":
		{
			"SummaryCubeNavBar": SummaryCubeNavBar
		}	
	});

    SummaryCubeNavBar.prototype.NavBar = function(config)
	{
		if(config != null)
		{
			var _this = this;
            
			$(this.nav).empty();

			var navUl = $('<ul id="cubeNavBarList" class="cubeNavBarList"></ul>').appendTo(this.nav);
			
            for(var x=0;x<config.length;x++)
            {
                var navItem = config[x];

				// Select the Contact/Account tab when Summary Cube is loaded
                if((this.isContact && navItem.isContact) || (!this.isContact && navItem.anchor === "accountDetails"))
                {
                    $('<li class="cubeNavBarItem" order="' + navItem.order + '"><a Id="NavItem' + x +'" href="#' + navItem.anchor + '"  class="selected">' + navItem.label + '</a></li>').appendTo(navUl);
                }           
                else if(navItem.isContact == false)
                {
                    $('<li class="cubeNavBarItem" order="' + navItem.order + '"><a Id="NavItem' + x +'" href="#' + navItem.anchor + '">' + navItem.label + '</a></li>').appendTo(navUl);
                }             
            }
            				
			var lastSelected = null;
			
			$('a[href*=#]:not([href=#])').click(function() 
	        {
				if(lastSelected == null || this != lastSelected)
				{
					lastSelected = this;
				
					$('.selected').removeClass('selected');
					$(this).addClass('selected');
					
			        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) 
			        {
			            var target = $(this.hash);
			            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			            
			            if (target.length) 
			            {       
			               	$('.powerCubeContent').stop().animate(
			               	{
			               		scrollTop: $(target)[0].offsetTop - (65 + $(_this.nav).height())
			               	}, 1000);
			            }		           
			        }
				}
				return false;
	        });           
			
			//this.resortTargetList();
		}
	}

	// Function to re-order the sections according the AFR for the Logged in User (even for OBO)
	SummaryCubeNavBar.prototype.resortTargetList = function()
	{		
		var childItems = $('#cubeNavBarList').children('li');

		$(childItems).sort(function(a,b){
			var an = a.getAttribute('order'),
				bn = b.getAttribute('order');
	
			if(an > bn) {
				return 1;
			}
			if(an < bn) {
				return -1;
			}
			return 0;
		}).detach().appendTo('#cubeNavBarList');
	}

    ACE.AML.ControllerManager.register("summary-cube:navBar", SummaryCubeNavBar);
    
//# sourceURL=SummaryCubeNavBar.js
})(jQuery);