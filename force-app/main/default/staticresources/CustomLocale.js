//=============================================================================
/**
 * Name: CustomLocale.js
 * Description: Custom locale class to override the en_US locale bundle strings for client customization. Used to expose the locale keys
 * 				available for the client to override
 *
 * Confidential & Proprietary, 2017 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
//============================================================================
(function()
{
	ACE.Locale.registerBundle("en_US", 
	{
		//Keys can be overriden here... custom keys can also be added here
		//Configuring dynamic labels with {Locale.<CUSTOMKEY>} will have the application adhere to it correctly
		
		//Default "My Accounts" label (can be overriden in the AFR also)
		MY_ACCOUNTS: "My Clients",

		//Tooltip for the "Team Accounts" list
		TEAM_ACCOUNTS: "Team Clients",

		//Text for the Inherit Account Address in QAC
		QUICK_FIND_CONTACTS_INHERIT_ACCOUNT_ADDRESS: "Inherit Client Address",				
		QUICK_ADD_CONTACTS_ACCOUNT_COVERAGE_WARNING: "Client Coverage Required. Please select a covered Client from the auto-complete.",
		QUICK_ADD_CONTACTS_ACCOUNT_TYPE_WARNING: "You cannot create Contacts for an Client of this Client Type.  Please select a Client from one of your available Client Types.",

		//Text for the Manage Interest "Client Account" column header
		CLIENT_ACCOUNT: "Client",

		//Text for the "Current Account" field in Employment Change		
		CURRENT_ACCOUNT: "Current Client",
		//Text for the "New Account" field in Employment Change		
		NEW_ACCOUNT: "New Client",
		//Text for the "Enter Account Name" prompt displayed in Employment Change
		ACCOUNT_SEARCH_LABEL: "Enter Client Name",
		//Text for the "Inherit Address from Account" label displayed in Employment Change
		EMPLOYMENT_CHANGE_INHERIT_ADDRESS_LABEL: "Inherit Address from Client",
		//The general label for account used in multiple locations
		//Used in Employment History cube face
		ACCOUNT: "Company",

		//General use label for Account coverage
		//Used in the Coverage Cube Face	
		//Used in Mass Coverage Management form (Coverage List pane grid "Account Coverage" is replaced by this value)
		//Used in Label in footer of the notification tab of the QAI dialog and in the reason column		
		ACCOUNT_COVERAGE: "Company",
		//General use label for the account hierarchy label
		ACCOUNT_HIERARCHY: "Client Hierarchy",

		//Text that will be used for the search picker in APM
		APM_SEARCH_ACCOUNTS: "Search Clients",
		
		//Text that will be used for the manage interests top search picker for searching accounts
		SEARCH_ACCOUNTS: "Search Clients",
		//Text that will be displayed for the search scope "In All Accounts" in Manage Interests
		IN_ALL_ACCOUNTS: "In All Clients",
		//Text that will be displayed for the search scope "In My Accounts" in Manage Interests
		IN_MY_ACCOUNTS: "In My Clients",
		
		//Text that will be used in "Mass Coverage management" Coverage picker
		ALL_ACCOUNT_COVERAGE: "All Company Coverage",
		
		MASS_COVERAGE_MANAGEMENT_ACCOUNT_COVERAGE: "Company",
		
		//Prompt text that will be displayed in the "Manage Relationships" screen for the text filter
		COVERAGE_MANAGEMENT_DIALOG_FILTER_PROMPT: "To filter grid enter Contact Name or Client",

		//Text that will be displayed in the Application User Preferences dialog for the header of the Account Coverage Filter Roles
		USER_PREFERENCES_ACCOUNT_COVERAGE_ROLE_FILTER_LABEL: "Client Coverage Filter Roles",
		//Text that will be displayed in the Application User Preferences dialog for the dropdown of the default search for Manage Interests
		ACCOUNTS_AND_CONTACTS: "Clients and Contacts",
		
		//Text that will be displayed in the warning when a user attempts to re-order sequence on an account list that has no accounts in it
		REOREDER_NO_ACCOUNT_WARNING: "No clients in selected list",

		//Prompt text to display in the Subscription List Dialog for the text filter
		SUBSCRIPTION_LIST_MANAGEMENT_FILTER_PROMPT: "Enter full or partial Contact or Client to filter on.",

		//Text displayed when the user chooses the Main Grid Default List in the Application Start up Preferences
		TEAM_ACCOUNTS_SELECTED: "Team Clients (last selected)",

		//Text displayed when the List Activity Permission for deletion is set to PROMPT
		//This will be the warning displayed to the user for moving activities
		LIST_ACTIVITY_DELETE_ACTION_MESSAGE: "Do you want to have them re-associated to the Clients involved?",
		LIST_ACTIVITY_DELETE_USER_SELECTION_MESSAGE: "Associate activities with client",

		// Account name / tier in the reorder SEQ
		ACCOUNT_NAME : "Client Name",
		ACCOUNT_TIER: "Client Tier",

		// Primary account tooltip
		INTERACTION_LOGGING_PRIMARY_ACCOUNT_TOOLTIP: "Primary Client: The primary client the interaction report is associated to. If there are multiple clients, you will see the interaction under all clients.",

		// Tooltip for the coverage modes in the main grid / popup
		COVERAGE_MODE_ICON_ACCOUNT_TOOLTIP: "In Client Coverage",
		// tooltip for the coverage tooltip in QFC
		QUICK_FIND_CONTACTS_IN_ACCOUNT_COVERAGE_TOOLTIP: "{0} is in your Client Coverage List.",
		//Tooltip for the Inherit Address from Client checkbox help button
		EMPLOYMENT_CHANGE_INHERIT_ADDRESS_HELP_TEXT: "When selected, the address of the selected Client will populate in the form",
		//QAC tooltips for what is searched in the QAC Account field
		QAC_ACCOUNTS_IN_YOUR_ACCOUNT_TYPES: 'Only returns Clients within your available Client Types',
		QAC_ACCOUNTS_IN_YOUR_ACCOUNT_COVERAGE: 'Only returns Clients within your Client Coverage',
		QAC_ACCOUNTS_IN_YOUR_ACCOUNT_TYPES_AND_COVERAGE: 'Only returns Clients within your Client Coverage and your available Client Types',
		//Warning Message , when no contact or account is selected - Advance Coverage Management form 
		EXCEL_EXPORT_NO_DATA_WARNING: "There are no clients or contacts selected for this activity. Select one or more clients or contacts before proceeding",
		//Used in the Export to Excel settings dialog
		ACCOUNT_LIST: "Client List",
		//Tooltip on Quick Find Contact
		QUICK_FIND_CONTACTS_HELP_TOOLTIP_TOKENIZED: "Search for a Contact not displayed:" +
											"\n- Enter at least 2 characters." +
											"\n- Maximum 3 search terms (tokens) allowed." +
											"\n- An asterisk * before your text will perform a wildcard search." +											
											'\n- Wrap criteria that contains a space in quotations (e.g. entering Fran "De Luc" to find Frank De Luca)' +
											'\n- Searches adapt to the number of tokens provided:\n1 term - either First Name, Last Name or Client Name must start with the term\n' + 
											'2 and 3 terms - a combination of First Name, Last Name and Client Name must start with the terms provided (provides more targeted search results)',
		ADV_COVERAGE_MANAGEMENT_NO_SELECTION : "There are no clients or contacts selected for this activity. Select one or more clients or contacts before proceeding",
		//Used in the content type picker in the top search and saved search. 
		//Displayed in toolbar to show search content type.
		ACCOUNTS: "Clients",
		//USed in APM checkbox label under Manage Restriction Settings
		APM_MANAGE_ACCOUNT_LEVEL_RESTRICTIONS: "Manage Client level restrictions",
        //Error message, when session expires
        SESSION_EXPIRED_MESSAGE: "Add a custom error message for session expiration",
		MANAGE_RELATIONSHIP_SEARCH_DESCRIPTION : "Use the search belowto find employees to assign to your contacts.",
		BANKING_REPORT_TASK_TAB_LABEL: "Tasks",
		CALL_REPORT_TASK_TAB_LABEL: "Tasks",
		MEETING_TYPE_VALIDATION_WARNING_MESSAGE:"Meeting Type must be selected",
		DURATION: "Duration (mins)",
		// Summary Cube Definitions
		INTERACTIONS:"Interactions",
		INTERESTS:"Interests",
		COVERAGE:"Coverage",
		ACCOUNT_COVERAGE_LABEL:"Account",
		CONATCT_COVERAGE_LABEL:"Contact",
		DATE_LABEL:"Date",
		EMPLOYEE_LABEL:"Employee",
		SUBJECT_LABEL:"Subject",
		TYPE_LABEL:"Type",
		ACCOUNT_LABEL: 'Account',
		CONTACT_LABEL:'Contact',
		REGION_LABEL:'Region',
		LINE_OF_BUSSINESS_LABEL:'LOB',
		ROLE_LABEL:'Role',
		SELECT_LABEL:'Select ',
		COVERAGE_ADDITIONAL_TO:"Additional To",
		COVERAGE_ADDITIONAL_BCC:"Additional BCC",
		COVERAGE_ADDITIONAL_CC:"Additional CC",
		COVERAGE_SUBJECT:"Email Subject",
		EMAIL_COVERAGE_TEAM:"Email Coverage Team",
		PICKLIST_SELECT_PROMPT:"-- Select One --"

	});
})();

//# sourceURL=CustomLocale.js