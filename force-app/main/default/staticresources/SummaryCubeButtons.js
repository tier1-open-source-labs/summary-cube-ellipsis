/**
 * Name: SummaryCubeButtons.js 
 *
 * Confidential & Proprietary, 2021 Tier1 Financial Solutions Inc.
 * Property of Tier1 Financial SolutionsInc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1 Financial Solutions.
 */

(function($)
{   
	function SummaryCubeButtons(context)
	{
		var _this = this;
		
		this._context = context;
		
		this.buttonContainer = $('<div class="buttonContainer"></div>').appendTo(context.target);
		this.confirmButton = $('<button class="confirmButton">Save</button>').appendTo(this.buttonContainer);
		this.cancelButton = $('<button class="cancelButton">Cancel</button>').appendTo(this.buttonContainer);
		$(this.buttonContainer).hide();

		this._context.global.eventDispatcher.on("showSummaryCubeButtons", function(e, eventData)
		{
			$(_this.buttonContainer).show(333);
		});
		
		this._context.global.eventDispatcher.on("hideSummaryCubeButtons", function(e, eventData)
		{
			$(_this.buttonContainer).hide(333);
		});
		
		$(this.confirmButton).click(function()
		{
			_this.thisAccount = _this._context.local.state.get('Account');
			_this.thisContact = _this._context.local.state.get('Contact');
			var accountUpdated = _this._context.local.state.get('AccountUpdated');
			var contactUpdated = _this._context.local.state.get('ContactUpdated');
			
			// Separate Null fields from the Account being updated
			// Required since Apex does not like JS Nulls for Date fields
			var acctNullFields = [];
			if (!accountUpdated)
			{
				_this.thisAccount = null;
			}
			else
			{
				for (var [key, value] of Object.entries(_this.thisAccount))
				{
					if (value === null || value === undefined)
					{
						acctNullFields.push(key);
						delete _this.thisAccount[key];
					}
				}
			}
			
			// Separate Null fields from the Contact being updated
			var contNullFields = [];
			if (!contactUpdated)
			{
				_this.thisContact = null;
			}
			else
			{
				for (var [key, value] of Object.entries(_this.thisContact))
				{
					if (value === null || value === undefined)
					{
						contNullFields.push(key);
						delete _this.thisContact[key];
					}
				}
			}
			
			ACE.Remoting.call("SummaryCubeDetailController.saveInfo", [_this.thisAccount, _this.thisContact, acctNullFields, contNullFields], function(result,event)
			{ 
				if(event.status && (result == null || result.length==0))
				{
					console.log('result: ' + result);

					var itemsUpdated = [];
					if(_this.thisAccount != null && _this.thisAccount[_this.t1cNS +'Inactive__c'] != true)
					{
						itemsUpdated.push(_this.thisAccount.Id);
					}

					if(_this.thisContact != null && _this.thisContact[_this.t1cNS +'Inactive__c']!= true)
					{
						itemsUpdated.push(_this.thisContact.Id);
					}

					ACE.CustomEventDispatcher.trigger('refreshMainGridContent', { items: itemsUpdated});
					_this._context.global.eventDispatcher.trigger('cancelSummaryCube');
				}
				else
				{
					console.log(result);
					console.log(event);
					ACE.FaultHandlerDialog.show({title:'Unexpected Error Occured on Save',message : event.message, error : 'Please review your data and try again'});
				}	
			});
		});
		
		$(this.cancelButton).click(function()
		{
			_this._context.global.eventDispatcher.trigger('cancelSummaryCube');
		});
		
		this._context.readyHandler();
	}

	ACE.AML.ControllerManager.register("summary-cube:buttonsFooter", SummaryCubeButtons);
	
//# sourceURL=SummaryCubeButtons.js
})(jQuery);