/**
 * Name: SummaryCubeCoverage.js 
 *
 * Confidential & Proprietary, 2021 Tier1 Financial Solutions Inc.
 * Property of Tier1 Financial SolutionsInc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1 Financial Solutions.
 */
//=============================================================================
(function($)	
{			
	var _super = ACE.inherit(SummaryCubeCoverage, ALM.AbstractCubeFace);

	/**
	 * Constructor
	 *
	 * @class      SummaryCubeCoverage (ellipsis context)
	 */
	
	function SummaryCubeCoverage(context)
	{
		_super.constructor.call(this, context);
		
		this._loadLayout(context);
		this._context = context;
		this.config = context.options;
		
		var _this = this;

		this._context.local.eventDispatcher.on('ec-CubePopulate', function(e, eventData)
        {
			if(eventData != null)
			{
				_this.selectedItem = eventData.data;
				_this.selectedType = eventData.type;
				_this.setData(eventData.data);  
			}			   
		});

		this.featurePath = this.config.path;

        this._context.readyHandler();
	}

	
	SummaryCubeCoverage.prototype._init = function(config)
	{
		if(config == null)
		{
			return SummaryCubeCoverage.prototype._init;
		}

		this._config = config;
		
		var _this = this;

		if(this.hasInit == null)
		{
			this.sections = {};
			this.timezoneOffset;

			this.userLocale = ACE.Salesforce.userLocale.replace('_','-');

			this.t1cNS = config.t1cNS;

			// Figuring out the difference between SFDC Time and Machine Time, show in SFDC Time
			this.timezoneOffset = config.timeZoneOffset;
			this.timeZone = moment.tz.guess();
			this.machineOffset = new Date().getTimezoneOffset() * -60000;
			this.timezoneOffset = this.timezoneOffset - this.machineOffset;						

			this.componentMap = {};
			this.buttonMap = {};
			this.buttonSectionMap = {};
			this.containerMap = {};
			this.emailInputMap = {};
			this.coverageURLPrefix = config.coverageURLPrefix;
			this.coverageURLSuffix = config.coverageURLSuffix;
			this.coverageLaunchTarget = config.coverageLaunchTarget;
			this.coverageLaunchOptions = config.coverageLaunchOptions;
			this.setupFormatterMap();
				
			if(this.coverageSection == null)
			{
				this.coverageSection = this.setupRelatedListSectionHeader(config.coverageSectionOrder,config.coverageSectionLabel,'coverage');
			}

			this.hasInit = true;
		}

		this.setupCoverages(config,this.coverageSection,this.buttonSectionMap['coverage']);
	}

	function getElementId(config)
	{
		return config.fieldName + ':' + config.objectName;
	}

	SummaryCubeCoverage.prototype.setupCoverages = function(result,section,buttonSection)
	{
		var _this = this;
		this._config.coverage = {};
		this._config.coverage.coverageTeamEmails = result.coverageTeamEmails;
		this._config.coverage.additionalTo = result.additionalTo;
		this._config.coverage.additionalCC = result.additionalCC;
		this._config.coverage.additionalBCC = result.additionalBCC;
		this._config.coverage.emailSubject = result.emailSubject;

		var emailSettingsForm = this.componentMap['emailSettingsForm'];
		var manageEmailButton = this.componentMap['manageEmailButton'];

		var acSection = this.componentMap['acSection'];
		var acSectionLabelLine;
		var acSectionLabel = this.componentMap['acSectionLabel'];
		var accountCoverageSection = this.componentMap['accountCoverageSection'];

		var ccSection = this.componentMap['ccSection'];
		var ccSectionLabel = this.componentMap['ccSectionLabel'];
		var contactCoverageSection = this.componentMap['contactCoverageSection'];

		this.accountFiltersDiv; 
		
		if(result.showEmailSettings == true && emailSettingsForm == null)
		{
			manageEmailButton = $('<button class="manageEmail"></button>').appendTo(buttonSection);
			this.componentMap['manageEmailButton'] = manageEmailButton;

			emailSettingsForm = $('<div id="emailSettingsForm" class="hiddenSubSection detailComponents"/>').appendTo(section);
			this.componentMap['emailSettingsForm'] = emailSettingsForm;
			$(emailSettingsForm).hide();

			$(manageEmailButton).click(function() 
			{
				$('#emailSettingsForm').toggle(333);
			});

			var addToConfig = {label: '{Locale.COVERAGE_ADDITIONAL_TO}',fieldName: 'additionalTo', objectName: 'coverage', record: this._config.coverage};
			var addCCConfig = {label: '{Locale.COVERAGE_ADDITIONAL_CC}',fieldName: 'additionalCC', objectName: 'coverage', record: this._config.coverage};
			var addBCCConfig = {label: '{Locale.COVERAGE_ADDITIONAL_BCC}',fieldName: 'additionalBCC', objectName: 'coverage', record: this._config.coverage};
			var emailSubConfig = {label: '{Locale.COVERAGE_SUBJECT}',fieldName: 'emailSubject', objectName: 'coverage', record: this._config.coverage};
			
			var addTo = createEmailInput(this,emailSettingsForm,addToConfig);
			var addCC = createEmailInput(this,emailSettingsForm,addCCConfig);
			var addBCC = createEmailInput(this,emailSettingsForm,addBCCConfig);
			var emailSub = createEmailInput(this,emailSettingsForm,emailSubConfig);

			this.emailButtonContainer = $('<div style="height:35px;" class="buttonContainer componentContainer subSectionItem p-all-m"></div>').appendTo(emailSettingsForm);
			this.emailConfirmButton = $('<button class="confirmButton">Save</button>').appendTo(this.emailButtonContainer);
			this.emailCancelButton = $('<button class="cancelButton">Cancel</button>').appendTo(this.emailButtonContainer);

			$(this.emailConfirmButton).click(function()
			{
				var thisToValue = $(addTo).val();
				var thisCCValue = $(addCC).val();
				var thisBCCValue = $(addBCC).val();
				var thisSubjectValue = $(emailSub).val();

				ACE.Remoting.call("SummaryCubeCoverageController.saveEmailInfo", [ACE.Salesforce.userId,thisToValue,thisCCValue,thisBCCValue,thisSubjectValue], function(result,event)
				{ 
					if(event.status)
					{
						$('#emailSettingsForm').hide(333);
						$(_this.emailButtonContainer).hide();
					}
					else
					{
						console.log(result);
						console.log(event);
					}	
				});
			});

			$(this.emailCancelButton).click(function()
			{
				$('#emailSettingsForm').hide(333);
				$(_this.emailButtonContainer).hide();
			});

			$(this.emailButtonContainer).hide();
		}

		$.each(result.buttons,function(key, value)
		{
			createButton(_this,buttonSection,value);
		});

		this.accCovFilters = {}; 
		this.conCovFilters = {}; 
		this.bothCovFilters = {};
		this.accCovFiltersExistingData = new Set(); 
		this.conCovFiltersExistingData = new Set(); 
		this.bothCovFiltersExistingData = new Set(); 


		if (this.filterContainerCreated == null || !this.filterContainerCreated)
		{
			this.accountFiltersDiv = $('<div style="display:flex;width:100%;flex-wrap:wrap;"></div>').appendTo(section); 
			this.filterContainerCreated = true; 
		}
		else 
		{
			this.accountFiltersDiv.empty();
		}
		

		if(acSection == null)
		{
			acSection = $('<div></div>').appendTo(section);
			this.componentMap['acSection'] = acSection;

			acSectionLabelLine = $('<div style="display:flex;width:100%"></div>').appendTo(acSection);

			acSectionLabel = $('<div style="width:100%" class="primaryLabel">' + result.acctCoverageGridLabel + '</div>').appendTo(acSectionLabelLine);
			this.componentMap['acSectionLabel'] = acSectionLabel;

			accountCoverageSection = $('<div class="subSection cubeDataGridContainer" style="cursor:pointer;height:' + this.acGridHeight + 'px"></div>').appendTo(acSection);
			this.componentMap['accountCoverageSection'] = accountCoverageSection;
		}

		if(this.acGrid == null)
		{
			this.acGrid = createGrid(this,accountCoverageSection,result.acColumns);
			
			this.acGrid.grid.onDblClick.subscribe(function(e, args) 
			{				   
				var data = _this.acGrid.grid.getData();
				if(data !== undefined && data.length === undefined)
				{
					data = data.getItems();
				}
				var column = _this.acGrid.grid.getColumns()[args.cell];
				
				if(column.linkField != undefined && column.linkField.length > 0 && column.cssClass == 'link' && data !== undefined && data.length !== undefined)
				{
					var rowData = data[args.row];  
					var thisLinkField = column.linkField;
					var thisData = rowData;

					if(thisLinkField == 'Id')
					{
						thisData = rowData['recordHREF'];
						if(thisData != undefined)
						{
							window.open(thisData, _this.coverageLaunchTarget, _this.coverageLaunchOptions);
						}
					}
					else
					{
						var thisLinkFields = thisLinkField.split('.');
						
						for(var x=0;x<thisLinkFields.length;x++)
						{
							thisData = thisData[thisLinkFields[x]];
						}

						window.open(thisData, _this.coverageLaunchTarget, _this.coverageLaunchOptions);
					}					
				}	
			});
		}
		
		this.acGrid.setItems(cleanRows(result.acColumns, result.accountCoverages,_this));	

		if(ccSection == null)
		{
			ccSection = $('<div></div>').appendTo(section);
			_this.componentMap['ccSection'] = ccSection;

			ccSectionLabel = $('<div class="primaryLabel">' + result.contCoverageGridLabel + '</div>').appendTo(ccSection);
			_this.componentMap['ccSectionLabel'] = ccSectionLabel;

			contactCoverageSection = $('<div class="subSection cubeDataGridContainer" style="cursor:pointer;height:' + _this.ccGridHeight + 'px"></div>').appendTo(ccSection);
			_this.componentMap['accountCoverageSection'] = contactCoverageSection;
		}

		if(this.ccGrid == null)
		{
			this.ccGrid = createGrid(this,contactCoverageSection,result.ccColumns);
			
			this.ccGrid.grid.onDblClick.subscribe(function(e, args) 
			{				   
				var data = _this.ccGrid.grid.getData();
				if(data !== undefined && data.length === undefined)
				{
					data = data.getItems();
				}
				var column = _this.ccGrid.grid.getColumns()[args.cell];
				
				if(column.linkField != undefined && column.linkField.length > 0 && column.cssClass == 'link' && data !== undefined && data.length !== undefined)
				{
					var rowData = data[args.row];  
					var thisLinkField = column.linkField;
					var thisData = rowData;

					if(thisLinkField == 'Id')
					{
						thisData = rowData['recordHREF'];
						if(thisData != undefined)
						{
							window.open(thisData, _this.coverageLaunchTarget, _this.coverageLaunchOptions);
						}
					}
					else
					{
						var thisLinkFields = thisLinkField.split('.');
						
						for(var x=0;x<thisLinkFields.length;x++)
						{
							thisData = thisData[thisLinkFields[x]];
						}

						window.open(thisData, _this.coverageLaunchTarget, _this.coverageLaunchOptions);
					}					
				}	
			});
		}

		var conFilterMethod = function (item)
		{
			for (var dataField in _this.conCovFilterValues) {
				if (Object.prototype.hasOwnProperty.call(_this.conCovFilterValues, dataField))
				{
					var dataFieldSplit = dataField.split("."); 
					var curItem = item; 

					try {
						for (var i = 0; i < dataFieldSplit.length; i++)
						{
							if(curItem != null)
							{
								curItem = curItem[dataFieldSplit[i]];
							}							
						}

						if(curItem == null && _this._config.ccAdditionalInfoPrefix != null)
						{
							curItem = item; 
							var dataFieldToSplit = _this._config.ccAdditionalInfoPrefix + dataField;
							dataFieldSplit = dataFieldToSplit.split("."); 

							for (var i = 0; i < dataFieldSplit.length; i++)
							{
								if(curItem != null)
								{
									curItem = curItem[dataFieldSplit[i]];
								}
							}
						}											

						if (_this.conCovFilterValues[dataField] != null && _this.conCovFilterValues[dataField] != '' && 
							_this.conCovFilterValues[dataField] != curItem)
						{
							return false; 
						}
					}
					catch (err)
					{
						// if dataFieldSplit has undefined items. 
						console.log(err);
						return false; 
					}
				}
			}
			
			return true;
		}

		this.ccGrid.setItems(cleanRows(result.ccColumns, result.contactCoverages,_this));	
		
		if(result.contactCoverages.length > 0)
		{
			$(ccSection).show();
		}
		else
		{
			$(ccSection).hide();
		}
		
		// object with selected filter values. 
		this.accCovFilterValues = {};
		this.conCovFilterValues = {};

		var accCovConfigs = result.filterFields; 

		var makeFilter = function(accCovFilters, prop, gridType) 
		{
			if (gridType === 'both' || _this.selectedType !== 'Account' || (gridType === _this.selectedType.toLowerCase()))
			{
				var filterContainer = $('<div style="padding-right:10px;padding-bottom:8px;"></div>').appendTo(_this.accountFiltersDiv);
				var filt = new ACEMenuButton(filterContainer, { dataProvider : accCovFilters[prop]});

				// for issue where the the picklist doesn't close when you click on a draggable area. 
				// this will run after the specific click handler for the child divs run so there should be no worry about this preventing
				// a other click handlers. 
				$(document).on("click",function() {
					filt.close();
				});

				filt.on('click', function(e) 
				{
					if (gridType == 'account')
					{
						_this.accCovFilterValues[prop] = e.item.data;
					}
					else if (gridType == 'contact')
					{
						_this.conCovFilterValues[prop] = e.item.data;
					}
					else 
					{
						_this.accCovFilterValues[prop] = e.item.data;
						_this.conCovFilterValues[prop] = e.item.data;
					}

					_this.acGrid.dataProvider.refresh();
					_this.acGrid.grid.resizeCanvas();
					_this.ccGrid.dataProvider.refresh();
					_this.ccGrid.grid.resizeCanvas();

					// get all currently visible items and then update coverageTeamEmails
					_this._config.coverage.coverageTeamEmails = ""; 
					var emails = new Set();

					for (var i = 0; i < _this.acGrid.dataProvider.getLength(); i++)
					{
						if (_this.acGrid.dataProvider.getItem(i)[_this.t1cNS + 'Employee__r'] != null && _this.acGrid.dataProvider.getItem(i)[_this.t1cNS + 'Employee__r'][_this.t1cNS + 'Email__c'] != null)
						{
							emails.add(acGrid.dataProvider.getItem(i)[t1cNS + 'Employee__r'][_this.t1cNS + 'Email__c']);
						}
					}
					
					for(var email of emails.values())
					{
						_this._config.coverage.coverageTeamEmails += email + ';';
					}
				});
			}
		}

		if (accCovConfigs != null && accCovConfigs.length > 0)
		{
			// go through the filter configs and construct the data provider picklists. 
			for (var i = 0; i < accCovConfigs.length; i++)
			{	
				if (!accCovConfigs[i].enabled)
				{
					continue; 
				}
				var applyTo = accCovConfigs[i].applyTo;

				// loop through items and make picklist data provider. 
				var items;
				if (applyTo == 'account')
				{
					items = _this.acGrid.dataProvider.getItems(); 
				} 
				else if (applyTo == 'contact')
				{
					items = _this.ccGrid.dataProvider.getItems(); 
				} 
				else 
				{
					items = [];
					var acItems = _this.acGrid.dataProvider.getItems();
					var ccItems = _this.ccGrid.dataProvider.getItems();
					for (var j = 0; j < acItems.length; j++)
					{
						items.push(acItems[j]);
					}
					for (var j = 0; j < ccItems.length; j++)
					{
						items.push(ccItems[j]);
					}
				}

				var gridType = ((applyTo == 'account' || applyTo == 'contact') ? applyTo : 'both' );

				// set up picklist item array and adding default labels 
				if (applyTo == 'account')
				{
					if (_this.accCovFilters[accCovConfigs[i].fieldName] == null)
					{
						_this.accCovFilters[accCovConfigs[i].fieldName] = [];
					}
				}
				else if (applyTo == 'contact')
				{
					if (_this.conCovFilters[accCovConfigs[i].fieldName] == null)
					{
						_this.conCovFilters[accCovConfigs[i].fieldName] = [];
					}
				}
				else 
				{	
					if (_this.bothCovFilters[accCovConfigs[i].fieldName] == null)
					{
						_this.bothCovFilters[accCovConfigs[i].fieldName] = [];
					}
				}

				if (applyTo == 'account' && _this.accCovFilters[accCovConfigs[i].fieldName] != null)
				{
					_this.accCovFilters[accCovConfigs[i].fieldName].push({
						data : '', 
						label : (accCovConfigs[i].label == null ? '-- Select One --' : accCovConfigs[i].label), 
						defaultSelected : true
					});
				}
				else if (applyTo == 'contact' && _this.conCovFilters[accCovConfigs[i].fieldName] != null)
				{
					_this.conCovFilters[accCovConfigs[i].fieldName].push({
						data : '', 
						label : (accCovConfigs[i].label == null ? '-- Select One --' : accCovConfigs[i].label), 
						defaultSelected : true
					});
				}
				else if (_this.bothCovFilters[accCovConfigs[i].fieldName] != null)
				{
					_this.bothCovFilters[accCovConfigs[i].fieldName].push({
						data : '', 
						label : (accCovConfigs[i].label == null ? '-- Select One --' : accCovConfigs[i].label), 
						defaultSelected : true
					});
				}
				
				if(applyTo == 'account' || applyTo == 'contact')
				{
					for (var j = 0; j < items.length; j++)
					{
						try 
						{	
							// disect fieldname 
							var fieldNameSplit = accCovConfigs[i].fieldName.split(".");
							var labelSplit = accCovConfigs[i].picklistLabelField.split(".");
							var picklistValueToAdd = items[j];						
	
							for (var fieldNamePartIdx = 0; fieldNamePartIdx < fieldNameSplit.length; fieldNamePartIdx++)
							{
								picklistValueToAdd = picklistValueToAdd[fieldNameSplit[fieldNamePartIdx]];
							}
							
							var picklistLabelToAdd = items[j];
							for (var labelPartIdx = 0; labelPartIdx < labelSplit.length; labelPartIdx++)
							{
								picklistLabelToAdd = picklistLabelToAdd[labelSplit[labelPartIdx]];
							}
	
							if (applyTo == 'account') // account or all 
							{
								if (picklistValueToAdd != null && picklistLabelToAdd != null && !_this.accCovFiltersExistingData.has(picklistValueToAdd))
								{
									_this.accCovFiltersExistingData.add(picklistValueToAdd);
									_this.accCovFilters[accCovConfigs[i].fieldName].push({'data': picklistValueToAdd, 'label': picklistLabelToAdd});
								}
							}
							else if (applyTo == 'contact') 
							{
								if (picklistValueToAdd != null && picklistLabelToAdd != null && !_this.conCovFiltersExistingData.has(picklistValueToAdd))
								{
									_this.conCovFiltersExistingData.add(picklistValueToAdd);
									_this.conCovFilters[accCovConfigs[i].fieldName].push({'data': picklistValueToAdd, 'label': picklistLabelToAdd});
								}
							}
							else 
							{
								if (picklistValueToAdd != null && picklistLabelToAdd != null && !_this.bothCovFiltersExistingData.has(picklistValueToAdd))
								{
									_this.bothCovFiltersExistingData.add(picklistValueToAdd);
									_this.bothCovFilters[accCovConfigs[i].fieldName].push({'data': picklistValueToAdd, 'label': picklistLabelToAdd});
								}
							}
						} catch (err) {
							console.log(err);
						}
					}
				}
				else
				{
					var acItems = _this.acGrid.dataProvider.getItems();
					var ccItems = _this.ccGrid.dataProvider.getItems();
					
					for (var j = 0; j < acItems.length; j++)
					{
						var picklistValueToAdd = acItems[j];
						var fieldNameSplit = accCovConfigs[i].fieldName.split(".");
						var labelSplit = accCovConfigs[i].picklistLabelField.split(".");

						for (var fieldNamePartIdx = 0; fieldNamePartIdx < fieldNameSplit.length; fieldNamePartIdx++)
						{
							if(picklistValueToAdd != null)
							{
								picklistValueToAdd = picklistValueToAdd[fieldNameSplit[fieldNamePartIdx]];
							}	
						}

						if(picklistValueToAdd == null)
						{
							picklistValueToAdd = acItems[j];
							var additionalInfoDataField = (_this._config.acAdditionalInfoPrefix + accCovConfigs[i].fieldName).split(".");

							for (var fieldNamePartIdx = 0; fieldNamePartIdx < additionalInfoDataField.length; fieldNamePartIdx++)
							{
								if(picklistValueToAdd != null)
								{
									picklistValueToAdd = picklistValueToAdd[additionalInfoDataField[fieldNamePartIdx]];
								}
							}
						}
						
						var picklistLabelToAdd = acItems[j];

						for (var fieldNamePartIdx = 0; fieldNamePartIdx < fieldNameSplit.length; fieldNamePartIdx++)
						{
							if(picklistLabelToAdd != null)
							{
								picklistLabelToAdd = picklistLabelToAdd[fieldNameSplit[fieldNamePartIdx]];
							}
						}

						if(picklistLabelToAdd == null)
						{
							picklistLabelToAdd = acItems[j];
							var additionalInfoDataField = (_this._config.acAdditionalInfoPrefix + accCovConfigs[i].picklistLabelField).split(".");

							for (var fieldNamePartIdx = 0; fieldNamePartIdx < additionalInfoDataField.length; fieldNamePartIdx++)
							{
								if(picklistLabelToAdd != null)
								{
									picklistLabelToAdd = picklistLabelToAdd[additionalInfoDataField[fieldNamePartIdx]];
								}
							}
						}

						if (picklistValueToAdd != null && picklistLabelToAdd != null && !_this.bothCovFiltersExistingData.has(picklistValueToAdd))
						{
							_this.bothCovFiltersExistingData.add(picklistValueToAdd);
							_this.bothCovFilters[accCovConfigs[i].fieldName].push({'data': picklistValueToAdd, 'label': picklistLabelToAdd});
						}					
					}

					for (var j = 0; j < ccItems.length; j++)
					{
						var picklistValueToAdd = ccItems[j];
						var fieldNameSplit = accCovConfigs[i].fieldName.split(".");
						var labelSplit = accCovConfigs[i].picklistLabelField.split(".");

						for (var fieldNamePartIdx = 0; fieldNamePartIdx < fieldNameSplit.length; fieldNamePartIdx++)
						{
							if(picklistValueToAdd != null)
							{
								picklistValueToAdd = picklistValueToAdd[fieldNameSplit[fieldNamePartIdx]];
							}
						}

						if(picklistValueToAdd == null)
						{
							picklistValueToAdd = ccItems[j];
							var additionalInfoDataField = (_this._config.ccAdditionalInfoPrefix + accCovConfigs[i].fieldName).split(".");

							for (var fieldNamePartIdx = 0; fieldNamePartIdx < additionalInfoDataField.length; fieldNamePartIdx++)
							{
								if(picklistValueToAdd != null)
								{
									picklistValueToAdd = picklistValueToAdd[additionalInfoDataField[fieldNamePartIdx]];
								}
							}
						}
						
						var picklistLabelToAdd = ccItems[j];

						for (var fieldNamePartIdx = 0; fieldNamePartIdx < fieldNameSplit.length; fieldNamePartIdx++)
						{
							if(picklistLabelToAdd != null)
							{
								picklistLabelToAdd = picklistLabelToAdd[fieldNameSplit[fieldNamePartIdx]];
							}
						}

						if(picklistLabelToAdd == null)
						{
							picklistLabelToAdd = ccItems[j];
							var additionalInfoDataField = (_this._config.ccAdditionalInfoPrefix + accCovConfigs[i].picklistLabelField).split(".");

							for (var fieldNamePartIdx = 0; fieldNamePartIdx < additionalInfoDataField.length; fieldNamePartIdx++)
							{
								if(picklistLabelToAdd != null)
								{
									picklistLabelToAdd = picklistLabelToAdd[additionalInfoDataField[fieldNamePartIdx]];
								}
							}
						}

						if (picklistValueToAdd != null && picklistLabelToAdd != null && !_this.bothCovFiltersExistingData.has(picklistValueToAdd))
						{
							_this.bothCovFiltersExistingData.add(picklistValueToAdd);
							_this.bothCovFilters[accCovConfigs[i].fieldName].push({'data': picklistValueToAdd, 'label': picklistLabelToAdd});
						}					
					}
				}
			}
			

			// loop through the accCovFilters and then make picklists 
			for (var prop in _this.accCovFilters) {
				if (Object.prototype.hasOwnProperty.call(_this.accCovFilters, prop)) 
				{
					makeFilter(_this.accCovFilters, prop, 'account');
				}
			}

			// loop through the conCovFilters and then make picklists 
			for (var prop in _this.conCovFilters) {
				if (Object.prototype.hasOwnProperty.call(_this.conCovFilters, prop)) 
				{
					makeFilter(_this.conCovFilters, prop, 'contact');
				}
			}

			// loop through the conCovFilters and then make picklists 
			for (var prop in _this.bothCovFilters) {
				if (Object.prototype.hasOwnProperty.call(_this.bothCovFilters, prop)) 
				{
					makeFilter(_this.bothCovFilters, prop, 'both');
				}
			}
		}

		var accFilterMethod = function (item)
		{
			var hasFilter = false;

			for (var dataField in _this.accCovFilterValues) 
			{
				if(_this.accCovFilterValues[dataField].length > 0)
				{
					// no filter, return all
					hasFilter = true;
				}
			}

			if(!hasFilter)
			{
				return true;
			}

			for (var dataField in _this.accCovFilterValues) 
			{
				if (Object.prototype.hasOwnProperty.call(_this.accCovFilterValues, dataField))
				{
					var dataFieldSplit = dataField.split("."); 
					var curItem = item; 

					try 
					{
						for (var i = 0; i < dataFieldSplit.length; i++)
						{
							if(curItem != null)
							{
								curItem = curItem[dataFieldSplit[i]];
							}							
						}

						if(curItem == null && _this._config.acAdditionalInfoPrefix != null)
						{
							curItem = item; 
							var dataFieldToSplit = _this._config.acAdditionalInfoPrefix + dataField;
							dataFieldSplit = dataFieldToSplit.split("."); 

							for (var i = 0; i < dataFieldSplit.length; i++)
							{
								if(curItem != null)
								{
									curItem = curItem[dataFieldSplit[i]];
								}
							}
						}

						if (_this.accCovFilterValues[dataField] != null && _this.accCovFilterValues[dataField] != '' && 
							_this.accCovFilterValues[dataField] != curItem)
						{
							return false; 
						}
					}
					catch (err)
					{
						// if dataFieldSplit has undefined items. 
						if(_this.accCovFilterValues[_this._config.acAdditionalInfoPrefix + dataField] != null && _this.accCovFilterValues[_this._config.acAdditionalInfoPrefix + dataField] != '')
						{
							return false; 
						}
					}					
				}
			}
			
			return true;
		}

		_this.acGrid.dataProvider.setFilter(accFilterMethod);
		_this.ccGrid.dataProvider.setFilter(conFilterMethod);

		if(result.accountCoverages.length > 0)
		{
			$(acSection).show();
		}
		else
		{
			$(acSection).hide();
		}

		//Resize after showing everything
		_this.acGrid.grid.resizeCanvas();
		_this.ccGrid.grid.resizeCanvas();
	}

	SummaryCubeCoverage.prototype.setupRelatedListSectionHeader = function(order,label,type)
	{
		if(order == null)
		{
			return null;
		}

		var thisRelatedSectionHeader = this.componentMap[type];
		if(thisRelatedSectionHeader == null)
		{
			thisRelatedSectionHeader = $('<div Id="' + type + 'Details" order="' + order + '" class="section sectionDraggable  p-all-m"></div>').appendTo(this.cubeContent);
			var sectHeader = $('<div class="sectionHeader ' + type + 'Header"></div>').appendTo(thisRelatedSectionHeader);
			var titleContainer = $('<div class="headerItem headerTitleContainer"></div>').appendTo(sectHeader);
			$('<div class="primaryLabel">' + label + '</div>').appendTo(titleContainer);
			var sectionActionButtons = $('<div class="headerItem actionButtons"></div>').appendTo(sectHeader);

			this.componentMap[type] = thisRelatedSectionHeader;
			this.buttonSectionMap[type] = sectionActionButtons;
		}

		return thisRelatedSectionHeader;
	}
	
	SummaryCubeCoverage.prototype.setupSectionHeader = function(config, objectName)
	{
		if(config == null)
		{
			return null;
		}

		var order = config[objectName + 'SectionOrder']

		if(this.sections[objectName + 'Section'] == null)
		{
			this.sections[objectName + 'Section'] = {};

			var thisSection = $('<div Id="' + objectName + 'Details" order="' + order + '" class="section  p-all-m"></div>').appendTo(this.cubeContente);
			this.sections[objectName + 'Section']['mainDiv'] = thisSection;
			
			var thisSectionHeader = $('<div class="sectionHeader ' + objectName + 'Header"></div>').appendTo(thisSection);
			this.sections[objectName + 'Section'][objectName + 'Header'] = thisSectionHeader;

			var thisSectionTitle = $('<div class="headerItem headerTitleContainer"></div>').appendTo(thisSectionHeader);
			this.sections[objectName + 'Section'][objectName + 'HeaderTitleContainer'] = thisSectionTitle;

			this.buttonSectionMap[objectName] = $('<div class="headerItem actionButtons"></div>').appendTo(thisSectionHeader);
		}

		return this.sections[objectName + 'Section'];
	} 

	SummaryCubeCoverage.prototype.setupFormatterMap = function()
	{
		var _this = this;
		this.formatterMap = {};

		function defaultFormatter(row, cell, value, columnDef, dataContext)
		{
			if(row == null)
			{
				return defaultFormatter;
			}
	
			if (value == null) return ''; 
	
				return $p.escapeHTML(value.toString()); 
		}
		
		this.formatterMap['defaultFormatter'] = defaultFormatter;
		
		function gridLinkFormatter(row, cell, value, columnDef, dataContext)
		{
			if(row == null)
			{
				return gridLinkFormatter;
			}
	
			if(value == null) return '';
			
			return value.toString();
		}
		
		this.formatterMap['gridLinkFormatter'] = gridLinkFormatter;
		
		function gridFullLinkFormatter(row, cell, value, columnDef, dataContext)
		{
			if(row == null)
			{
				return gridFullLinkFormatter;
			}
	
			if(value == null) return '';
			
			return value.toString();
		}
		
		this.formatterMap['gridFullLinkFormatter'] = gridFullLinkFormatter;
	
		function dateFormatter(row, cell, value, columnDef, dataContext)
		{	
			if(row == null)
			{
				return dateFormatter;
			}		
			if (value == null || value == '') return ''; 
			var datesTimeZoneOffset = moment(value).tz(_this.timeZone)._offset;
			var valueWithOffset = value + (datesTimeZoneOffset * -60000);
			return (new Date(valueWithOffset)).toLocaleDateString(_this.userLocale); 
		}

		this.formatterMap['dateFormatter'] = dateFormatter;

		function dateTimeFormatter(row, cell, value, columnDef, dataContext)
		{	
			if(row == null)
			{
				return dateTimeFormatter;
			}		
			if (value == null || value == '') return ''; 

			var valueWithOffset = value + _this.timezoneOffset;
			var dateTimeString = new Date(valueWithOffset).toLocaleString(_this.userLocale);
			return dateTimeString;
		}

		this.formatterMap['dateTimeFormatter'] = dateTimeFormatter;
	}
	// Cube Required Functions

	SummaryCubeCoverage.prototype.populate = function(selectedItem)
	{
		this.selectedItem = selectedItem;
	
		if(this.isReady)
		{
			this._handle.setData(eventData.selectedItem.data);   
		}
	};

	SummaryCubeCoverage.prototype._loadLayout = function(target)
	{		
		this._parentContainer = target.target;
		this.createChildren(target.target);
	}
		
	SummaryCubeCoverage.prototype.cancelFunction = function()
	{		
		this.showButtonContainer(false);
		this.setData(this.dataProvider);
	}

	SummaryCubeCoverage.prototype.cancel = function()
	{
		//Cancel the current pull transaction
	}

	SummaryCubeCoverage.prototype.setData = function(dataProvider)
	{	
		this.dataProvider = dataProvider;
		this.getInfo(dataProvider.Id);
	}

	SummaryCubeCoverage.prototype.getInfo = function(dataProviderId)
	{
		var _this = this;

		if(dataProviderId != null)
		{	
			ACE.Remoting.call(/*ACE.Namespace.CORE_APEX +*/ "SummaryCubeCoverageController.getCoverages", [dataProviderId, ACE.Salesforce.userId, this.config.recordLimit, this.featurePath], function(result,event)
			{ 
				if(event.status)
				{	
					$(_this._context.target).show();
					_this._init(result);	
				}
				else
				{
					console.log(result);
					console.log(event);
				}	
			});
		}
	}
	
	SummaryCubeCoverage.prototype.resizeHandler = function()
	{	
		//Should resize the cube face elements will be called when elements are resized on the form
		if(this.acGrid != null)
		{
			this.acGrid.resizeCanvas();				
		}

		if(this.ccGrid != null)
		{
			this.ccGrid.resizeCanvas();				
		}
	}
	
	SummaryCubeCoverage.prototype.createChildren = function(parent)
	{		
		this.cubeContent = parent;
		this.lastPullTimeout = null;
	}
	
	$.extend(true, window,
	{
		"Custom":
		{
			"SummaryCubeCoverage": SummaryCubeCoverage
		}	
	});

	function cleanRows(columnDefs,rows,_this)
	{
		var dateFieldMap = {};
		var linkFieldMap = {};

		for(var x=0;x<columnDefs.length;x++)
		{
			var columnDef = columnDefs[x];
			if(columnDef != null)
			{
				if(columnDef.fieldType == 'Date' ||columnDef.fieldType == 'DateTime')
				{
					dateFieldMap[columnDef.dataField] = columnDef.fieldType;
				}
				
				if(columnDef.formatter == 'gridLinkFormatter')
				{
					linkFieldMap[columnDef.dataField] = 'gridLinkFormatter:' + columnDef.linkField;
				}
				else if(columnDef.formatter == 'gridFullLinkFormatter')
				{
					linkFieldMap[columnDef.dataField] = 'gridFullLinkFormatter:' + columnDef.linkField;
				}
			}
		}

		for(var y=0; y < rows.length; y++)
		{
			var row = rows[y];
			if(row.id == null && row.Id != null)
			{
				row.id = row.Id;
			}

			for (var property in linkFieldMap) 
			{
				if (!linkFieldMap.hasOwnProperty(property)) continue;	
				
				var formatterName = linkFieldMap[property].split(':')[0];
				var thisLinkField = linkFieldMap[property].split(':')[1];
				var thisLinkFields = thisLinkField.split('.');
				var thisData = row;
				
				for(var x=0;x<thisLinkFields.length;x++)
				{
					thisData = thisData[thisLinkFields[x]];
				}
		
				var linkFld = 'recordHREF';
				if(thisLinkField != 'Id')
				{
					linkFld = thisLinkField;
				}

				if(formatterName == 'gridLinkFormatter')
				{
					row[linkFld] = _this.coverageURLPrefix + thisData + _this.coverageURLSuffix;
				}
				else if(formatterName == 'gridFullLinkFormatter')
				{
					row[linkFld] = thisData;
				}
			}
						
			for (var property in dateFieldMap) 
			{			
			    if (!dateFieldMap.hasOwnProperty(property)) continue;
			    
			    var thisData = row[property];
			    // Generically go through the SForce object to get at the property if needed
			    if(thisData == null)
			    {	
				    thisLinkFields = property.split('.');
					thisData = row;
					
					for(var x=0;x<thisLinkFields.length;x++)
					{
						thisData = thisData[thisLinkFields[x]];
					}
			    }
			    
			    var tempDate = new Date(thisData).getTime();
			
				if(isNaN(tempDate) || tempDate == 0)
				{
					row[property] = null;
				}
				else
				{
					row[property] = tempDate;
				}			   
			}
		}

		return rows;
	}

	function createGrid(_this,section,columnDefs)
	{
		var coverageGrid = new ACEDataGrid($(section) ,{  
			forceFitColumns: true, 
			explicitInitialization: true,
			editable: true,																					
			headerRowHeight: 40
		});

		var columns = [];
		
		for(var x=0;x<columnDefs.length;x++)
		{
			var columnDef = columnDefs[x];
			if(columnDef != null)
			{
				var formatterName = columnDef.formatter;
				var column =  {id: columnDef.dataField, resizable: columnDef.resizable, field: columnDef.dataField, width: columnDef.width, name: columnDef.label, sortable: true, fieldType: columnDef.fieldType, linkField : columnDef.linkField, featureName : columnDef.featureName};
				if(formatterName != null && formatterName.length > 0)
				{
					column['formatter'] = _this.formatterMap[formatterName];
					if(formatterName == 'gridFullLinkFormatter' || formatterName == 'gridLinkFormatter')
					{
						column['cssClass'] = 'link';
					}	
				}
				else
				{
					column['formatter'] = _this.formatterMap['defaultFormatter'];
				}
				columns.push(column);
			}
		}		

		coverageGrid.setColumns(columns);
		coverageGrid.grid.init();
		
		return coverageGrid;
	}

	function createButton(_this,component,button)
	{
		var foundValue = true;
		var type = button.type;
		var objName = button.objectName;
		var fldName  = button.fieldName;
		var elementId = objName + ':' + fldName;
		var mergeVal;
		var urlTar = button.urlTarget;
		var urlPre = button.urlPrefix;
		var urlSuf = button.urlSuffix;
		var label  = button.label;
		var fullURL;

		if(button.buttonId != null)
		{
			elementId = button.buttonId;
		}

		if(label == null)
		{
			label = '';
		}

		if(urlTar == null)
		{
			urlTar = '';
		}

		if(urlPre == null)
		{
			urlPre = '';
		}

		if(urlSuf == null)
		{
			urlSuf = '';
		}

		if(objName != null && fldName != null)
		{
			var objVal = _this._config[objName];
			if(objVal != null)
			{
				mergeVal = objVal[fldName];
				fullURL = urlPre + mergeVal + urlSuf;
			}
			else
			{
				foundValue = false;
			}			
		}
		else if(objName != null && fldName == null)
		{
			mergeVal = _this._config[objName];
			fullURL = urlPre + mergeVal + urlSuf;
		}

		var thisButton = _this.buttonMap[elementId];

		if(thisButton == null)
		{
			thisButton = $('<button style="margin:0 5px" title="' + label + '"></button>').appendTo(component);
			_this.buttonMap[elementId] = thisButton;

			if(button.svgIcon != null)
			{
				$(thisButton).append(button.svgIcon);
				$(thisButton).attr('class','genericButton');
			}
			else if(button.classNames != null)
			{
				$(thisButton).attr('class',button.classNames);
			}

			if(type == 'Event')
			{
				$(thisButton).click(function()
				{
					var entity = $(this).attr('mergeVal');
					var action = $(this).attr('action');

					if(action == 'sendCoverageEmailButton')
					{
						var mailLink = createEmailLink(_this,false);

						if(mailLink.length <= _this._config.coverageTeamEmailMaxLength)
						{
							window.location.href = mailLink;
						}
						else if(_this._config.copyEmails)
						{
							copyToClipboard(_this._config.coverage.coverageTeamEmails);

							mailLink = createEmailLink(_this,true);
							
							ACEConfirm.show
							(
								'<p>'+_this._config.copyEmailsMessage+'</p>',
								'Emails Copied to clipboard',
								["OK::confirmButton"],
								function(result)
								{
									window.location.href = mailLink;
								},
								{resizable: false, height:250, width:500, dialogClass: "warningDialog"}
							);
						}
						else
						{
							ACE.FaultHandlerDialog.show({title:'', message : _this._config.emailTooLargeError, error : 'Could not open email client'});
						}
					}
					else if(action != null && entity != null && action.length > 0 && entity.length > 0)
					{	
						var launchEvent  = action + entity;
						ACE.CustomEventDispatcher.trigger(launchEvent);
					}
					else if(action != null && action.length > 0)
					{	
						ACE.CustomEventDispatcher.trigger(action);
					}					
				});
			}
			else if(type == 'URL')
			{
				$(thisButton).click(function()
				{
					var fullURL = $(thisButton).attr('fullURL');
					var urlTar = $(thisButton).attr('target');
					window.open(fullURL, urlTar);
				});
			}
		}
			
		$(thisButton).attr('mergeVal',mergeVal);
		$(thisButton).attr('action',button.action);
		
		if(fullURL != null)
		{
			$(thisButton).attr('fullURL',fullURL);
		}
		
		$(thisButton).attr('target',urlTar);


		if(foundValue)
		{
			$(thisButton).show();
		}
		else
		{
			$(thisButton).hide();
		}		
	}

	function createEmailLink(_this,manualPaste)
	{
		var mailLink = 'mailto:';
		
		if(!manualPaste)
		{
			var totalEmailsNumber = _this.acGrid.grid.getData().getItems().length;
			var totalCCEmailsNumber = _this.ccGrid.grid.getData().getItems().length;
			var filteredEmailsNumber = _this.acGrid.grid.getData().getLength();
			var filteredCCEmailsNumber = _this.ccGrid.grid.getData().getLength();
			
			if (filteredEmailsNumber === totalEmailsNumber && filteredCCEmailsNumber === totalCCEmailsNumber)
			{
				mailLink += _this._config.coverageTeamEmails;
			}
			else
			{
				var emailList = [];
				for (var i = 0; i < filteredEmailsNumber; i++)
				{
					var email = _this.acGrid.grid.getData().getItem(i).T1C_Base__Employee__r.T1C_Base__Email__c;
					
					if (emailList.indexOf(email) !== -1 && email && email !== "")
					{
						mailLink += email + ";";
						emailList.push(email);
					}
				}
				for (var i = 0; i < filteredCCEmailsNumber; i++)
				{
					var email = _this.ccGrid.grid.getData().getItem(i).T1C_Base__Employee__r.T1C_Base__Email__c;
					if (emailList.indexOf(email) !== -1 && email && email !== "")
					{
						mailLink += email + ";";
						emailList.push(email);
					}
				}
			}
		}

		var addTo = _this.emailInputMap['additionalTo:coverage'];
		var addCC = _this.emailInputMap['additionalCC:coverage'];
		var addBCC = _this.emailInputMap['additionalBCC:coverage'];
		var subject = _this.emailInputMap['emailSubject:coverage'];

		var additionalTo = $(addTo).val();
		var additionalCC = $(addCC).val();
		var additionalBCC = $(addBCC).val();
		var emailSubject = $(subject).val();						

		if(additionalTo != null && additionalTo.length > 0)
		{
			mailLink += ';' + additionalTo;
		}
		
		var sepParam = '?';

		if(additionalCC != null && additionalCC.length > 0)
		{
			mailLink += sepParam + 'cc='+ additionalCC;
			sepParam = '&';
		}

		if(additionalBCC != null && additionalBCC.length > 0)
		{
			mailLink += sepParam + 'bcc='+ additionalBCC;
			epParam = '&';
		}

		if(emailSubject != undefined && emailSubject != null && emailSubject.length > 0)
		{
			mailLink += sepParam + 'subject='+ emailSubject;
		}

		return mailLink;
	}

	function copyToClipboard(text) {
		if (window.clipboardData && window.clipboardData.setData) {
			// Internet Explorer-specific code path to prevent textarea being shown while dialog is visible.
			return clipboardData.setData("Text", text);
	
		}
		else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
			var textarea = document.createElement("textarea");
			textarea.textContent = text;
			textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in Microsoft Edge.
			document.body.appendChild(textarea);
			textarea.select();
			try {
				return document.execCommand("copy");  // Security exception may be thrown by some browsers.
			}
			catch (ex) {
				console.warn("Copy to clipboard failed.", ex);
				return false;
			}
			finally {
				document.body.removeChild(textarea);
			}
		}
	}

	function createEmailInput(_this,component,config)
	{
		var value = getValue(config);
		var elementId = getElementId(config);
		var disabled = (config.accessLevel == 'Read');
		var componentContainer = _this.componentMap[elementId];

		if(componentContainer == null)
		{
			componentContainer = new SummaryCubeComponentContainer(_this,component,config);
			_this.componentMap[elementId] = componentContainer;
		}

		var thisInput = _this.emailInputMap[elementId];

		if(thisInput == null)
		{	
			thisInput = $('<input Id="' + elementId + '" label="" value="' + value + '"> </input>').appendTo(componentContainer.editContainer);

			thisInput.on('change', function() 
			{			
				$(_this.emailButtonContainer).show(333);
			});

			_this.emailInputMap[elementId] = thisInput;
		}
		else
		{
			$(thisInput).val(value);
		}

		$(thisInput).attr('disabled', disabled);

		return thisInput;
	}

	function getValue(config)
	{
		var thisLinkFields = config.fieldName.split('.');
		thisData = config.record;
		
		for(var x=0;x<thisLinkFields.length;x++)
		{
			if(thisData != null)
			{
				thisData = thisData[thisLinkFields[x]];
			}
		}
		
		if(thisData == null)
		{
			thisData = '';
		}

		return thisData;
	}

	function SummaryCubeComponentContainer(_this,component,config)
	{
		this.helpText = config.helpText

		if(this.helpText == null)
		{
			this.helpText = '';
		}

		this.disabled = (config.accessLevel == 'Read');
		
		this.elementId = config.fieldName + ':' + config.objectName;

		if(this.subSectionItem == null)
		{			
			this.subSectionItem = $('<div id="' + this.elementId + 'subSectionItem" class="componentContainer subSectionItem"></div>').appendTo(component);
			this.labelComponent =  $('<div title="' + this.helpText  + '" class="label">' + config.label + '</div>').appendTo(this.subSectionItem);
			this.editContainer = $('<div  id="' + this.elementId + 'editContainer" class="cubeComponentContainer"></div>').appendTo(this.subSectionItem);

			if(config.required)
			{	
				$(this.subSectionItem).addClass('required');
			}
		}
		
		return this;
	}

	ACE.AML.ControllerManager.register("alm:SummaryCubeCoverage", SummaryCubeCoverage);
//# sourceURL=SummaryCubeCoverage.js	
})(jQuery);